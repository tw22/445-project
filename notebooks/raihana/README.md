# Raihana Worklog
- Raihana Worklog
- 2021-09-12 - Research
- 2021-09-20
- 2021-09-27
- 2021-10-5
- 2021-10-11
- 2021-10-13
- 2021-10-14
- 2021-10-15
- 2021-10-18
- 2021-10-20
- 2021-10-22
- 2021-10-25
- 2021-10-27
- 2021-10-29
- 2021-11-1
- 2021-11-3
- 2021-11-5
- 2021-11-8
- 2021-11-10
- 2021-11-14
- 2021-11-17
- 2021-11-19
- 2021-11-22
- 2021-11-29
- 2021-12-3
- 2021-12-6



# 2021-09-12 - Research
## objectives

- figure out how to generate electricity through pedaling
- figure out how to connect system to spin bike
- figure out process of transferring electricity generated to outlet


The first thing I looked at was the actual mechanism of generating electricity from pedaling the spin exericise bike. 

[Link](http://newatlas.com/the-pedal-a-watt-stationary-bike-power-generator-create-energy-and-get-fit/13433/)


- Motor

1. The first aspect is having a motor connected to the front wheel of the bike. The generator is spun by the movement of the bikes wheel. The generator is made of spinning magnets within a coil of wire. Then as the magnet spins the electricity flows through the coil.
-need to figure out what type of motor we are using
-need to figure out if there needs to be a power source

[Link](http://uk.rs-online.com/web/generalDisplay.html?id=solutions/dcdc-converters-overview)


- DC-DC Converter
1. Voltage is not always stable and when it fluctuates the voltage supply can degenerate. A DC-DC converter can help stabilize the voltage. It passes a current through a "switching element." Then turns the signal into a square wave and passes it through another wave to change it back into the DC signal.
-maybe we could use a diode for this part

- Inverter
1. We would also need an inverter. Since the PCB operates using DC voltage and the wall outlet uses AC voltage we need some type of DC-AC inverter.

there were some good visual diagrams I found to show the generation process
[Link](http://www.instructables.com/How-To-Build-A-Bicycle-Generator/)


# 2021-09-20
- doing some research on safety features such as the relay
- https://www.electronics-tutorials.ws/blog/relay-switch-circuit.html

![](/notebooks/raihana/relay.PNG)
- this is what the generic relay circuit looks like 
- after about 2 seconds of someone stop pedaling the bike we want to stop our system.
  - this is the job of the relay to disconnect our circuit from the rest of the system 

# 2021-09-27
Making sure prepared for possible quiz questions
Regulator
1. Types:Linear(special type:LDO), Switching(boost, buck, buck-boost, etc)
2. How to choose yours: Which is more important: efficiency vs noise
 -Not having noise is more important for us
 -LDO-- less noise
 -Output higher or lower than input
 -Minimum input for LM78L05 is 7V and our input will not always be higher  than our output. So if our supply is less than 7V our boost converter will not work anyways so we do not need to worry if the regulator is on.
3. Why not use voltage divider(resisters) for power supply?
 -Very inefficient
4. We chose a linear regulator for the microcontroller supply
-Didn’t want a lot of noise going into microcontroller because microcontrollers are fragile
Low current output, so power loss is small
5. Denoise Capacitor
When to use: if you use a switching regulator or some noisy components
How to use: where should you place the capacitor? before/after the regulator or right next to your circuit?
 -As close as possible to the supply pin of the component
6. Level shifter: if you use chips of different logic voltages, check this out.However try to use the same logic level either 5v or 3.3V when possible
7.Diodes: 
How do you protect your circuit if someone connects your battery in the opposite polarity?
 -You put a diode in series with the supply line
 -It isolates the rest of the circuit because it will not conduct current  when the cathode voltage is higher than the anode voltage
Difference selection of the diode, what is the breakdown voltage? 
The largest reverse voltage that can be applied without causing an exponential increase in leakage current in the diode
How does the diode help protect the circuit under reverse polarity?
It isolates the rest of the circuit because it will not conduct current when the cathode voltage is higher than the anode voltage
8. When is the diode burnt?
 -When it goes over the current
9. Common communication protocols:
Before you jump onto one, we want you to know your choices: UART, SPI, CAN,(the one cars use), RS232, 5V-TTL, Ethernet, I2C
 -SPI
Justify your communication protocol choices?
 -I think our microcontroller only lets us use these two??
 -For SPI 
 --all line are driven by the transmitter both high and low. This minimizes the time required for the wire to change states
 --Easy to set up if you have I/O and connector space and dont need busses
 -I2C 
 --all lines are open-collector which means the transmitter only drives the line low
10. What are the Pros and Cons for each? When should you use differential signals communication?
11. Pull-up and pull down resistor
Why needed? Think will you get a 1 or 0 from your floating transistor?
Its used to set any unused input pins to high Vcc

![Image](http://url/a.png)


# 2021-10-5
  - worked on PCB design and revisions from design review
  - need to lower voltages from our original goal
    - aim was 40 W but now going to do testing at less than 20 W

# 2021-10-11
  - since we are waiting for parts and pcb we split up the coding assignments
  - My part is to work on the display system
      - red LED to show mode 1 which will display the current power value on the seven segment
      - yellow LED to show mode 2 is on and the display will show the total energy
      - If you hold down the button for longer than 2 seconds then the display will reset and the total energy value will go back to zero
  - In terms of testing this code
    - first I will try it out with the arduino just to see if numbers are displaying correctly
    - Then incorporate Tiffany's sensing code to test out my calculations to see if the correct value is being calculated for the power and energy.
    - Then finally integrate the code with our microcontroller

# 2021-10-13 - Differences between switching modes 
- one method is using a button counter
Define error signal	//from elisas code
If (error signal <=10%) 
	Green LED turns ON

If (button pressed for >=2 seconds) 
	Total power = 0 
	Red LED turns OFF
	Yellow LED turns OFF
	Button counter set to zero

If (button pressed for <=.5 seconds for odd num of times)
	Red LED  ON
	Yellow LED OFF
	Display shows current power

If (button pressed for <=.5 seconds for even num of times)
	Red LED OFF
	Yellow LED ON
	Display shows total power

# 2021-10-14 - Differences between switching modes continued
- Main objective is to use the button to light up red led to show mode 1 is on and green led to show mode 1 is on 
- first thing I looked at was difference between the press times for the button
  - press button less than 2 seconds switch through modes
  - press button longer than 2 seconds display should reset

- using arduino tutorial as a reference (https://arduinogetstarted.com/tutorials/arduino-button-long-press-short-press)
* Long press vs Short Press 
1. Define how long you want short press to be 
* const int SHORT_PRESS_TIME = 500; //500 miliseconds
2. Detect button is pressed and save the time  
* if (lastState == HIGH && currentState == LOW)
 pressedTime = milis();
3. Detect button is released and save the time
* if (lastState == LOW && currentState == HIGH)
 releaseTime = milis();
4. Calculate how long it is pressed
* long pressDuration = releasedTime - pressedTime;
5. Compare the pressed time with the defined short press time to see if they match up.
* if ( pressDuration < SHORT_PRESS_TIME )
  Serial.println("A short press is detected");

* for long press essentially same type of method just change the defined press time 

- This helps differentiate between pressing the button to reset vs pressing the button just to switch through the two modes 

- in terms of switching modes
  - Orignal idea was to make a counter to count how many times the button is being pressed.
    - if the number was even then its in first mode, if odd than its in the second mode 
    - this was not efficient
    - would have to keep refreshing counter 
  - Second idea was to make case statements
    - starts button on state 1
    - if statement (if is state 1 become state 2)
    - if statement (if state 2 become state 1 )
    - later declare what it means to be in the different button modes 

     for button we need to add debounce
 * there can be false triggers after being pressed so we much add a debounce

 *button and LED
``
![](/notebooks/raihana/code1.PNG)

#code block`
//set constants to the pin numbers 
const int BUTTON = ?; //change ? to button pin number
const int MODE_PRESS_TIME = 500; //500 miliseconds
const int RESET_PRESS_TIME = 2000; // 2 seconds
//nonconstant variables
int lastState = LOW; 
int lastState = LOW;  // the previous state from the input pin
int currentState;     // the current reading from the input pin
unsigned long pressedTime  = 0;
unsigned long releasedTime = 0;
void setup() {
  Serial.begin(9600);
  pinMode(BUTTON, INPUT_PULLUP);
}
void loop() {
  // read the state of the switch/button:
  currentState = digitalRead(BUTTON);
  if(lastState == HIGH && currentState == LOW)        // button is pressed
    pressedTime = millis();
  else if(lastState == LOW && currentState == HIGH) { // button is released
    releasedTime = millis();
    long pressDuration = releasedTime - pressedTime;
    if( pressDuration < MODE_PRESS_TIME )
      Serial.println("A short press is detected");
  //here we would also put a state for the LED to be on 
    if( pressDuration > RESET_PRESS_TIME )
      Serial.println("A long press is detected");
      //at this we would set display to be reset
  }

  // save the the last state
  lastState = currentState;
}


# 2021-10-15 - Displaying on HEX 
- objective is to display numbers on the seven segment display
- using arduino tutorial as reference (https://www.circuitbasics.com/arduino-7-segment-display-tutorial/)
 - in the tutorial their seven segment has 6 pins whereas ours has 8. So using the datasheet I had to make adjustments to the pin assignments and code 

   - INSERT DISPLAY PIN ASSIGNMENTS 
  ![](/notebooks/raihana/hexPins.PNG)
- The basic code just to display something 

# code block
#include "SevSeg.h"
SevSeg sevseg; 

void setup(){
    byte numDigits = 1;
    byte digitPins[] = {};
    byte segmentPins[] = {6, 5, 2, 3, 4, 7, 8, 9};
    bool resistorsOnSegments = true;

    byte hardwareConfig = COMMON_CATHODE; 
    sevseg.begin(hardwareConfig, numDigits, digitPins, segmentPins, resistorsOnSegments);
    sevseg.setBrightness(90);
}

void loop(){
        sevseg.setNumber(4);
        sevseg.refreshDisplay();        
}

![](/notebooks/raihana/pinning.PNG)
Then editing this to work for system with the pin assignment edition and the case statements for printing

![](/notebooks/raihana/hex1.PNG)

# 2021-10-18 - Calculations 
- objective is to figure out how to calculate current power and total energy
- current power
  - using tiffany's sensing code there are already assigned variables for the current and voltage we can just multiple those two values 

- total energy
  - first calculate average power
    - make an array to contain a set of current power values
    - add all those values together and divide by the size of the array
  - Calculate duration
    - set initial time when we first press the button 
    - after modulation and calcutions find current time 
    - subtracting the two times should give you the duration 
- remember to add a loop so that the display is not constantly changing that you are not able to read it fast enough
- remember to refresh display 
- remember to do conversion of watts per hours to seconds 


# 2021-10-20 - calculation code 
-did research on collecting values 
  - potentiometer values from arduino ()
https://www.arduino.cc/en/Tutorial/BuiltInExamples/Smoothing
- this is a reference code of calcuting average using potentiometer
    - One possible way for calculating average power  
       - create an array of about 10 values
       - set 10 of the power value outputs from the function generator into the array
       - add the 10 values in the array together
       - divide that value by 10
       - this should give the average power
    - Now to get the total energy
      - multiply the average power by time 

![](/notebooks/raihana/code_1_diagram__1_.png)

# 2021-10-22 - Combining with Sensing
- now to see if the display code works i tested it on arduino. The sensing code converts the current and voltage values to a leveled down value. It maps out values for the current and voltage. Combing that with my code allowed me to multiply the waveforms generated and see if the display is showing the correct values. 

![](/notebooks/raihana/sensing.PNG)
-there was now some new display errors.
- 111.3 was displaying as 1.333
- 333.1 was displaying as 3.333
- 3.1 was displaying 3.133
- 333.4 was displaying as 3.999
- 333.5 was displayiyng as 3.999

- thinking the issue might be that the letters are reversed


#code block
#include "SevSeg.h"
SevSeg sevseg; 
const int BUTTON_PIN = A0;
//const int MODE_PRESS_TIME = 500;
const int RESET_PRESS_TIME = 2000;

int buttonMode = 1;
int lastButtonState;  //previous state of button
int currentButtonState; //current state of button
unsigned long pressedTime = 0;
unsigned long releasedTime = 0;
unsigned long final_time = 0;
unsigned long init_time = 0;
unsigned long total_time_elapsed = 0;

int iteration;
float totPower;
float totalEnergy;
float currPower;
float avgPower_display;         //average power being displayed since it is not constantly changing

const int powerReadings = 10;   // size of array
float readings[powerReadings];  //values from current sensor
int readIndex = 0;              //index of current value
float avgPower = 0;             //average power

// defining ADC pins being used
#define Vout_sens A5
#define Vdc_sens A4
#define Vgrid_sens A3
#define Iout_sens A2

// store raw ADC in these variables
int Vout_adc;
int Vdc_adc;
int Vgrid_adc;
int Iout_adc;

// non-ideal mapping variables for AC, DC sensing circuit
float A;
float B;
float C;

// non-ideal mapping variables for Arduino
float x;
float y;

// original values
float Vout_map;   //inverter output
float Vdc_map;    //DC-DC output
float Vgrid_map;  //grid output
float Iout_map;   //current output

void setup() {

  Serial.begin(2000000);

  // configure inputs/outputs
  pinMode(Vout_sens, INPUT);
  pinMode(Vdc_sens, INPUT);
  pinMode(Vgrid_sens, INPUT);
  pinMode(Iout_sens, INPUT);

  //assign non-ideal mapping vars for Arduino
    //ideal Arduino mapping (ADC,V): (1023,5),(0,0)
    //non-ideal Arduino mapping: (x,5),(y,0)
    //adjusted slope: instead of (5/1023)(ADC), use ((5/(x-y))(ADC)+y)
      //((5/(x-y))*(ADC)+y)
  x = 1023;
  y = 0;

  // comment this out later: for display
  //Serial.begin(2000000);
    byte numDigits = 4;
  byte digitPins[] = {10, 11, 12, 13};
  byte segmentPins[] = {9, 2, 3, 5, 6, 8, 7, 4};
  pinMode(BUTTON_PIN, INPUT_PULLUP);
  bool resistorsOnSegments = true; 
  bool updateWithDelaysIn = true;
  byte hardwareConfig = COMMON_CATHODE; 
  sevseg.begin(hardwareConfig, numDigits, digitPins, segmentPins, resistorsOnSegments);
  sevseg.setBrightness(70);
  bool updateWithDelays = false; // Default 'false' is Recommended
  bool leadingZeros = false; // Use 'true' if you'd like to keep the leading zeros
  bool disableDecPoint = false;
  //Serial.begin(9600);

  buttonMode = 1;
  lastButtonState = HIGH;
  currentButtonState = HIGH;
  totalEnergy = 0;
  currPower = 0;

  for (int i = 0; i < powerReadings; i++){
    readings[i] = 0;
  }

  init_time = millis();
  final_time = 0;
  
}

void loop() {
  // read raw ADC values (0 to 1023)
  Vout_adc = analogRead(Vout_sens);
  Vdc_adc = analogRead(Vdc_sens);
  Vgrid_adc = analogRead(Vgrid_sens);
  Iout_adc = analogRead(Iout_sens);

  //DC mapping
    /* raw ADC 0 to 1023 corresponds with 0V to 5V
      // v_in = 0 to 5V
      // v_in = (5/1023)x(ADC)
    // v_in mapping = 0 to B (microcontroller input voltage values)
    // v_orig = 0 to A (original voltage values, before voltage divider)
      //v_orig = (A/B)x(v_in)*/
    // equation to use for DC mapping: v_orig = (A/B)x(5/1023)x(ADC) 
    
  //AC mapping
    /* v_in mapping = 0 to C (microcontroller input voltage values)
    // v_orig = -A to +A (original voltage values)
      // (C,A), (0,-A)
      //v_orig = (2xA/C)x(v_in)-A*/
    // equation to use for AC mapping: V_orig = (2xA/C)x(5/1023)x(ADC)-A

  //Current mapping
    /*-10A to 10A -> 0V to 5V   (0,-10),(5,10)
      //I_orig = (10+10)/5 x v_in - 10*/
    // equation to use for current mapping: I_orig = (4)x(5/1023)x(ADC)-10

  // different testing voltages
  int test = 1; //this variable can be changed depending on what voltage is being tested
  
  switch (test) {
    /** for now, pretend all mapping values map to 0V to 4.2V ***/
    case 1: //170V
      A = 170;
      B = 5.0; //4.7375;
      C = 5.0; //4.037;
      break;
    case 2: //100V
      A = 100;
      B = 5;
      C = 5;
      break;
    case 3: //50V
      A = 50;
      B = 5;
      C = 5;
      break;
    default: //170V
      A = 170;
      B = 5;
      C = 5;
      break;
  }

  //DC: R values map input 0 to A -> 0 to B
  //AC: R values map input -A to A -> 0 to C
    Vout_map = (2*A/C)*((5/(x-y))*Vout_adc+y)-A;
    Vdc_map = (A/B)*((5/(x-y))*Vdc_adc+y);
    Vgrid_map = (2*A/C)*((5/(x-y))*Vgrid_adc+y)-A;
    Iout_map = 4*((5/(x-y))*Iout_adc+y)-10;


  //Serial Monitor / Plotter
    //Serial.println(Iout_map);
        currentButtonState = digitalRead(BUTTON_PIN);
    if(lastButtonState == HIGH && currentButtonState == LOW){
      pressedTime = millis();
    }
    else if (lastButtonState == LOW && currentButtonState == HIGH){      
      releasedTime = millis();

      long pressDuration = releasedTime - pressedTime;

      if(pressDuration > RESET_PRESS_TIME) {
        totalEnergy = 0;
        Serial.println("reset");
      }

      else if(pressDuration < RESET_PRESS_TIME){
        if(buttonMode == 1){
          buttonMode = 2;
          //Serial.println("Mode 2");
        }
        else if (buttonMode == 2){
          buttonMode = 1;
          //Serial.println("Mode 1");
        }
        else
          buttonMode = 1;
        //sevseg.setNumberF(10.345, 1);
      }
    }
     switch(buttonMode){
      case 1:
        sevseg.setNumberF(avgPower_display, 1);
        break;
      case 2:
        sevseg.setNumberF(totalEnergy, 1);
        break;
      default:
        sevseg.setNumberF(11.1111, 1);
        break;  
     }
    currPower = Vout_map*Iout_map;  //p=IV 
    //Serial.println(currPower);
    
    totPower = totPower - readings[readIndex];
    readings[readIndex] = currPower;
    totPower = totPower + readings[readIndex];
    readIndex = readIndex + 1;
    if (readIndex >= powerReadings ){
      readIndex = 0;
    }
    avgPower = totPower / powerReadings;
   // delay(1);
    //Serial.println(Vout_map);
    
    lastButtonState = currentButtonState;
    sevseg.refreshDisplay(); 
    iteration++;
    iteration = iteration % 1000;
    if(iteration==0){
      avgPower_display = avgPower;
      //Serial.println(avgPower_display);
      
      final_time = millis();

      total_time_elapsed = final_time - init_time;
      Serial.println(total_time_elapsed);
      totalEnergy = totalEnergy + (total_time_elapsed)*(avgPower)/(3600000); //division by 3.6e6 converts total time elapsed from ms to hours
      
      init_time = millis();
      
    }
      
}

# 2021-10-25 - Modifications to PCB
- since the microcontroller(ATmega16U4) was not able to be programmable with the arduino we needed to switch to the ATmega32U4
 - even though it said it was an internal clock resonator on the datasheet it actually was not.
    - had to include a oscillator circuit into the PCB
- change boost converter chip size since the footprint was too small
- change transformer footprint since the pin holes were not big enough
- change relay footprint sinze current relay had too high threshold that it was not getting enough current to start up 
- add the missing ground planes


# 2021-10-27 - Microcontroller issues 
- microcontroller was not programming at all 
- had to set internal oscilator for the microcontroller to run
- Had to make a temporary oscilator circuit in order to start testing
https://forum.arduino.cc/t/board-definitions-for-atmega32u4-using-internal-8mhz-clock/188697/2
- had to change the fuse settings for the microcontroller according to forum post 


# 2021-10-29 - update on microcontroller
- Microcontroller pins were potentially fried due to either incorrect connections or overheating due to the soldering
- had to implement the new microcontroller chip 



# 2021-11-1 - assigning ports
- After testing code onto arduino it needs to be incorporated onto the PCB. This means connecting it to the microcontroller
- In the second lab assignment for this class when we did there you could just call the pins directly
  - you can say Button pin4 and it set sets the button to the corresponding pins. 
  - however i tried this with every single pin however nothing was being called.
  - when i would set the corresponding pins on the microcontroller to high or low nothing was getting the correct signal.

![](/notebooks/raihana/pins.PNG)






# 2021-11-3 - testing

- Then we realized that we actually had to go in and individually assign ports to each pin 
- also use ddrc to assign as either an output or input pin 

https://web.ics.purdue.edu/~jricha14/Port_Stuff/PortC_general.htm

- this website discusses how you can write the ports to be either an input or output, have internal pull up, or have a read or write 
- example of this would be :
- DDRC = 0x00
- PORTC = 0x00

# 2021-11-5
 - There was an error on the new PCB design. The mosfets for the transformer circuit were switched. So I desoldered the connections and cut the traces. Then soldered on two new seperate wires to the PCB. 
 - Now was mainly just testing out the code and seeing calibration values since there is a slight offset when combining the inverter code with the display.


# 2021-11-8
- Elisa was finishing up the testing with the boost converter circuit
- I desoldered the components from the testing circuit and starting resoldering them to the new PCB 


# 2021-11-10

- putting all the componets together to finalize our project for the mock demo 
- decided on not including the on/off switch to our circuit
  - since it is not part of the high level requirements
  - we did not invlude this into the PCB so it would have to be attached externally with another circuit board. 
  - this would make our pcb look messy and not as organized

# 2021-11-14
  - started doing research into how to make our code faster since there were alot of delays once we added everyone's code together 
  - a team from previous years had implemented multicore to make their code run faster. 
  - an example of multicore is google chrome
  - essentially refers to an architecture where a single processor processes the core logic of more than one processor. So essentially having "multiple cores"
  - this would be ideal for us since our display code and the PLL algorithm does not interact with eachother.
  - so we could have each of the codes running simultaneously
  - however we cannot actually implement multicore since we do not have multiple cores into our microcontroller 

# 2021-11-17
  - At this point most of the systems works except the boost converter portion
  - tried to implement multithread onto display code in order to decrease delays  

https://www.perforce.com/blog/qac/multithreading-parallel-programming-c-cpp#:~:text=Multithreaded%20programming%20is%20programming%20multiple,running%20on%20multiple%20processor%20cores.

 - when you cannot do multicore you can do something called multithread
 - this is when you can execute multiple sequential set of instructions at the same time.
 - it is an illusion of running in parallel

 - However after implementing this there was no improvements with the waveforms. 
 - There was essentially the same amount of delay 

# 2021-11-19
  - Had mock demostration with TA today 
  - tested our entire system with the waveform generator at first 
    - correct waveform signals for inverter, DC-DC converter and shows correct output on display 
  - Then we tested our system with the bike and motor 
    - This also worked! (we recorded our demo in case of anything breaking)
    - when we test systems seperately it works faster than when we tested display and converter/inverter together
      - this is due to delays in the coding since we could not implement multithread


# 2021-11-22
  - worked on presentation slides for mock presentation 
  - researched more on PLL algorithm


# 2021-11-29
  - worked on soldering leds, button and display onto our box
  - filmed extra credit video
  - worked on finishing touches before demonstration
  - worked on final report


# 2021-12-3
  - Had mock presentation today 
  - feedback from TAs 
    - Pros  
      - liked transitional speaking 
      - liked our color coded requirements to show which worked and did not work
    - Cons
      - increase font size 
      - explain visuals more 
      - shorten our title
      - add animation to the slides 
      - find a better way to flow through our presentation

# 2021-12-6
  - finished working on the final edits of the final paper
  - reviewed edits from the feedback from the desgin proposal 
  - Also worked on preparing for the final presentation
