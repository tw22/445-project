# Elisa's Worklog

* [9/14/21](#9/14/21)
* [9/14/21 After TA Meeting](#9/14/21 After TA Meeting)
* [9/23/21](#9/23/21)
* [9/24/21](#9/24/21)
* [9/25/21](#9/25/21)
* [9/28/21](#9/28/21)
* [10/13/21](#10/13/21)
* [10/17/21](#10/17/21)
* [10/19/21](#10/19/21)
* [10/23/21](#10/23/21)
* [10/26/21](#10/26/21)
* [10/29/21](#10/29/21)
* [11/10/21](#11/10/21)
* [11/17/21](#11/17/21)
* [11/23/21](#11/23/21)
* [11/29/21](#11/29/21)

<a name="9/14/21"></a>
# 9/14/21

## Objectives
- Power converter topology research
- Grid synchronization research

1) Dc-dc converter
- Output voltage from motor may be intermittent
    - How can we regulate voltage at motor terminals?
    - Only supply power when power from bike passes a certain threshold?
- Need something with a high gain
    - Common dc motors I've seen are operated around 24V or 12V
    - Need to boost voltage up to roughly 170V
        - Voltage value will be different because 1st fundamental has a magnitude of 4/pi*Vpk (need to check relationship)
    - Maybe something similar to a flyback converter?

2) Inverter
- Resonant Inverter
    - Frequency fixed by LC components
    - Large components
- PWM Inverter
    - Easy to adjust frequency
    - Need slightly variable frequency for PLL
    - Probably better choice

3) Grid Synchronization
    - PLL
        - Implementing the PLL through an algorithm in the microcontroller would probably be easier
        - Analog PLL leads to more circuit complexity that may be too difficult to complete in a semester
        - There is a simple algorithm and more complex ones for accounting for disturbances in the grid.
        Start with the simpler algorithm and expand to disturbances if time.
        - Familiar with square wave PLLs, not PLLs for sine waves
            - Convert sine wave to a square wave using a threshold. May cause distortion, but hopefully not too much.
            Continue to look for sine wave PLLs.
        - Resource for PLLs synchronizing to grid: https://www.digikey.com/en/articles/synchronizing-small-scale-pv-systems-with-the-grid
        - Resource for PLLs for slow sine waves: https://ieeexplore.ieee.org/document/4497145
    - Grid standards
        - Initially looked at IEE 1547-2018: https://site.ieee.org/gms-pes/files/2017/02/IEEE-1547-Vermont-Chapter.pdf
            - Only required for distributed energy resources (DER) "having an aggregate DER nameplate rating of 500 kW or greater, and having an average load demand of equal or less than 10% of the DER nameplate rating"
                -Not strictly required for us, but would be nice to meet standards
            - Should our generator be able to respond to abnormal grid voltage or frequency?
                - Ties in with question of complexity of PLL
        - Power requirements
            - DER shall be capacble of injecting reactive power and absorbing reactive power equal to the minimum reactive power
            - Adjustable constant power factor mode
            - Voltage reactive power
            - Active power reactive power mode
            - How can we control the power factor of our generator?
                - Resource discussing power factor control for solar system: https://cdn.selinc.com/assets/Literature/Publications/Technical%20Papers/6532_PowerFactor_KH_20120223_Web2.pdf?v=20190325-150731

<a name="9/14/21 After TA Meeting"></a>
# 9/14/21 After TA Meeting

## Objectives
- Note feedback on project proposal from Bonhyun

1) Block diagram
- Clarify 12V power supply for gate driver
- Combine dc-dc and dc-ac converters
- Decide between 3.3V and 5V supply for microcontroller
    - Might need a voltage regulator
- Use a current transformer sensor or a current sensor?
    - Look at price difference
- Replace disconnect with relay
- Add display and button to control and sensors
- Add spin bike wheel to generator block

2) High level goals
- change order
- create complete sentences
- be specific about IEEE standards meet
    - might not meet all of the requirements
    - clarify which requirements will be meet
    - clarify that we are not required to meet the requirements because we are way under the power threshold

<a name="9/15/21"></a>
# 9/15/21

## Objectives
- Look into internally driving gate drivers
- Decide which IEEE 1547 standards we will try to meet

1) Gate drivers
- Group from last semester (BikeBikeRevolution) used a wide Vin boost controller
    - don't have to use digital control; there are solutions out there that are fairly easy to implement
    - accepts voltages from 2-40V
        - dc motor will likely generate somewhere from 0-24 V
    - can power boost converter internally
- For the inverter, can use the output of the boost
    - output of boost = 120sqrt(2)/(4/pi) = 133 V
    - Is 133V too high of a voltage to drive the mosfets?
        - Yes
    - need a voltage regulator to step down 133 V boost voltage
        - could also have separate wide vin dc-dc boost that create a more manageable dc supply level

2) IEEE 1547 standards
- Not required to meet as previously discussed
    - Quick math:
        - max number of bikes in a gym = 100 (for a very large gym)
        - max power from 1 bike = 300 W (really more 200-300W, this is worst case scenario)
        - max total power = 100*300 = 300 kW < 500 kW requirement
- Probably just steady state conditions
    - maybe add steady state as reach goal

<a name="9/23/21"></a>
# 9/23/21
## Objectives
- Clarifications after proposal
- Design power converters

1) Clarifications
- We are required to meet IEEE 1547 standards to connect to the grid. Misread the standards at first
    - Would have to contact local utility to figure out a setup
- No need to boost voltage all the way up to 170 V. Instead, create lower voltage AC signal and use transformer to step up to appropriate voltage
    - Minimizing high voltage circuitry
        - Good for safety and size of components

2) Power converters
- Boost converter
    - No longer need flyback because boost doesn't need to be that high
    - Can use basic boost converter
    - Doesn't need to be synchronous
        - Possible concerns at light load. Need to fully simulate

<a name="9/24/21"></a>
# 9/24/21
## Objectives
- Characterize motor

1) Motor characterization
    - Some data points from testing

| Voltage (V) | Current (A) | Power (W) |
| ------ | ------ | ------ |
| 29.2 | 1.95 | 57.2 |
| 35.7 | 2.38 | 85.3 |
| 7.85 | 0.52 | 4.09 |
| 10.2 | 0.68 | 7.04 |
| 28.6 | 3.73 | 106.4 |

    - Minimum voltage is approximately 8 V, though it is difficult to achieve such a low number
        - With minimal effort, voltage is 10 V
    - Maximum voltage is roughly 36 V
    - Maximum power is 106.4 W

<a name="9/25/21"></a>
# 9/25/21
## Objectives
- Work on power converter design

1) Boost converter
    - Derive rating from 170 V and 150 W (increased for safety factor)
        - Voltage rating should be at least 340 V and average current will be 1.176 A
    - Flyback topology
        - No longer need transformer on final ac output
        - Boost up to 170 V
        - Can have second output for gate drivers
        - might get two transformers b/c can't find any with two outputs
        - Possible transformers
            - 750370228 - Wurth Electronik
                - Datasheet: https://www.we-online.com/katalog/datasheet/750370228.pdf
                - Cost: $8.33
                - Auxiliary 12 V output
                    - Only can supply 12 mA; not enough for a gate driver
                - could work and just not use 12 V output/get second transformer
                    - Second transformer ratings:
                        Turn ratio: 1:0.46 for 10 V, 1:0.56 for 12 v
                            - Look for ratio of 1:0.5; 11 V will be good for gate drivers
            
                - Saturation current: 1.325 A 
                    - Expected average current of 1.176 A; with large ripple could go into saturation
                - Vprimary: 120 - 375 VDC
                - Vsecondary: 12 V, 1.25 A (Is the 1.325 A saturation current rating for the secondary side?)
                - Rated for 15 VA: won't work
            - MTPL-2516-S12V
                - primary side magnetizing inductance = 450 uH
                - primary side leakage inductance = 1.70 uH
                - Turns ratio: 0.176
                - Rated current: 22.0 a
                - highest current and voltage rating for a planar transformer I could find
                - best option
        - Difficult to find transformers for dc-dc rated for 200 W with high gain
            - Most are for isolation or low turn ratios
    - SEPIC converter
        - Would need a 12 Vac to 120 Vac transformer at the end of the inverter
            - Rest of components would be rated for a lower voltage level, but higher current level   

![](/notebooks/elisa/flyback_ratings.JPG)

2) Inverter
    - Full bridge because half bridge would require an even higher voltage
    - Voltage rating = 2*170V
    - Current rating = 150/170 = 0.88 A
    - Choose as large an inductor as possible
        - Choke inductor - 4.7mH

LTSpice Simulation results
![](/notebooks/elisa/Vdc_and_Vout.JPG)
![](/notebooks/elisa/whole_inverter.JPG)

<a name="9/28/21"></a>
# 9/28/21
## Objectives
- Finish overall schematic
- Layout first draft of PCB

1) Schematic
    - Do we need differential voltage sensing? (probably)
        - Currently measuring positive terminal with respect to ground...
    - Check transformer
        - Check rating and potential inrush current
    - What is the resonator for the microcontroller?
        - Need to decide what to set as value
    - Potential half-bridge driver issue?
        - Vin should be from 0 to Vcc
        - Do we need to shift the microcontroller output up?
    - Should the negative side of the flyback outputs be connected to ground?
        - Potential for ground loops?
    - Follow layout from TI datasheet for boost converter: https://www.ti.com/lit/ds/symlink/lm5156.pdf?ts=1639078035791&ref_url=https%253A%252F%252Fwww.ti.com%252Fproduct%252FLM5156
        - Use TI calculator to find resistor values: https://www.ti.com/product/LM5156#hardware-development
2) PCB Layout
    - need to go back and double check footprints
        - change inductor and transformer footprints specifically
    - break into two boards?
        - don't have time...

<a name="10/13/21"></a>            
# 10/13/21
## Objectives
1) Review PI control Arduino code 
2) Create open-loop control for inverter

Arduino PI control
-Use ECE 469 code

```
#include "TimerHelpers.h"

// Timer 0

// input     T0     pin  6  (D4)
// output    OC0A   pin 12  (D6)
// output    OC0B   pin 11  (D5)

const byte timer0Input = 4;
const byte timer0OutputA = 6;
const byte timer0OutputB = 5;

unsigned long last_time_sampled = 0;
float D = 0;                    // Duty ratio is at the scale of 0-1023
int last_sample = 0;                // X(z-1) in z-domain
float I_out = 0;                // output of I control
float KI, KP, KD;                   // These are the parameters for P control and I control. 
bool flag = 0;                  // This is the flag of triggering overflow and underflow(when flag == 1, the integral will be paused)
bool AW_enable = 1;             // This is the boolean to enable anti-windup
const float conversion =  1e-6; // Convert the sample/us to sample/s
float P_out = 0;                // Proportional output

void setup() {
   pinMode (timer0OutputA, OUTPUT); 
   pinMode (timer0OutputB, OUTPUT); 
   pinMode (A0, INPUT);  
   pinMode (A1, INPUT);
   pinMode (3, OUTPUT);            
   TIMSK0 = 0;  // no interrupts
   Timer0::setMode(5, Timer0::PRESCALE_1, Timer0::CLEAR_A_ON_COMPARE | Timer0::CLEAR_B_ON_COMPARE);      // Use Timer0 to speed up the output frequency
   OCR0A = 133;    // number of counts for a cycle, the resulting period is 160 * 62.5 ns *2 = 20us, and corresponding frequency is 50kHz
   Serial.begin(9600);
}  // end of setup

void loop() {
    digitalWrite(3, HIGH);
    KI = 0.00001;
    //KI = 0;
    KP = 0.05;
    //KP = 0.5 ;  //These are for the P control
    KD = 0; //added derivative coontrol

    int ref = analogRead(A1);  //Record the reference input
    int fdb = analogRead(A0);  //Record the feedback input
    int err = 3 * (ref - fdb); //Here the error is 3x the difference of reference and feedback
    unsigned long now = micros();

	// Add your code here
  //if close-loop:
  float P_out = KP * err; //P_out is the proportional output
  //define bilinear_integral equation
  float bilinear_integral = 0.5*(now - last_time_sampled)*KI*(last_sample + err);
    if(AW_enable){ //AW_enable is a boolean to trigger anti-windup
      if(!flag){
        I_out += bilinear_integral*conversion; //anti-windup is tested here. Use t_now
        //and t_before here. And conversion is 1e-6, to convert the us scale to s scale
      }
    }                                                                                                   
    else{
      I_out += bilinear_integral*conversion; 
    }
    last_time_sampled = now; //Remember to set an initial value for t_before
    last_sample = err;
    D = P_out + I_out;
  //if open loop:
   //D = 0.35*1023;//Enter duty ratio here

  if(D>870){ //Duty cycle of 85%
    D = 870;
    flag = 1;
    }
  else if(D<0){
    D = 0;
    flag = 1;    
    }
  else{
    flag = 0;
    }
   
//    D = 307; //D = 307 means the corresponding duty ratio is 30%. The following are just for testing
//    Serial.print(ref);
//    Serial.print(';');
//    Serial.print(fdb);
//    Serial.print(';');
//    Serial.print(err);
//    Serial.print(';');
//    Serial.print(now-last_time_sampled);
//    Serial.print(';');
//    Serial.print(D);
      //Serial.print(';');

    float PWM = D*160.0/1023.0;  //Convert the duty ratio
    OCR0B = (int)PWM +1;    // duty cycle within OCR0A, +1 is for compensating the rounddown when casting from float to int.
    digitalWrite(3, LOW);
}
```
- already include derivative control as well so would just need to change constants
- be sure to include timer helper file as well

2) Inverter Open-Loop Control
- Reference for creating sine wave duty cycle PWM: https://www.instructables.com/Arduino-Sinewave-for-Inverters/
- Another reference: https://github.com/Irev-Dev/Arduino-Atmel-sPWM 
- Create lookup table for sine wave so calculations don't need to be repeated every cycle
- Use interrupts to ensure constant switching frequency
    - From github reference, TCCR1A, TCCR1B and TIMSK1 are used to interrupt program
        - May be necessary if control and other sensing takes too long and the PWM needs to be output quickly
        - Similar approach is used for 464 control
            - Put duty cycle value into register OCR0B
            - Github reference uses OCR1B and OCR1A
                - Need to check microcontroller datasheet to find the appropriate register
- Using github code as starting point
    - validated code works as desired: fsw = 100kHz, sine f = 50 Hz and half-sine waves generated
    - switching period set by ICR1. Period for 16MHz crystal for Arduino Uno. Check expected frequency for ATmega16u4
    - set output pins with DDRB. Check how to set on ATmega16u4
    - can use trig signal as q3 (no changes needed)
    - Changes made:
       - changed switching frequency to 20kHz
            - ICR1 = 800 (16MHz/20MHz)
            - lookUp table values scaled by 0.5
                - Used to be normalized to 1600, so now normalized to 800
                - Update index for lookUp table every other cycle to maintain 50Hz sine wave
        - created full wave, all positive sine wave
            - adjust lookup tables to create full sine wave
               - Formula for lookUp table entries:
                   - For entry i in a table with n entries:
                       - Value = ICR1*sine(360 degrees/n x i)
                       - Created matlab script to generate values
                - Only need to include a quarter of a sine wave due to symmetry
                - Include 256 entries
                - Angle = count * 360 /(clock freq/ICR/fsine)
    - Debugging:
        - less than 5 for a long period of time...
        - modulation frequency correct but not resetting correctly
        - count only going up to 1248
        - issue was incorrect understanding of interrupt
        - interrupt triggers everything switching frequency (every 800 clock cycles in this case)
        - scaling factor was incorrect for calculating angle
        - also moved everything to the regular program space so that the interrupt will be as short as possible

Brief Arduino Timers Tangent
- Source: https://create.arduino.cc/projecthub/Marcazzan_M/internal-timers-of-arduino-58f6c9
- Control logic increments TCNTn register by 1 every clock cycle
- Value is compared to OCRn register
- Triangle wave for comparison is generated by timer
    - Timer counts to the value specified by ICRn
    - ISR(TIMER1_OVF_vect) interrupts the program when the timer overflows
        - Executes program at this point
            - Need to make sure program execution time is shorter than the timer
                - Put bulk of program in the regular execution loop

Open Loop Control Completed:
```
/*
 * sPWMv2.c
 *
 * Created: 31/12/2014 3:44:43 PM
 *  Author: Kurt Hutten
 sPWM on the atMega328 for the arduino Uno, might be compatable with other atmel chips / arduino boards.
 Compare outputs A and B output to PORTB pins 1 and 2 which are pins 9 and 10 respectively on the Uno.
 Also useful to know the led on the Uno is pin 5 on PORTB.
 */ 


#include <avr/io.h>
#include <avr/interrupt.h>

long unsigned count;

// Look up tables with 256 entries each, normalised to have max value of 800 which is the period of the PWM loaded into register ICR1.
int lookUp1[] = {410, 420, 429, 439, 449, 459, 468, 478, 488, 497, 507, 516, 525, 535, 544, 553, 562, 571, 580, 589, 597, 606, 614, 622, 630, 638, 646, 654, 661, 669, 676, 683, 690, 696, 703, 709, 715, 721, 727, 733, 738, 743, 748, 753, 757, 762, 766, 770, 773, 777, 780, 783, 786, 788, 790, 792, 794, 796, 797, 798, 799, 800, 800, 800, 800, 800, 799, 798, 797, 796, 794, 792, 790, 788, 786, 783, 780, 777, 773, 770, 766, 762, 757, 753, 748, 743, 738, 733, 727, 721, 715, 709, 703, 696, 690, 683, 676, 669, 661, 654, 646, 638, 630, 622, 614, 606, 597, 589, 580, 571, 562, 553, 544, 535, 525, 516, 507, 497, 488, 478, 468, 459, 449, 439, 429, 420, 410, 400, 390, 380, 371, 361, 351, 341, 332, 322, 312, 303, 293, 284, 275, 265, 256, 247, 238, 229, 220, 211, 203, 194, 186, 178, 170, 162, 154, 146, 139, 131, 124, 117, 110, 104, 97,  91,  85,  79,  73,  67,  62,  57,  52,  47,  43,  38,  34,  30,  27,  23,  20,  17,  14,  12,  10,  8, 6, 4, 3, 2, 1, 0, 0, 0, 0, 0, 1, 2, 3, 4, 6, 8, 10,  12,  14,  17,  20,  23,  27,  30,  34,  38,  43,  47,  52,  57,  62,  67,  73,  79,  85,  91,  97,  104, 110, 117, 124, 131, 139, 146, 154, 162, 170, 178, 186, 194, 203, 211, 220, 229, 238, 247, 256, 265, 275, 284, 293, 303, 312, 322, 332, 341, 351, 361, 371, 380, 390, 400};

void setup(){
    // Register initilisation, see datasheet for more detail.
    TCCR1A = 0b10100010;
       /*10 clear on match, set at BOTTOM for compA.
         10 clear on match, set at BOTTOM for compB.
         00
         10 WGM1 1:0 for waveform 15.
       */
    TCCR1B = 0b00011001;
       /*000
         11 WGM1 3:2 for waveform 15.
         001 no prescale on the counter.
       */
    TIMSK1 = 0b00000001;
       /*0000000
         1 TOV1 Flag interrupt enable. 
       */
    ICR1   = 800;     // Period for 16MHz crystal, for a switching frequency of 100KHz for 200 subdevisions per 50Hz sin wave cycle.
    sei();             // Enable global interrupts.
    DDRB = 0b00000110; // Set PB1 and PB2 as outputs.
}

void loop(){    
    static float angle;
    static char trig;
    int index;
    int index2;
    // change duty-cycle every period.
    index = 0.7111*(angle + 1);
 
    if(index + 128 < 256){
       index2 = index + 128;
    }else{
       index2 = index - 128;
    }
 
    OCR1A = lookUp1[index];
    OCR1B = lookUp1[index2];;

    angle = count*1.08;
    
    if(angle >= 360){ // Pre-increment num then check it's below 200.
       angle = 0;       // Reset num.
       count = 0;
     } 
    digitalWrite(11,trig);
}

ISR(TIMER1_OVF_vect){
count = count + 1;
}

```

![](/notebooks/elisa/sinusoidal_deadtime.png)
![](/notebooks/elisa/sinusoidal_freq.png)

Potential issues:
- Oscilloscope isn't measuring frequency super accurately...
- Maybe need to add more entries to lookup table in an attempt to get more accurate sine wave

Other thoughts:
- Supply voltage is derived from higher voltage (will it be high enough if run at lower voltage?)
- Less distortion when q3 = q1' -> just use one output from microcontroller... Should probably change control scheme. Code above is for the updated control scheme. Thanks to this reference: https://academica-e.unavarra.es/xmlui/bitstream/handle/2454/22430/TFE_ElisaBraco.pdf?isAllowed=y&sequence=1
See plots below for distortion comparison:

- Also talked through basics of creating a sinusoidal signal through PWM modulation with Raihana and Tiffany:
![](/notebooks/elisa/elisa_teaching_pwm_signals.JPG)

<a name="10/17/21"></a> 
# 10/17/21
## Objectives
1) Figure out PLL Control Algorithm and how to test

1) PLL Control Algorithm
    - Combined PI control from 469 with open-loop control and Tiffany's sensing code
    - Doesn't work at all..
    - Need to add LPF still

<a name="10/19/21"></a> 
# 10/19/21
## Objectives
1) Solder and test inverter

1) Inverter testing
    - Still waiting on LDO so won't test that until later
        - Use an external supply for now
    - Calculate bootstrap capacitance:
        - https://www.ti.com/lit/an/slua887/slua887.pdf?ts=1634677463727&ref_url=https%253A%252F%252Fwww.google.com%252F
        - Cg = Qg/Vg1
        - Cg = 24n/10 = 2.4n
        - Cboot,min = 24nF -> choose 33nF +- 10%

<a name="10/23/21"></a> 
# 10/23/21
## Objectives
1) Test boost converter

1) Boost converter
    - Soldered wires onto LM5156 because footprint was incorrect on PCB
    - Switching signal produced as input voltage is increased past 4 V
        - No voltage output though...
    - Broke LM5156 by continuing to increase voltage even when there is no output
        - Order a new IC and test using 469 gate driver circuit for now

<a name="10/26/21"></a> 
# 10/26/21
## Objectives
1) Configure microcontroller for internal clock
2) Document changes for pcb

1) Fuse settings
    - Configure fuses on ATmega32u4 for internal oscillator
    - https://arduino.stackexchange.com/questions/58456/32u4-using-internal-clock-on-custom-pcb-loading-sketch
    - U lfuse:w:0xE2:m -U hfuse:w:0xD8:m -U efuse:w:0xCB:m 

2) PCB Revisions
    - RCS connection
    - LM5156 footprint
    - Change relay pins
    - Include external oscillator circuit (ceramic through hole cap)
    - Vout- feedback connection
    - Change current sensor cap to through hole
    - Change q1 and q3 routing
    - Add backup driver for boost
    - Potentially add microcontroller test points
    - Add RC filters for q1, q3 and qboost

<a name="10/29/21"></a> 
# 10/29/21
## Objectives
1) Test inverter

1) Inverter testing
    - Used wrong timer/microcontroller output pin for q3
        - For now just use worse switching control; will be distorted but should still work
    - Need to adjust switching period
        - Clock frequency should be 8MHz
            - Change ICR to 400 for 20kHz switching
            - Don't forget to scale look up table values as well
            - Needed to change count variable to volatile
                - Documentation explanation: http://medesign.seas.upenn.edu/index.php/Guides/MaEvArM-interrupts
    - Successfully ran with 50% duty cycle at 20kHz
    - Successfully ran with q1 60.2 Hz sinusoidal input
    - Successfully configured q3 to square wave so output will be approximately sinusoidal (less efficient method but will have to go with until new pcb)
    - Did we order a fuse?
```
/*
 * sPWMv2.c
 *
 * Created: 31/12/2014 3:44:43 PM
 *  Author: Kurt Hutten
 sPWM on the atMega328 for the arduino Uno, might be compatable with other atmel chips / arduino boards.
 Compare outputs A and B output to PORTB pins 1 and 2 which are pins 9 and 10 respectively on the Uno.
 Also useful to know the led on the Uno is pin 5 on PORTB.
 */ 

/*
 * Initial Edits: 17/10/2021
 * Author: Elisa Krause
 * Adjusted code for ATmega32u4 with a q1 all positive sinusoidal signal and a q3 square wave.
 * This version is for an internal clock configuration with fclk = 8MHz
 */


#include <avr/io.h>
#include <avr/interrupt.h>

volatile long unsigned count;

// Look up tables with 256 entries each, normalised to have max value of 800 which is the period of the PWM loaded into register ICR1.
int lookUp1[] = {410, 420, 429, 439, 449, 459, 468, 478, 488, 497, 507, 516, 525, 535, 544, 553, 562, 571, 580, 589, 597, 606, 614, 622, 630, 638, 646, 654, 661, 669, 676, 683, 690, 696, 703, 709, 715, 721, 727, 733, 738, 743, 748, 753, 757, 762, 766, 770, 773, 777, 780, 783, 786, 788, 790, 792, 794, 796, 797, 798, 799, 800, 800, 800, 800, 800, 799, 798, 797, 796, 794, 792, 790, 788, 786, 783, 780, 777, 773, 770, 766, 762, 757, 753, 748, 743, 738, 733, 727, 721, 715, 709, 703, 696, 690, 683, 676, 669, 661, 654, 646, 638, 630, 622, 614, 606, 597, 589, 580, 571, 562, 553, 544, 535, 525, 516, 507, 497, 488, 478, 468, 459, 449, 439, 429, 420, 410, 400, 390, 380, 371, 361, 351, 341, 332, 322, 312, 303, 293, 284, 275, 265, 256, 247, 238, 229, 220, 211, 203, 194, 186, 178, 170, 162, 154, 146, 139, 131, 124, 117, 110, 104, 97,  91,  85,  79,  73,  67,  62,  57,  52,  47,  43,  38,  34,  30,  27,  23,  20,  17,  14,  12,  10,  8, 6, 4, 3, 2, 1, 0, 0, 0, 0, 0, 1, 2, 3, 4, 6, 8, 10,  12,  14,  17,  20,  23,  27,  30,  34,  38,  43,  47,  52,  57,  62,  67,  73,  79,  85,  91,  97,  104, 110, 117, 124, 131, 139, 146, 154, 162, 170, 178, 186, 194, 203, 211, 220, 229, 238, 247, 256, 265, 275, 284, 293, 303, 312, 322, 332, 341, 351, 361, 371, 380, 390, 400};
void setup(){
    // Register initilisation, see datasheet for more detail.
    TCCR1A = 0b00100010;
       /*10 clear on match, set at BOTTOM for compA.
         10 clear on match, set at BOTTOM for compB.
         00
         10 WGM1 1:0 for waveform 14.
       */
    TCCR1B = 0b00011001;
       /*000
         11 WGM1 3:2 for waveform 14.
         001 no prescale on the counter.
       */
    TIMSK1 = 0b00000001;
       /*0000000
         1 TOV1 Flag interrupt enable. 
       */
       
    ICR1   = 400;     // Period for 8MHz crystal, for a switching frequency of 20KHz for 200 subdevisions per 50Hz sin wave cycle.
    sei();             // Enable global interrupts.
    DDRB = 0b11000000; // Set PB1 and PB2 as outputs.
}

void loop(){    
    static float angle;
    int index;
    int index2;
    // change duty-cycle every period.
    index = 0.7111*(angle + 1);
 
    if(index + 128 < 256){
       index2 = index + 128;
    }else{
       index2 = index - 128;
    }
 
    //OCR1A = lookUp1[index];
    OCR1B = lookUp1[index]/2;
    //OCR1B = count;
    //PB7 is q3- corresponds to OC0A
    //PB6 is q1- corresponds to 0C1B
    //OCR0A = 25;
    //OCR1B = 200;

    angle = count*1.08;
    if(angle >= 180){
      //set a bit
      PORTB |= (1<<PORTB7);
      
    }else{
      //clear a bit
      PORTB &= ~(1<<PORTB7);
      
    }
    if(angle >= 360){ // Pre-increment num then check it's below 200.
       angle = 0;       // Reset num.
       count = 0;
     } 
}

ISR(TIMER1_OVF_vect){
count = count + 1;
}
```

<a name="11/10/21"></a> 
# 11/10/21

## Objectives
1) Test boost converter with external control

1) Boost converter testing
    - Recalculate snubber values bc low efficiency (around 10%)
        - https://www.ti.com/lit/ug/slvubb4a/slvubb4a.pdf?ts=1636567717749&ref_url=https%253A%252F%252Fwww.ti.com%252Ftool%252FPOWERSTAGE-DESIGNER
        - R = 20
        - C = 470nF
        - Still 25% efficiency
        - reduce R further
    - Keep transformer external so can use current probes to measure transformer current
        - Measurements don't match datasheet turns ratio?
        - Lots of ringing
            - Improved when transformer is soldered onto board

![](/notebooks/elisa/transformer_external.png)    

<a name="11/17/21"></a> 
# 11/17/21
## Objectives
1) Continue testing boost converter

1) Boost converter testing
    - Removed snubber circuit
        - efficiency now 70-80% (yay)
    - Test with LM5156
        - seems to work
        - kept increasing output voltage
            - broke at 30 V...
            - solder LM5156 onto final board and operate at lower voltage (19V)
    - Test with bike in mock demo
        - also added inverter
            - distorted but somewhat worked
    - Still pretty bad ringing

Ringing on switch node (20V divisions):
![](/notebooks/elisa/Large_Ringing.png)

Boost results from mock demo (blue- input voltage, teal- output voltage, purple- output current):
![](/notebooks/elisa/Boost_low_Vin_png)

<a name="11/23/21"></a> 
# 11/23/21
## Objectives
1) Redo PLL calculations/simulations

1) Digial PLL Implementation
- https://cppsim.com/PLL_Lectures/digital_pll_cicc_tutorial_perrott.pdf
- Time to digial converter
    - Use time measurements (look at power calculations for example)
- Figure out appropriate loop filter
- VCO created by adding 60 to whatever error is output from loop filter
- https://www.slideserve.com/tavarius-josiah/phase-locked-loop-powerpoint-ppt-presentation
- phase detector gain (caused me to change to constant pulses): https://www.sciencedirect.com/topics/engineering/phase-detector
- reconsider type of loop filter: https://reader.elsevier.com/reader/sd/pii/S1874610199800076?token=B409DE37A7A9363D5BB2D46EAFF612C7B555FA94905C42B84A36B8AAA4494C63A203C073A38D58D2CDC1B6A82B8D22DC&originRegion=us-east-1&originCreation=20211123214618 

![](/notebooks/elisa/PLL1.JPG)
![](/notebooks/elisa/PLL2.jpg)
![](/notebooks/elisa/PLL3.jpg)
![](/notebooks/elisa/PLL4.jpg)
![](/notebooks/elisa/PLL5.jpg)
![](/notebooks/elisa/PLL6.jpg)


2) Microcontroller Implementation
- index = # of table elements /360 degrees * (angle + 1)

```
/*
 * sPWMv2.c
 *
 * Created: 31/12/2014 3:44:43 PM
 *  Author: Kurt Hutten
 sPWM on the atMega328 for the arduino Uno, might be compatable with other atmel chips / arduino boards.
 Compare outputs A and B output to PORTB pins 1 and 2 which are pins 9 and 10 respectively on the Uno.
 Also useful to know the led on the Uno is pin 5 on PORTB.
 */ 


#include <avr/io.h>
#include <avr/interrupt.h>

long unsigned count;
static float angle;
static bool V1, V2, readyVar;
static double PD, DeltaT,w_dot_dot, w_dot, w, Fsine, y;
int Vgrid, Vfdbk;
static int index, index2;
static unsigned long time1, time2;

// Look up tables with 256 entries each, normalised to have max value of 800 which is the period of the PWM loaded into register ICR1.
int lookUp1[] = {410, 420, 429, 439, 449, 459, 468, 478, 488, 497, 507, 516, 525, 535, 544, 553, 562, 571, 580, 589, 597, 606, 614, 622, 630, 638, 646, 654, 661, 669, 676, 683, 690, 696, 703, 709, 715, 721, 727, 733, 738, 743, 748, 753, 757, 762, 766, 770, 773, 777, 780, 783, 786, 788, 790, 792, 794, 796, 797, 798, 799, 800, 800, 800, 800, 800, 799, 798, 797, 796, 794, 792, 790, 788, 786, 783, 780, 777, 773, 770, 766, 762, 757, 753, 748, 743, 738, 733, 727, 721, 715, 709, 703, 696, 690, 683, 676, 669, 661, 654, 646, 638, 630, 622, 614, 606, 597, 589, 580, 571, 562, 553, 544, 535, 525, 516, 507, 497, 488, 478, 468, 459, 449, 439, 429, 420, 410, 400, 390, 380, 371, 361, 351, 341, 332, 322, 312, 303, 293, 284, 275, 265, 256, 247, 238, 229, 220, 211, 203, 194, 186, 178, 170, 162, 154, 146, 139, 131, 124, 117, 110, 104, 97,  91,  85,  79,  73,  67,  62,  57,  52,  47,  43,  38,  34,  30,  27,  23,  20,  17,  14,  12,  10,  8, 6, 4, 3, 2, 1, 0, 0, 0, 0, 0, 1, 2, 3, 4, 6, 8, 10,  12,  14,  17,  20,  23,  27,  30,  34,  38,  43,  47,  52,  57,  62,  67,  73,  79,  85,  91,  97,  104, 110, 117, 124, 131, 139, 146, 154, 162, 170, 178, 186, 194, 203, 211, 220, 229, 238, 247, 256, 265, 275, 284, 293, 303, 312, 322, 332, 341, 351, 361, 371, 380, 390, 400};

void setup(){
    // Register initilisation, see datasheet for more detail.
    TCCR1A = 0b10100010;
       /*10 clear on match, set at BOTTOM for compA.
         10 clear on match, set at BOTTOM for compB.
         00
         10 WGM1 1:0 for waveform 15.
       */
    TCCR1B = 0b00011001;
       /*000
         11 WGM1 3:2 for waveform 15.
         001 no prescale on the counter.
       */
    TIMSK1 = 0b00000001;
       /*0000000
         1 TOV1 Flag interrupt enable. 
       */
    ICR1   = 3200;     // Period for 16MHz crystal, for a switching frequency of 100KHz for 200 subdevisions per 50Hz sin wave cycle.
    sei();             // Enable global interrupts.
    DDRB = 0b00000110; // Set PB1 and PB2 as outputs for arduino uno
    readyVar = true;
    PD = 0;
    //time period of interrupt for 16MHz crystal
    DeltaT = 0;
    w_dot = 0;
    w = 0;
    Fsine = 60;
    y = 0;
    pinMode(11, OUTPUT);
    pinMode(3, OUTPUT);
    pinMode(4, OUTPUT);
    pinMode(6, OUTPUT);
    time2 = 0;
    Serial.begin(9600);
}

void loop(){    
    time1 = micros();
    DeltaT = (time1-time2);
    time2 = time1;
    //implement loop filter transfer function
    w_dot = PD - 90*w;
    w = w_dot*DeltaT + w;
    Fsine = 135000*w + 6750*w_dot;
    //Fsine = 60;
//    if(Fsine > 61){
//      w_dot = 0; 
//      w = 0.0045185;
      //digitalWrite(3,HIGH);
//    }else if(Fsine < 59){
//      w_dot = 0; 
//      w = 0.00437037;
      //digitalWrite(4,HIGH);
//    }


//Serial.println(y, 8);
    //digitalWrite(3,LOW);
    //digitalWrite(4,LOW);
}

ISR(TIMER1_OVF_vect){
    //digitalWrite(11, 1023);

    //sample voltages at approximately the same time
    Vgrid = analogRead(A1);
    Vfdbk = index;
    // zero crossing detection
    if(Vgrid > 511){
      V1 = true;
    }else{
      V1 = false; 
    }
    // zero crossing detection
    if(Vfdbk < 128){
      V2 = true;
    }else{
      V2 = false;
    }

    if(V1 == false && V2 == false){
      readyVar = true; 
    }
    if(V1 == true && V2 == true){
      readyVar = false;  
    }

    if(readyVar == true){
      if(V1 != V2){
        if(V2 == false){
          //time period
          PD += 0.0002;
          //digitalWrite(11,HIGH);
        }else if(V2 == true){
          PD -= 0.0002;
          //digitalWrite(3, HIGH);
        }
      }
    }else{
        //digitalWrite(11,LOW);
        //digitalWrite(3, LOW);
      }


    //digitalWrite(3, V1);
    //digitalWrite(4, V2);

    //index = 0.7111*(angle + 1);
 
    if(index < 128){
       index2 = index + 128;
    }else{
       index2 = index - 128;
    }
    OCR1A = lookUp1[index]*4;
    OCR1B = lookUp1[index2]*4;

    //angle = count*0.018*Fsine;
    //index = count*0.0128*Fsine;
    y = Fsine*0.0002 + y;
    index = y*256 - 1;
    //index = (Fsine*0.0002 + index)*0.0512;
    //index = count*0.0512*Fsine;
    
    if(index >= 256){ // Pre-increment num then check it's below 200.
       //index = 0;       // Reset num.
       y = 0;
       //count = 0;
    } 
        //digitalWrite(3, V1);
        //digitalWrite(4, V2);
        //digitalWrite(11,readyVar);
        //digitalWrite(6, LOW);
        //digitalWrite(11, 0);
        //digitalWrite(11,LOW);
        //digitalWrite(3, LOW);
}
```


![](/notebooks/elisa/PLL7.jpg)
![](/notebooks/elisa/PLL8.jpg)

<a name="11/29/21"></a> 
# 11/29/21
## Objectives
1) Integrate total system

1) Integration
    - Decided not to include secondary output (turns ratio wasn't correct)
        - Removing second transformer improved ringing

Final inverter output (blue- input voltage, teal- output voltage, purple- output current):
![](/notebooks/elisa/Results.png)
