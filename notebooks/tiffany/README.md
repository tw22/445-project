## Tiffany's Worklog 

* [2021/09/10](#2021/09/10)
* [2021/09/20](#2021/09/20)
* [2021/09/21](#2021/09/21)
* [2021/09/22](#2021/09/22)
* [2021/09/26](#2021/09/26)
* [2021/09/28](#2021/09/28)
* [2021/09/29](#2021/09/29)
* [2021/09/30](#2021/09/30)
* [2021/10/04](#2021/10/04)
* [2021/10/05](#2021/10/05)
* [2021/10/07](#2021/10/07)
* [2021/10/10](#2021/10/10)
* [2021/10/11](#2021/10/11)
* [2021/10/12](#2021/10/12)
* [2021/10/19](#2021/10/19)
* [2021/10/20](#2021/10/20)
* [2021/10/21](#2021/10/21)
* [2021/10/26](#2021/10/26)
* [2021/10/27](#2021/10/27)
* [2021/10/28](#2021/10/28)
* [2021/10/29](#2021/10/29)
* [2021/10/30](#2021/10/30)
* [2021/10/31](#2021/10/31)
* [2021/11/01](#2021/11/01)
* [2021/11/02](#2021/11/02)
* [2021/11/03](#2021/11/03)
* [2021/11/04](#2021/11/04)
* [2021/11/07](#2021/11/07)
* [2021/11/08](#2021/11/08)
* [2021/11/09](#2021/11/09)
* [2021/11/10](#2021/11/10)
* [2021/11/11](#2021/11/11)
* [2021/11/14](#2021/11/14)
* [2021/11/15](#2021/11/15)
* [2021/11/16](#2021/11/16)
* [2021/11/17](#2021/11/17)
* [2021/11/18](#2021/11/18)
* [2021/11/19](#2021/11/19)
* [2021/11/28](#2021/11/28)
* [2021/11/29](#2021/11/29)

<a name="2021/09/10"></a>
# 2021/09/10

## Objectives:
* Metering and display research for electricity generated
* Sufficient information for part of proposal block diagram for metering and display 


**(1) measure power generation at the moment (in Watts)**

* get microcontroller (Arduino) to read the measurements every __x__ seconds 
* use an **internal** (timer) interrupt
    * polling would consume an unnecessary amount of energy
    * polling would also slow down the microcontroller
    * Polling vs Interrupts
        * https://www.geeksforgeeks.org/difference-between-interrupt-and-polling/ 
        * https://avrgeeks.com/polling-vs-interrupt/ 
    * internal interrupt with timer:
        * https://www.hobbytronics.co.uk/arduino-timer-interrupts 
        * https://www.instructables.com/Create-Internal-Interrupt-In-Arduino/ 

**(2) during the above interrupt function, also calculate energy generated within those __x__ seconds and add to total power generated**

**(3) update display device with new total energy generated**

***button input-- **external** interrupt
* use an external interrupt of higher order than the previously mentioned interrupt
* button press will reset the total power generated variable back to 0

https://www.instructables.com/Simple-Arduino-Home-Energy-Meter/ 
* someone’s Arduino code from online to calculate power (watts)
* uses current sensor transformer (“CT sensor”) and LCD screen (we could use a different display type)
* general info about how to use CT sensor with Arduino: 
    * https://learn.openenergymonitor.org/electricity-monitoring/ct-sensors/interface-with-arduino 

***we need to check with an oscilloscope that we’re interpreting and calculating the values from the current sensor transformer correctly

general info about CT sensor (current transformer sensor):
* https://learn.openenergymonitor.org/electricity-monitoring/ct-sensors/introduction 
* measures alternating current
* “particularly useful for measuring whole building electricity consumption or generation”

current sensor installation:
from https://learn.openenergymonitor.org/electricity-monitoring/ct-sensors/installation \
![Current sensor with wire](/notebooks/tiffany/current sensor with wire.png)

* ~5V battery power source for our microcontroller \

<a name="2021/09/20"></a>
# 2021/09/20

## Objectives:
* in-depth research on microcontroller (ATmega328P), current sensor, and voltage sensors
* find products on DigiKey that match requirements needed for current sensor
* derive voltage divider circuit used for voltage sensor (maximum 5V output)

**(1) Microcontroller**
* program ATmega328P with Arduino Uno
    * Arduino Uno: https://store-usa.arduino.cc/products/arduino-uno-rev3 
    * ATmega328P datasheet: https://www.microchip.com/en-us/product/ATmega328 
* voltage info about ATmega328P
    * maximum input voltage into ATmega328P: -0.5V to (Vcc+0.5V)
    * microcontroller Vcc operating voltage range: 1.5V to 5.5V
    *** -> instead of using an operating voltage of 3.3V as mentioned in the proposal, use an operating voltage of 5V so that we can have a larger maximum input voltage into ATmega328P and so a higher resolution for reading analog input information**

ATmega328P pin-out:
![ATmega328P pin-out](/notebooks/tiffany/atmega328 pinout.png)

**(2) Current Sensor (AC)-- inverter output**
* **device must handle up to at least 3A (peak) AC current**
* based on inverter output requirements:
    * 200W @ 120 Vrms ± 10%, PF ≥ 0.9
* calculations: S = 222.22 VA for P = 200W at PF = 0.9
    * want to handle maximum apparent power
    * S = Vrms*Irms
    * Irms = 1.68A to 2.06A
    * maximum of I_peak = 2.06*sqrt(2) = 2.913 A ~ 3A
* possible products to use
    * ACHS-7121-000E
        * https://www.digikey.com/en/products/detail/broadcom-limited/ACHS-7121-000E/9178254 
        * $4.38 for 1
        * supply voltage Vcc = 4.5-5.5V (aka operating voltage)
        * linear relation: -10A -> 0.75V, 10A -> 4.25V
        * max frequency: 80kHz
        * accuracy = +/-1.5%
    * ACS709LLFTR-20BB-T
        * https://www.digikey.com/en/products/detail/allegro-microsystems/ACS709LLFTR-20BB-T/2195927 
        * $4.11 for 1
        * supply voltage Vcc = 3-5.5V (aka operating voltage)
        * linear relation (input current -> output voltage): -20A -> 0V, 20A -> Vcc
        * max frequency: 120 kHz
        * accuracy = +/-2%

**(3) Voltage Sensor (DC or AC)**
* **sensor must handle up to at least 170V-peak AC/DC voltage**
    * see calculations below
    * use voltage divider:
        * R1 = 50 MOhm, R2 = 1.5 MOhm
        * maximum of 4.95V for analog input into microcontroller pin
        * total power dissipated in resistors: 2.74 mW
            * P1 = 2.72 mW
            * P2 = 16.5 uW
        * mapping:
            * Vin = 0 to 170V
            * Vout = 0 to 4.95V
* possible products
    * 50 MOhm resistor:
        * https://www.digikey.com/en/products/detail/stackpole-electronics-inc/HVA12FA50M0/6195874 
        * $0.67 for 1
        * P_rated = 1.2W
    * 1.5 MOhm resistor:
        * https://www.digikey.com/en/products/detail/yageo/CFR-12JB-52-1M5/3921 
        * $0.10 for 1
        * P_rated = 0.167W

Voltage divider calculations: \
![voltage divider calculations](/notebooks/tiffany/voltage divider work.jpg)

<a name="2021/09/21"></a>
# 2021/09/21

## Objectives:
* new voltage sensor module

**Reasoning for new voltage sensor**
* using voltage divider for high voltages is dangerous in case one of the resistors fail...
* 50 MOhms and 1 MOhms are way too large of resistors (physically large)
* I forgot about the negative voltages that we will have to deal with, due to the voltage signal being AC-- recalculculation needed if going with the voltage sensor

**(1) High voltage sensors**
* not purchasable at a cheap-enough cost, and high voltage AC sensors are not available on Digikey or Adafruit
* it would be the safer option, but also, the datasheets are kind of sketchy online
* would probably be more accurate compared

**(2) Power transformers**
* not purchasable at a cheap-enough cost
* voltage decreases aren't big enough (still a large voltage to deal with)
* can't find many that would take 120Vrms (170 Vpeak)

**(3) Voltage divider + voltage source**
* need to step down from -170,170V to -2.5,2.5V. then add 2.5VDC to the signal to make the AC signal non-negative
* adding 2.5VDC
    * at first, I considered a voltage divider from the 5VDC supply, but after research, it is not a good idea to use a voltage divider for a voltage source
    * new idea: voltage regulator-- 5VDC supply -> 2.5VDC
    * voltage regulator:
        * https://www.digikey.com/en/products/detail/torex-semiconductor-ltd/XC6215B252MR-G/2138121 
        * maximum 6V -> regulate to 2.5V (fixed)
        * $0.75 for 1
    * these are all invalid solutions and don't work-- to shift the voltage, we need a level-shifting op-amp. we don't need a 2.5V supply for that, just the 5V supply
        * we still need the voltage regulator to shift the voltage, though
* new resistor values: R1 = 150kOhm, R2 = 2kOhm (calculation below)
    * R1
        * https://www.digikey.com/en/products/detail/stackpole-electronics-inc/RNF14FTD150K/1706643 
        * $0.10 for 1
        * PR = 0.25W
        * error: +/- 1%
            * 148.5 - 151.5 kV
    * R2
        * https://www.digikey.com/en/products/detail/stackpole-electronics-inc/RNF14FTD2K00/1682155 
        * $0.10 for 1
        * PR = 0.25W
        * error: +/- 1%
            * 1.98 - 2.02 kV
    * total power dissipated in resistors: 190.5 mW
        * P1 = 188 mW
        * P2 = 2.5 mW
    * mapping:
        * Vin = 0 to 170V
        * Vout = 0 to ___
            * R1 = 150 kOhm, R2 = 2 kOhm (no error)
                * max Vout + 2.5 = 4.737V
            * R1 = 148.5 kOhm, R2 = 1.98 kOhm
                * max Vout + 2.5 = 4.737V
            * R1 = 148.5 kOhm, R2 = 2.02 kOhm
                * max Vout + 2.5 = 4.781V
            * R1 = 151.5 kOhm, R2 = 1.98 kOhm
                * max Vout + 2.5 = 4.693V
            * R1 = 151.5 kOhm, R2 = 2.02 kOhm
                * max Vout + 2.5 = 4.737V
        * add 2.5V to the voltage divider value -> range is 0.263V to 4.737V
        * final Vout values all within the maximum microcontroller input value

Voltage divider calculations: \
![voltage divider calculations](/notebooks/tiffany/voltage divider work2.jpg)

* **conclusion solution for voltage sensing:**
    * 1. input of 170V AC or DC
    * 2. voltage divider to get from -170 to 170V -> -2.5 to 2.5V
        * R1 = 150kOhm, R2 = 2kOhm
    * add +2.5 voltage source to increase signal to non-negative values. use level-shift with op-amp
        * need 5V supply
        * need 2.5VDC that can be obtained with voltage regulator

<a name="2021/09/22"></a>
# 2021/09/22

## Objectives:
* level shifter circuit

**update: no voltage divider needed for level shift op-amp**
* that is because we can use RRIO op-amp to only need the 5V supply
* op-amp to use: LMV358
    * https://www.digikey.com/en/products/detail/texas-instruments/LMV358QDDUR/697825 
* circuit and simulation using .MOD file of LMV358: \
**with input -2.5V to 2.5V** \
![](/notebooks/tiffany/ltspice_level_shifter_circuit_too_low.png) \
**with input -3V to 3V** \
![](/notebooks/tiffany/ltspice_level_shifter_circuit.png) \
* because of non-idealities of the LMV358, we need our input to be -3V to 3V instead of -2.5V to 2.5V
* recalculation of resistor values to obtain -3V to 3V sinusoid:
    * R1 = 150 kOhms -> P_r1 = 186 mW -> I = 1.13 mA -> R2 = 2.695 kOhm
    * let R1 = 150 kOhm, R2 = 2.5 kOhm
        * need 2.5 kOhm: https://www.digikey.com/en/products/detail/vishay-dale/RN60D2501FB14/3193892 
            * $0.56 for 1
            * error: +/- 1%
            * P_r = 0.25W
        * I = 1.11 mA
        * V_out = 2.79V
        * error analysis:
            * Vout goal = -3V to 3V
                * R1 = 150 kOhm, R2 = 2 kOhm (no error)
                    * Vout = 2.79V
                    * P_total = 190 mW
                * R1 = 148.5 kOhm, R2 = 2.475 kOhm
                    * Vout = 2.79V
                * R1 = 148.5 kOhm, R2 = 2.525 kOhm
                    * Vout = 2.84V
                * R1 = 151.5 kOhm, R2 = 2.475 kOhm
                    * Vout = 2.73V
                * R1 = 151.5 kOhm, R2 = 2.525 kOhm
                    * Vout = 2.79V
            * error range is appropriate based on simulation above

<a name="2021/09/26"></a>
# 2021/09/26

## Objectives:
* design document-- additional research

* wiring of the 4-digit 7-segment display: https://www.onetransistor.eu/2018/12/wiring-of-4digit-7segment-display.html 
* previous current sensor that I selected is no longer in stock
    * new current sensor: https://www.digikey.com/en/products/detail/allegro-microsystems/ACS718KMATR-10B-T/5125777 
    * -10A to 10A
    * datasheet includes wiring
* on/off switch:
    * https://www.digikey.com/en/products/detail/cit-relay-and-switch/ANT11SEBQE/12503360
    * handles 120VAC and 5 A (AC/DC)
* microcontroller-- we did not have enough pins because we underestimated how many pins we needed
    * from 28 pins to 44 pins: chose ATmega16U4
    * https://www.digikey.com/en/products/detail/microchip-technology/ATMEGA16U4-AUR/2238235


<a name="2021/09/28"></a>
# 2021/09/28

## Objectives:
* change the voltage sensors-- the inverter output is not with respect to ground, so we need a differential voltage op-amp

* differential op-amp: https://www.electronicshub.org/differential-amplifier/ 
* .MOD file for the LMV358 is innaccurate-- the V+ and V- pins are not equal
* needed to simulate the equivalent LM741 circuit set-up to get the appropriate output
The first graph shows that the V+ and V- terminals are not the same, which does not match how op amps operate. The .MOD file does not reflect the behavior of an op amp
![](/notebooks/tiffany/lmv358_simulation.png) \
This simulation below is done using the LM741, and the op amp is set up to be equivalent to the previous, except not using a rail to rail op-amp, but they should still operate the same. We will still use the LMV358, but since the .MOD file does not work correctly for the LMV358 for this simulation, the behavior of the circuit is simulated using the equivalent circuit with the LM741
![](/notebooks/tiffany/lm741_simulation.png) \
* this differential op-amp acts as a voltage divider, too. but i'd like to use the original voltage divider set-up for the DC voltage, then (cheaper)
* next time: need to fix the resistance values of the voltage divider for DC
* CONCLUSION FOR VOLTAGE SENSING:
    * grid and inverter outputs-- use differential op-amp above combined with level-shifter from before
    * DC boost output-- use voltage divider
        * (need to fix resistance values-- no longer need to handle negative values, and no longer need a level-shifter)

<a name="2021/09/29"></a>
# 2021/09/29

## Objectives:
* determine resistance values for voltage sensors (reasonable magnitudes, low power dissipation)

* for DC, 0 to 170 range: attempt to map to range 0 to 5
    * calculations:
        * 165V / R_1 = I. P = I^2*R <= 0.25W
        * 165V / (0.25/I^2) <= I
        * 165V * I^2 / 0.25 <= I
        * 165*I / 0.25 <= 1
        * I <= 1.515 mA, R_1 >= 108.9 kOhms
        * choose R_1 = 150kOhm -> I = 1.1 mA
        * 5V = I*R_2
        * R_2 = 4.545 kOhms
    * **CONCLUSIONs: R_1 = 150 kOhms, R_2 = 4.3 kOhms, mapping range: 0V to 4.74V**
        * I = 1.10175 mA
        * P_1 = 182 mW, P_2 = 5.22 mW (power dissipated by each resistor)
        * https://www.digikey.com/en/products/detail/yageo/RC1206FR-074K3L/728872 
            * +/- 1% tolerance
            * P_r = 0.25 W
            * size 1206
            * $0.10 for one

* for AC: this is the output of the combined differential + level-shifter circuit (using LM741-- but in real life, we'll use the LMV358)
![](/notebooks/tiffany/ldifferential_and_level_shift_combined.png)
For a different input sinusoidal voltage of range -170AC to 170AC, it maps to a range of 0.634V to 4.42V.

<a name="2021/09/30"></a>
# 2021/09/30

## Objectives:
* ok so I was wrong about the capabilities of an RRIO op amp...
* new voltage sensor setup, but still using the differential

* earlier today:
    * tried the LM7301 (also a RRIO op amp)-- simulation had same results as LMV358 (V+ and V- not matching -> incorrect output)
    * is this an issue with the simulation software? or will these op amps really not work? I don't get it...

* ok no, fix this sht 
* V+: 1: 0 to 170 sinusoid -> 0 to 2
* level shift to 2 to 4
* V-: 0 to 170 square -> 0 to 2
* -> V+ - V- = 0 to 4

* Step 1: [Voltage Divider] Scale down the magnitude of V+ and V- to a range of 0 to 170V.
* Step 2: [Level-Shifting Op-Amp] Level-shift V+ by 2V so that the range of V+ is now 2V to 4V.
* Step 3: [Differential Op-Amp] Subtract V- from V+ to obtain the scaled-down sinusoidal signal of the inverter/grid output. The inverter/grid * output is -170V to 170V, and this final step maps the signal to a 0V to 4V signal.


* voltage divider: (0 to 170) to (0 to 2)
    * choose R_1 = 150kOhms
        * need to get 168V drop across R_1--> 1.12 mA
        * need to get 2V drop across R_2 --> 1.785 kOhms
    * power dissipation: 
        * R_1 = 150 kOhms, R_2 = 1.74 kOhms
        * I = 1.12 mA
        * P_1 = 188 mW, P_2 = 2.20 mW

* level-shifter: increase by 2V
    * op-amp level-shifter equations: https://daycounter.com/Calculators/Op-Amp/NonInverting-Op-Amp-Resistor-Calculator.phtml 

Voltage Divider:\
![](/notebooks/tiffany/voltage_divider.png)

Level shifter:\
![](/notebooks/tiffany/level_shifter.png)

Differential:\
![](/notebooks/tiffany/differential.png)

Level-shift parameters using https://daycounter.com/Calculators/Op-Amp/NonInverting-Op-Amp-Resistor-Calculator.phtml :\
![](/notebooks/tiffany/level_shift_by_2V.png)

<a name="2021/10/04"></a>
# 2021/10/04

## Objectives:
* Figure out if can combine level-shifter op-amp and voltage divider for V+ signal

**No**
* negative resistance
![](/notebooks/tiffany/level_shift_attempt_170_start.png)

<a name="2021/10/05"></a>
# 2021/10/05

## Objectives:
* create and test the sensing code with Arduino
* map input ADC values to actual values (before signals were scaled/modified)
* graph the input voltage signal

**Overview**
* analog input signal: use voltage generator with Scopy & ADALM2000 or function generator in lab room
    * use sinusoid going from 0V to 5V
* ADC maps 0V to 5V (Vref) with 0 to 1023
    * Arduino ADC info: https://www.electronicwings.com/arduino/adc-in-arduino 
* code: depending on the voltage sensor, map 0 to 1023 to original voltage range (original-- before scaling the signals)
* output a graph of the signal using Arduino's Serial Plotter
    * https://learn.adafruit.com/experimenters-guide-for-metro/circ08-using%20the%20arduino%20serial%20plotter
* also using CAD-Soldering assignment code for reference
    * https://courses.engr.illinois.edu/ece445/wiki/#/soldering/index.md 

## Objectives 2
* create footprints for vishay transformer and wurth transformer

**References**
* https://www.youtube.com/watch?v=ZHH4G_EWhm0 
* length of solder pad for surface mount: (found by Raihana + Elisa)
    * https://www.altronmfg.com/pcb-design-for-manufacturability/ 
    * https://www.smps.us/pcbtracespacing.html

* the vishay one actually had a footprint on digikey, so my footprint doesn't have to be used now ahaha :'D 

![](/notebooks/tiffany/coordinates_for_wurth_transformer.png)
![](/notebooks/tiffany/coordinates_for_vishay_transformer.png)
![](/notebooks/tiffany/wurth_transformer_footprint.png)

<a name="2021/10/07"></a>
# 2021/10/07

## Objectives:
* calculate resistance values for voltage dividers (DC and AC) for testing-- 100V and 50V

* DC: 100V to 5V
    * calculations:
        * <=0.25W power consumption
            * 95V-drop
            * 95V / R_1 = I. P = I^2*R <= 0.25W
            * I <= 0.25/95, R_1 >= 95/I
            * I <= 2.632 mA, R_1 >= 36.1 kOhms
        * choose R_1 = 50 kOhm -> I = 1.9 mA
        * 5V = I*R_2
        * R_2 = 2.5 kOhms
    * CONCLUSIONs: R_1 = 50 kOhms, R_2 = 2.5 kOhms, mapping range: 0V to 4.7619V
        * I = 1.905 mA
        * P_1 = 181 mW, P_2 = 9.07 mW (power dissipated by each resistor)

* AC: 100V to 2V
    * CONCLUSIONs: R_1 = 50 kOhms, R_2 = 1 kOhm, mapping range: 0V to 1.9608V
    * 1st resistor is same as DC one. 2nd resistor is 2/5th of the DC one
    * R_1 = 50 kOhm, R_2 = 1 kOhm
    * power dissipation: 
        * I = 1.96 mA
        * P_1 = 192 mW, P_2 = 3.84 mW

* DC: 50V to 5V
    * calculations:
        * <=0.25W power consumption
            * 45V-drop
            * 45V / R_1 = I. P = I^2*R <= 0.25W
            * I <= 0.25/45, R_1 >= 45/I
            * I <= 5.556 mA, R_1 >= 8.1 kOhms
        * choose R_1 = 10 kOhm -> I = 4.5 mA
        * 5V = I*R_2
        * R_2 = 1.111 kOhms
    * CONCLUSIONs: R_1 = 10 kOhms, R_2 = 1 kOhms, mapping range: 0V to 4.545V
        * I = 4.54545 mA
        * P_1 = 207 mW, P_2 = 20.7 mW (power dissipated by each resistor)

* AC: 50V to 2V
    * CONCLUSIONs: R_1 = 10 kOhms, R_2 = 400 Ohm, mapping range: 0V to 1.923V
    * 1st resistor is same as DC one. 2nd resistor is 2/5th of the DC one
    * R_1 = 10 kOhm, R_2 = 400 Ohm
    * power dissipation: 
        * I = 4.81 mA
        * P_1 = 231 mW, P_2 = 9.25 mW

<a name="2021/10/10"></a>
# 2021/10/10

## Objectives:
* write up sensing code with Arduino
* figure out set-ups / connections for ADALM2000 & Scopy & Arduino

![](/notebooks/tiffany/code1.png)

* Pinout for ADALM2000 pins: https://wiki.analog.com/university/tools/m2k/devs/intro 

<a name="2021/10/11"></a>
# 2021/10/11

## Objectives:
* revisions with code, testing with ALDAM2000

Revisions / Comments
* changed Serial monitor baud rate to 2,000,000-- still not fast enough to capture 60 Hz, though
    * Serial.begin(2000000);
* changed Serial.print line to occur only once (Serial.println and Serial.print is just repeated, except one is a new line)
* Comment: 0V input does not correspond to ADC value of 0 for some reason
* Comment: need to smooth out the noise somehow (very staticy)

<a name="2021/10/12"></a>
# 2021/10/12

## Objectives:
* include variables to account for non-idealities for Arduino into code

Revisions:
* added variables x and y for Arduino non-idealities
    * mitigate issue from yesterday where 0V input does not correspond to ADC value of 0, and 5V does not correspond to ADC 1023
* removed variable assignment in setup code 

![](/notebooks/tiffany/code2.png)

<a name="2021/10/19"></a>
# 2021/10/19

## Objectives:
* recalculate values / finalize values based on surface mount resistors that we have

Voltage Dividers
* for DC, 0 to 170 range: attempt to map to range 0 to 5
    * R_1 = 150 kOhms, R_2 = 4.3 kOhms, mapping range: 0V to 4.74V
* for AC, 0 to 170 range: attempt to map to range 0 to 2
        * R_1 = 150 kOhms, R_2 = 1.8 kOhms, mapping range: 0V to 2.016V

Level shifter & differential op-amp
![](/notebooks/tiffany/rev3_differential.png)
![](/notebooks/tiffany/rev3_level_shift.png)

Links for Arduino pin-outs 
* https://community.arduboy.com/t/128-x-128-screen/8854/46
* https://www.arduino.cc/en/Hacking/PinMapping32u4
* https://github.com/DeanIsMe/SevSeg
* https://www.arduino.cc/en/Hacking/PinMapping32u4
* https://www.circuitbasics.com/arduino-7-segment-display-tutorial/
* https://www.circuito.io/blog/arduino-uno-pinout/

<a name="2021/10/20"></a>
# 2021/10/20

## Objectives:
* ---

Test transistors
* (with continuity mode on multimeter) https://vetco.net/blog/test-a-transistor-with-a-multimeter/2017-05-04-12-25-37-07 

New resistor values and ranges
* new AC range-- up to 4.037V
* prev DC range: 4.7375V

I fcked up so bad. atmega16u can't be programmed regularly with the Arduino...
* reference: https://hardwarefun.com/tutorials/use-arduino-code-in-non-arduino-avr-microcontroller 
* below: revised pins_arduino.h file
```
/**
  pins_arduino.h - Pin definition functions for Arduino for ATmega 16
  Based on the code from the following places
  - Alternate core - http://www.avr-developers.com/corefiles/index.html
  - Arduino - http://www.arduino.cc/
  @author: Sudar <http://hardwarefun.com>
*/

#ifndef Pins_Arduino_h
#define Pins_Arduino_h

#include <avr/pgmspace.h>

#define NUM_DIGITAL_PINS            26
#define NUM_ANALOG_INPUTS           6
#define analogInputToDigitalPin(p)  ((p < 8) ? (p) + 24 : -1)
#define digitalPinHasPWM(p)         ((p) == 7 || (p) == 6 || (p) == 5 || (p) == 9 || (p) == 8 || (p) == 17 || (p) == 16 || (p) == 10)
	//PWM's: PB7, PB6, PB5, PC7, PC6, PD7, PD6, PD0

// ***** REFER TO digital_pin_to_port_PGM SECTION FOR CORRESPONDING NUMBERS ***** //

static const uint8_t SS   = 0;
static const uint8_t MOSI = 2;
static const uint8_t MISO = 3;
static const uint8_t SCK  = 1;

static const uint8_t SDA = 11;
static const uint8_t SCL = 10;
//static const uint8_t LED_BUILTIN = 13; // We don't have a built-in LED, but still ..

static const uint8_t A0 = 25;
static const uint8_t A1 = 24;
static const uint8_t A2 = 23;
static const uint8_t A3 = 22;
static const uint8_t A4 = 21;
static const uint8_t A5 = 20;

//TODO: these are not assigned properly. Need to test more
#define digitalPinToPCICR(p)    (((p) >= 0 && (p) <= 32) ? (&PCICR) : ((uint8_t *)0))
#define digitalPinToPCICRbit(p) (((p) <= 7) ? 2 : (((p) <= 13) ? 0 : 1))
#define digitalPinToPCMSK(p)    (((p) <= 7) ? (&PCMSK2) : (((p) <= 13) ? (&PCMSK0) : (((p) <= 21) ? (&PCMSK1) : ((uint8_t *)0))))
#define digitalPinToPCMSKbit(p) (((p) <= 7) ? (p) : (((p) <= 13) ? ((p) - 8) : ((p) - 14)))

#ifdef ARDUINO_MAIN

// digital pins are also used for the analog output (software PWM).
// Analog input pins are a separate set.


// these arrays map port names (e.g. port B) to the
// appropriate addresses for various functions (e.g. reading
// and writing)
const uint16_t PROGMEM port_to_mode_PGM[] = {
	NOT_A_PORT,
	(uint16_t) &DDRB,
	(uint16_t) &DDRC,
	(uint16_t) &DDRD,
	(uint16_t) &DDRE,
	(uint16_t) &DDRF,
};

const uint16_t PROGMEM port_to_output_PGM[] = {
	NOT_A_PORT,
	(uint16_t) &PORTB,
	(uint16_t) &PORTC,
	(uint16_t) &PORTD,
	(uint16_t) &PORTE,
	(uint16_t) &PORTF,
};

const uint16_t PROGMEM port_to_input_PGM[] = {
	NOT_A_PIN,
	(uint16_t) &PINB,
	(uint16_t) &PINC,
	(uint16_t) &PIND,
	(uint16_t) &PINE,
	(uint16_t) &PINF,
};

const uint8_t PROGMEM digital_pin_to_port_PGM[] = {
	PB, /* 0   PB0 */
	PB,
	PB,
	PB,
	PB,
	PB,
	PB,
	PB,
	PC, /* 8   PC6*/
	PC,      //PC7
	PD, /* 10  PD0*/
	PD,
	PD,
	PD,
	PD,
	PD,
	PD,
	PD,
	PE, /* 18  PE0*/
	PE,
	PF, /* 20  PF0*/
	PF,	 //PF1
	PF,	 //PF4
	PF,	 //PF5
	PF,	 //PF6
	PF,	 //PF7
};

const uint8_t PROGMEM digital_pin_to_bit_mask_PGM[] = {
	_BV(0), /* 0, port B */
	_BV(1),
	_BV(2),
	_BV(3),
	_BV(4),
	_BV(5),
	_BV(6),
	_BV(7),
	_BV(0), /* 8, port C */
	_BV(1),
	_BV(0), /* 10, port D */
	_BV(1),
	_BV(2),
	_BV(3),
	_BV(4),
	_BV(5),
	_BV(6), 
	_BV(7),
	_BV(0), /* 18, port E */
	_BV(1),
	_BV(0), /* 20, port F */
	_BV(1),
	_BV(2),
	_BV(3),
	_BV(4),
	_BV(5), 
};

const uint8_t PROGMEM digital_pin_to_timer_PGM[] = {
	NOT_ON_TIMER, 	/* 0 - port B */
	NOT_ON_TIMER,
	NOT_ON_TIMER,
	NOT_ON_TIMER,
	NOT_ON_TIMER,
	TIMER1A,
	TIMER1B,
	TIMER1A,
	TIMER3A, 	/* 8 - port C */
	TIMER4A,
	NOT_ON_TIMER,	/* 10 - port D */
	NOT_ON_TIMER,
	NOT_ON_TIMER,
	NOT_ON_TIMER,
	NOT_ON_TIMER,
	NOT_ON_TIMER,
	NOT_ON_TIMER,
	TIMER4D,
	NOT_ON_TIMER,	/* 18 - port E */
	NOT_ON_TIMER,
	NOT_ON_TIMER,	/* 20 - port F */
	NOT_ON_TIMER,
	NOT_ON_TIMER,
	NOT_ON_TIMER,
	NOT_ON_TIMER,
	NOT_ON_TIMER,
};

#endif

#endif
```

websites referenced / used
* https://hardwarefun.com/tutorials/use-arduino-code-in-non-arduino-avr-microcontroller
* https://github.com/sudar/arduino-extra-cores/blob/master/variants/mega16/pins_arduino.h
* https://github.com/damellis/attiny/blob/master/variants/tiny14/pins_arduino.h
* https://goldfieldupc.com/
* http://ww1.microchip.com/downloads/en/devicedoc/atmel-8154-8-bit-avr-atmega16a_datasheet.pdf 
* https://ww1.microchip.com/downloads/en/DeviceDoc/Atmel-7766-8-bit-AVR-ATmega16U4-32U4_Datasheet.pdf
* https://www.arduino.cc/en/Hacking/PinMapping32u4 

i gotta alter the SevSeg library at some point

<a name="2021/10/21"></a>
# 2021/10/21

## Objectives:
xx

* low and high fuse calculator (used factory default): https://www.engbedded.com/fusecalc/ 
* "upload.maximum_size" is ROM: http://www.blitzlogic.com/avr.htm 
    * or is it "maximum program space"?: http://xed.ch/help/arduino.html 
    * Atmega32U4: 28672 (reference Leonardo and Micro in boards.txt file)
    * Atmega16: 14336 (divided by the 32)
        * so make atmega16U4 as 14336
* appropriate baud rate & clock speed for "upload.speed": https://cache.amobbs.com/bbs_upload782111/files_22/ourdev_508497.html 
* 
LM5156H-- incorrect footprint for our PCB 
* make own footprint:
Coordinates for LM5156H:
![](/notebooks/tiffany/coordinates_for_LM5156.png)
Footprint Editor:
![](/notebooks/tiffany/lm5156footprint.png)

* increased size holes of footprint for Vishay transformer 
    * incorrect hole sizes
    * datasheet hole diameter-- 40 mil 
    * actual hole diameter (caliper measurement): 41.5 mil 

<a name="2021/10/26"></a>
# 2021/10/26

* atmega32u4RC-- supposed to be configured as internal oscillator with default, but it's actually default as external
    * needed an external 16MHz oscillator

* had to write the fuse directly, not through code
* code did not work***

```
void setup() {
  // put your setup code here, to run once:
  
  DDRB = B11000001; //set PB0 as output, others as inputs. set PB6&PB7 also as output

  PORTB |= B11000001; //set pin 7 output as 1 (also set PB6&PB7)

  CLKSEL0 = B10001000;   // Choose internal clock
  
  CLKSEL1 = B00100000; //0010-- set to use internal 8MHz clock
  
  CLKPR = B10000000;  // Prescaler Change Enable (write 1 to CLKPCE)
  CLKPR &= B11110000; //clock division factor = 0 

  //Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:

  delay(1000);

  PORTB ^= B00000001; //toggle PB0

  //Serial.println(PORTB);
}
```

* next: find an appropriate relay
    * the one we currently bought / on our PCB: rated DC voltage of 24VDC
        * what we want: 5VDC
    * also, do additional BJT calculations to amplify the current coming out of the microcontroller
        * based on datasheet: absolute maximum current-- 40mA, but sum of specific ports can't exceed 100mA
            * relay output control signal is on Port B
            * calculation for output to be about 5mA
                * share with about 10 other pins-- so assume shared current draw-- 100mA/10 = 10mA
                * other pins are not stuff that consume that much current, so 10mA is decent estimate
    * general circuit it looks like we reference:
        * https://www.electronics-tutorials.ws/blog/relay-switch-circuit.html 
        * http://arnholm.org/raspberry-pi-controlling-a-relay/
    * relay I'm choosing: https://www.digikey.com/en/products/detail/picker-components/PC312-5H-X/12548903 
        * 5VDC coil voltage
        * 66.7mA coil current (looks like it's based on the 5VDC coil voltage and the 75Ohm coil resistance-- 5/75)
    * that relay above does not work-- 66.7mA is too high
        * https://www.digikey.com/en/products/detail/omron-electronics-inc-emc-div/G5Q-1A4-DC5/1815721 
        * 5VDC coil voltage, 125 Ohm coil resistance -> 40mA coil current
    * 2N2222: https://www.mouser.com/datasheet/2/68/2n2221-1149865.pdf 

Current calculations and transistor modeling
![](/notebooks/tiffany/relay_sim_and_circuit.png)

<a name="2021/10/27"></a>
# 2021/10/27

* modeling a coil relay in LTspice:
    * https://electronics.stackexchange.com/questions/185855/how-can-i-model-a-relay-in-ltspice-iv
    * https://www.analog.com/en/technical-articles/ltspiceiv-voltage-controlled-switches.html 
    * based on datasheet: must-operate voltage = 75% which is 3.75V
Transistor modeling (40mA going through it) with actual relay circuit
![](/notebooks/tiffany/relay_sim_with_actual_circuit.png)
* note: 4.75V should actually be 3.75V-- the behavior is the same regardless

Datasheet-- wiring
* "example of drive circuit"-- used this as a reference
* ****no coil polarity!!

marked-up kicad schematic revision
![](/notebooks/tiffany/revised_kicad_circuit_schem.png)

* To Do 
    * SevSeg library
    * change sensing code to do pins with microcontroller 
        * do not do this yet-- still testing code with microcontroller
    * test individual sensing components 
        * updates: Vout circuit works. Vgrid circuit works(?)
Vout_graphs
![](/notebooks/tiffany/vout_grid1.JPG)
![](/notebooks/tiffany/vout_grid2.JPG)

<a name="2021/10/28"></a>
# 2021/10/28

LM5156H revision
* found this footprint, even though our last footprint was too big? the dimensions on this is scaled correctly to the one we have (I think), BUT the dimensions are slightly off from the datasheet
    * https://app.ultralibrarian.com/details/2ad360a2-0e1e-11eb-9033-0a34d6323d74/Texas-Instruments/LM5156HPWPR?exports=KiCAD&open=exports
* but this footprint made me realize that the footprint I created before didn't have that extra soldering space to the side of the pins (that is, the datasheet didn't give me the information of the traces needed to create the footprint, but rather, what the outlines of the metal parts are)
* revised the footprint:
![](/notebooks/tiffany/LM5156H_revised.png)

Sensing circuit-- does the AC sensing circuit work?
* referencing the V-grid pictures from yesterday
* AC voltage divider:
    * 150k and 1.8k Ohm resistors
    * 0V-10V converts to 0V-0.118577V
* level-shifting op-amp:
    * 0V-0.118577V converts to to 2.0521024V-2.1676298V
* differential op-amp:
    * V+ is 2.0521024V-2.1676298V sinusoid, V- is 0V-0.118577V square wave
    * output should be 1.9459954V-2.1799713V with 233.97593mV peak-peak
    * Note: the reported Vpk-pk value is incorrect on the oscilloscope images above, based on the 500mV and 100mV spacing in the upper-left corner. The Vpk-pk is approximately 200mV based on the spacings

Test-sensing of LTSpice of level-shifter output
![](/notebooks/tiffany/test_sensing_levelshift.png)

Test-sensing of LTSpice of differential output
![](/notebooks/tiffany/test_sensing_differential.png)

<a name="2021/10/29"></a>
# 2021/10/29

Calibration of sensing circuit

* Updated code with new calibration method
```
// Team 3 (TREEE)
// Voltage and Current Sensing


// defining ADC pins being used
#define Vout_sens A5
#define Vdc_sens A4
#define Vgrid_sens A3
#define Iout_sens A2

// store raw ADC in these variables
int Vout_adc;
int Vdc_adc;
int Vgrid_adc;
int Iout_adc;

// non-ideal mapping variables for AC, DC sensing circuit
float inv_0; //output of inverter sensing circuit when -25V potential is applied
float inv_25; //output of inverter sensing circuit when +25V potential is applied
float dc_0; //output of DC sensing circuit when 0V is applied
float dc_25; //output of DC sensing circuit when 25V is applied
float grid_0;
float grid_25;
float i_0; //output of current sensor when 0A is applied to input
float i_10; //output of current sensor when 10A is applied to input

// non-ideal mapping variables for Arduino
float ard_0; //ADC value of microcontroller when 0V is applied to microcon
float ard_5; //ADC value of microcontroller when 5V is applied to microcon

//mapping ADC value to microcontroller input value
float Vard_map_inv;
float Vard_map_dc;
float Vard_map_grid; 
float Vard_map_i;

//slopes based on calibration values
float Vard_slope;
float inv_slope;
float dc_slope;
float grid_slope;
float i_slope;

// original values
float Vout_map;   //inverter output
float Vdc_map;    //DC-DC output
float Vgrid_map;  //grid output
float Iout_map;   //current output

float A; //our mapping value-- 170V (170VAC and 170VDC)

void setup() {

  // configure inputs/outputs
  pinMode(Vout_sens, INPUT);
  pinMode(Vdc_sens, INPUT);
  pinMode(Vgrid_sens, INPUT);
  pinMode(Iout_sens, INPUT);

  //assign non-ideal mapping vars for Arduino
    //ideal Arduino mapping (ADC,V): (1023,5),(0,0)
    // x is ard_5, y is ard_0
    //non-ideal Arduino mapping: (x,5),(y,0)
    //adjusted slope: instead of (5/1023)(ADC), use ((5/(x-y))(ADC)+y)
      //((5/(x-y))*(ADC)+y)

  // comment this out later: for display
  //Serial.begin(2000000);

  A = 170; //mapping our AC to -170V to 170V, and our DC to 0V to 170V

  /* CALIBRATION VALUES */
  ard_5 = 1023;
  ard_0 = 0;
  inv_0 = 0;
  inv_25 = 0;
  dc_0 = 0;
  dc_25 = 0;
  grid_0 = 1.65327;
  grid_25 = 1.88415;
  i_0 = 0;
  i_10 = 0;

  /* calibrated slopes */
  Vard_slope = 5/(ard_5-ard_0);
  inv_slope = 25/(inv_25-inv_0);
  dc_slope = 25/(dc_25-dc_0);
  grid_slope = 25/(grid_25-grid_0);
  i_slope = 10/(i_10-i_0);
  
}

void loop() {
  // read raw ADC values (0 to 1023)
  Vout_adc = analogRead(Vout_sens);
  Vdc_adc = analogRead(Vdc_sens);
  Vgrid_adc = analogRead(Vgrid_sens);
  Iout_adc = analogRead(Iout_sens);

  //ADC -> voltage input into microcontroller
  Vard_map_inv = Vard_slope*Vout_adc+ard_0;
  Vard_map_dc = Vard_slope*Vdc_adc+ard_0;
  Vard_map_grid = Vard_slope*Vgrid_adc+ard_0;
  Vard_map_i = Vard_slope*Iout_adc+ard_0;

  Vout_map = inv_slope*Vard_map_inv-170;
  Vdc_map = dc_slope*Vard_map_dc;
  Vgrid_map = grid_slope*Vard_map_grid-170;
  Iout_map = i_slope*Vard_map_i;


  //Serial Monitor / Plotter
  //Serial.println(Iout_map);
}

```

trying to figure things out with the code
![](/notebooks/tiffany/slopes.png)

calibration values 
* Vgrid_microcon with 25V input-- 1.88428V
* Vgrid_microcon with 0V input-- 1.65338V
* spent rest of day trying to figure out why those 2 values above were 5V at any input sometimes, and then those values above
    * likely a loose connections
* find calibration points another day...

Code above
* I'm not calculating it properly

Vgrid circuit revision
* so it turns out that its inputs aren't like the inverter
* anyway, make V- to be GND and V+ to be the input to the level-shifter, and we're done for the day lol 
* change Vgrid circuit so that it only needs to go through the level-shifter as a -V to +V 
* new:
    * -170V to 170V input -> voltage divider -> -2.0158V to 2.0158V 
    * -2.0158V to 2.0158V -> level-shift by 2V -> 0 to 4-something volts

new sensing circuit w/ revisions:
![](/notebooks/tiffany/new_vgrid_sense.png)

<a name="2021/10/30"></a>
# 2021/10/30

New code
```
// Team 3 (TREEE)
// Voltage and Current Sensing


// defining ADC pins being used
#define Vout_sens A5
#define Vdc_sens A4
#define Vgrid_sens A3
#define Iout_sens A2

// store raw ADC in these variables
int Vout_adc;
int Vdc_adc;
int Vgrid_adc;
int Iout_adc;

// non-ideal mapping variables for AC, DC sensing circuit
float inv_0; //output of inverter sensing circuit when -25V potential is applied
float inv_25; //output of inverter sensing circuit when +25V potential is applied
float dc_0; //output of DC sensing circuit when 0V is applied
float dc_25; //output of DC sensing circuit when 25V is applied
float grid_0;
float grid_25;
float i_0; //output of current sensor when 0A is applied to input
float i_30; //output of current sensor when 10A is applied to input

// non-ideal mapping variables for Arduino
//float ard_0; //ADC value of microcontroller when 0V is applied to microcon
//float ard_5; //ADC value of microcontroller when 5V is applied to microcon

//mapping ADC value to microcontroller input value
float Vard_map_inv;
float Vard_map_dc;
float Vard_map_grid; 
float Vard_map_i;

//slopes based on calibration values
float Vard_slope;
float inv_slope;
float dc_slope;
float grid_slope;
float i_slope;

// original values
float Vout_map;   //inverter output
float Vdc_map;    //DC-DC output
float Vgrid_map;  //grid output
float Iout_map;   //current output

float A; //our mapping value-- 170V (170VAC and 170VDC)

void setup() {

  // configure inputs/outputs
  pinMode(Vout_sens, INPUT);
  pinMode(Vdc_sens, INPUT);
  pinMode(Vgrid_sens, INPUT);
  pinMode(Iout_sens, INPUT);

  //assign non-ideal mapping vars for Arduino
    //ideal Arduino mapping (ADC,V): (1023,5),(0,0)
    // x is ard_5, y is ard_0
    //non-ideal Arduino mapping: (x,5),(y,0)
    //adjusted slope: instead of (5/1023)(ADC), use ((5/(x-y))(ADC)+y)
      //((5/(x-y))*(ADC)+y)

  // comment this out later: for display
  //Serial.begin(2000000);

  A = 170; //mapping our AC to -170V to 170V, and our DC to 0V to 170V

  /* CALIBRATION VALUES */
  //ard_5 = 1023;
  //ard_0 = 0;
  inv_0 = 0;
  inv_25 = 0;
  dc_0 = 0;
  dc_25 = 0;
  grid_0 = 1.65327;
  grid_25 = 1.88415;
  i_0 = 0;
  i_30 = 0; //30mA

  /* calibrated slopes */
  Vard_slope = 5/1023;
  inv_slope = 25/(inv_25-inv_0);
  dc_slope = 25/(dc_25-dc_0);
  grid_slope = 25/(grid_25-grid_0);
  i_slope = 0.03/(i_30-i_0); //30mA
  
}

void loop() {
  // read raw ADC values (0 to 1023)
  Vout_adc = analogRead(Vout_sens);
  Vdc_adc = analogRead(Vdc_sens);
  Vgrid_adc = analogRead(Vgrid_sens);
  Iout_adc = analogRead(Iout_sens);

  //ADC -> voltage input into microcontroller
  Vard_map_inv = Vard_slope*Vout_adc;
  Vard_map_dc = Vard_slope*Vdc_adc;
  Vard_map_grid = Vard_slope*Vgrid_adc;
  Vard_map_i = Vard_slope*Iout_adc;

  Vout_map = (Vard_map_inv-inv_0)*inv_slope;  //sense_out = (inv_25-inv_0)/25 x input + inv_0
  Vdc_map = Vard_map_dc*dc_slope;             //sense_out = (dc_25 - dc_0)/25 x input
  Vgrid_map = (Vard_map_grid-grid_0)*grid_slope;
  Iout_map = Vard_map_i*i_slope;


  //Serial Monitor / Plotter
  //Serial.println(Iout_map);
}
```

Calibration values
* ya know what? save this for after the whole circuit is done

Test code above:
* watched too many horror movies and nobody's in the lab room so i'm gonna leave hahahaha :'D

<a name="2021/10/31"></a>
# 2021/10/31

create symbol for LM5156H
* hot to create symbol: https://www.protoexpress.com/blog/how-to-create-a-schematic-and-symbol-library-kicad/
* symbol reference: https://app.ultralibrarian.com/details/2ad360a2-0e1e-11eb-9033-0a34d6323d74/Texas-Instruments/LM5156HPWPR?exports=KiCAD 
* reference2: (double-check pin names) https://www.ti.com/document-viewer/LM5156H/datasheet/GUID-2CC97BE1-6C4E-49F2-9E45-FA5634B8763E#TITLE-SNVSBT3SNVSAY48492 

<a name="2021/11/01"></a>
# 2021/11/01
* display code now works (W*s -> W*h fix, 2.5V with 1V peak-peak sinuosid for input values -> 48Vpk actual and 4Apk actual)
* DC calibrate:
    * 25V -> 0.693770V
    * 0V -> 0.085 mV (MILLIVOLTS)
    * ---verified: 0.6967 when hand-calculating it
* Current calibrate:
    * normal resistor circuit-- 6.6V supply, 220-Ohm resistance, resulting in 30mA
    * 30mA is burning the resistor a little too warmly... anyway
    * 30mA -> 2.517V
    * 0mA -> 2.511V

Most updated code (w/ Arduino Uno)-- includes calibration data (except inverter) and new calibration equations
* NOTE FOR GRID & INVERTER: these current calibration points have 170V correspond with 3.3V (not a perfect 4 or 5V due to nonidealities) and a center of about 1.6V
```
// Team 3 (TREEE)
// Voltage and Current Sensing


// defining ADC pins being used
#define Vout_sens A5
#define Vdc_sens A4
#define Vgrid_sens A3
#define Iout_sens A2

// store raw ADC in these variables
float Vout_adc;
float Vdc_adc;
float Vgrid_adc;
float Iout_adc;

// non-ideal mapping variables for AC, DC sensing circuit
float inv_0; //output of inverter sensing circuit when -25V potential is applied
float inv_25; //output of inverter sensing circuit when +25V potential is applied
float dc_0; //output of DC sensing circuit when 0V is applied
float dc_25; //output of DC sensing circuit when 25V is applied
float grid_0;
float grid_25;
float i_0; //output of current sensor when 0A is applied to input
float i_30; //output of current sensor when 30mA is applied to input

// non-ideal mapping variables for Arduino
//float ard_0; //ADC value of microcontroller when 0V is applied to microcon
//float ard_5; //ADC value of microcontroller when 5V is applied to microcon

//mapping ADC value to microcontroller input value
float Vard_map_inv;
float Vard_map_dc;
float Vard_map_grid; 
float Vard_map_i;

//slopes based on calibration values
float Vard_slope;
float inv_slope;
float dc_slope;
float grid_slope;
float i_slope;

// original values
float Vout_map;   //inverter output
float Vdc_map;    //DC-DC output
float Vgrid_map;  //grid output
float Iout_map;   //current output

float A; //our mapping value-- 170V (170VAC and 170VDC)

void setup() {
  
  // comment this out later: for display
  //Serial.begin(2000000);

  // configure inputs/outputs
  pinMode(Vout_sens, INPUT);
  pinMode(Vdc_sens, INPUT);
  pinMode(Vgrid_sens, INPUT);
  pinMode(Iout_sens, INPUT);

  //assign non-ideal mapping vars for Arduino
    //ideal Arduino mapping (ADC,V): (1023,5),(0,0)
    // x is ard_5, y is ard_0
    //non-ideal Arduino mapping: (x,5),(y,0)
    //adjusted slope: instead of (5/1023)(ADC), use ((5/(x-y))(ADC)+y)
      //((5/(x-y))*(ADC)+y)

  A = 170; //mapping our AC to -170V to 170V, and our DC to 0V to 170V

  /* CALIBRATION VALUES */
  //ard_5 = 1023;
  //ard_0 = 0;
  inv_0 = 1.65327; //placeholder
  inv_25 = 1.88415; //placeholder
  dc_0 = 0.000085;
  dc_25 = 0.693770;
  grid_0 = 1.65327;
  grid_25 = 1.88415; //NOTE: these current calibration points have 170V correspond with 3.3V (not a perfect 4 or 5V due to nonidealities)
  i_0 = 2.511;
  i_30 = 2.517;

  /* calibrated slopes */
  Vard_slope = 5.0/1023;
  inv_slope = 25.0/(inv_25-inv_0);
  dc_slope = 25.0/(dc_25-dc_0);
  grid_slope = 25.0/(grid_25-grid_0);
  i_slope = 0.03/(i_30-i_0); //30mA
  
}

void loop() {
  // read raw ADC values (0 to 1023)
  Vout_adc = analogRead(Vout_sens);
  Vdc_adc = analogRead(Vdc_sens);
  Vgrid_adc = analogRead(Vgrid_sens);
  Iout_adc = analogRead(Iout_sens);

  //ADC -> voltage input into microcontroller
  Vard_map_inv = Vard_slope*Vout_adc;
  Vard_map_dc = Vard_slope*Vdc_adc;
  Vard_map_grid = Vard_slope*Vgrid_adc;
  Vard_map_i = Vard_slope*Iout_adc;

  Vout_map = (Vard_map_inv-inv_0)*inv_slope;  //sense_out = (inv_25-inv_0)/25 x input + inv_0
  Vdc_map = Vard_map_dc*dc_slope;             //sense_out = (dc_25 - dc_0)/25 x input
  Vgrid_map = (Vard_map_grid-grid_0)*grid_slope;
  Iout_map = (Vard_map_i-i_0)*i_slope;        //sense_out = (i_30-i_0)/.03 x input + i_0


  //Serial Monitor / Plotter
  //Serial.println(Vdc_map);
}
```
<a name="2021/11/02"></a>
# 2021/11/02

Testing combined code for display (sense, calibrate, calculate, display)
* testing DC inputs 2.7V for current and inverter output 
    * result: Vout_map = 112V, Iout_map = 0.88A
    * hand-calculated from code: 113V, Iout_map = 0.945A
    * so power is: ~100W
        * displayed: 98.8W
    * time it (check accumulated energy)-- give one minute (1 minute = 1/60 hr)
        * 100W x (1/60h) = 1.67Wh
        * display: 1.6Wh
* testing AC inputs: Vout microcon input = 2.1Vpp, 1.65V. Iout microcon input = 0.4Vpp, 2.511V
    * ~ average of this signal: 100W / 2 ~ 50W
        * actual output: 60W

Combined code (including Raihana's display&calculation code)-- NOT with updated library parameters
```
// Team 3 (TREEE)
// Voltage and Current Sensing

#include "SevSeg.h"
SevSeg sevseg; 

/***********************************************************************/
/******************* SENSE AND CALIBRATION VARIABLES *******************/
/***********************************************************************/

// defining ADC pins being used
#define Vout_sens A5
#define Vdc_sens A4
#define Vgrid_sens A3
#define Iout_sens A2

// store raw ADC in these variables
float Vout_adc;
float Vdc_adc;
float Vgrid_adc;
float Iout_adc;

// non-ideal mapping variables for AC, DC sensing circuit
float inv_0; //output of inverter sensing circuit when -25V potential is applied
float inv_25; //output of inverter sensing circuit when +25V potential is applied
float dc_0; //output of DC sensing circuit when 0V is applied
float dc_25; //output of DC sensing circuit when 25V is applied
float grid_0;
float grid_25;
float i_0; //output of current sensor when 0A is applied to input
float i_30; //output of current sensor when 30mA is applied to input

// non-ideal mapping variables for Arduino
//float ard_0; //ADC value of microcontroller when 0V is applied to microcon
//float ard_5; //ADC value of microcontroller when 5V is applied to microcon

//mapping ADC value to microcontroller input value
float Vard_map_inv;
float Vard_map_dc;
float Vard_map_grid; 
float Vard_map_i;

//slopes based on calibration values
float Vard_slope;
float inv_slope;
float dc_slope;
float grid_slope;
float i_slope;

// original values
float Vout_map;   //inverter output
float Vdc_map;    //DC-DC output
float Vgrid_map;  //grid output
float Iout_map;   //current output

float A; //our mapping value-- 170V (170VAC and 170VDC)

/*************************************************************************/
/******************* CALCULATION AND DISPLAY VARIABLES *******************/
/*************************************************************************/

//intiliazing values
const int BUTTON_PIN = A0;
const int RESET_PRESS_TIME = 2000;

int buttonMode = 1;
int lastButtonState;  //previous state of button
int currentButtonState; //current state of button
unsigned long pressedTime = 0;
unsigned long releasedTime = 0;
unsigned long final_time = 0; //for energy calculation
unsigned long init_time = 0;  //for energy calculation
unsigned long total_time_elapsed = 0;

int iteration;                  //loop for printing delay
float totPower;                 //total power used to calculate average power
float totalEnergy;              //total energy
float currPower;                //current power
float avgPower_display;         //average power being displayed since it is not constantly changing

const int powerReadings = 10;   // size of array
float readings[powerReadings];  //values from current sensor
int readIndex = 0;              //index of current value
float avgPower = 0;             //average power

/*************************************************************************/
/*************************************************************************/
/*************************************************************************/

void setup() {
  
  // comment this out later: for display
  //Serial.begin(9600);

  // configure inputs/outputs
  pinMode(Vout_sens, INPUT);
  pinMode(Vdc_sens, INPUT);
  pinMode(Vgrid_sens, INPUT);
  pinMode(Iout_sens, INPUT);

  //assign non-ideal mapping vars for Arduino
    //ideal Arduino mapping (ADC,V): (1023,5),(0,0)
    // x is ard_5, y is ard_0
    //non-ideal Arduino mapping: (x,5),(y,0)
    //adjusted slope: instead of (5/1023)(ADC), use ((5/(x-y))(ADC)+y)
      //((5/(x-y))*(ADC)+y)

  A = 170; //mapping our AC to -170V to 170V, and our DC to 0V to 170V

  /* CALIBRATION VALUES */
  //ard_5 = 1023;
  //ard_0 = 0;
  inv_0 = 1.65327; //placeholder
  inv_25 = 1.88415; //placeholder
  dc_0 = 0.000085;
  dc_25 = 0.693770;
  grid_0 = 1.65327;
  grid_25 = 1.88415; //NOTE: these current calibration points have 170V correspond with 3.3V (not a perfect 4 or 5V due to nonidealities)
  i_0 = 2.511;
  i_30 = 2.517;

  /* calibrated slopes */
  Vard_slope = 5.0/1023;
  inv_slope = 25.0/(inv_25-inv_0);
  dc_slope = 25.0/(dc_25-dc_0);
  grid_slope = 25.0/(grid_25-grid_0);
  i_slope = 0.03/(i_30-i_0); //30mA

  /*************************************************************************/
  /*************************************************************************/

  // for display
  byte numDigits = 4;
  byte digitPins[] = {10, 11, 12, 13};
  byte segmentPins[] = {9, 2, 3, 5, 6, 8, 7, 4};
  pinMode(BUTTON_PIN, INPUT_PULLUP);
  bool resistorsOnSegments = true; 
  bool updateWithDelaysIn = true;
  byte hardwareConfig = COMMON_CATHODE; 
  sevseg.begin(hardwareConfig, numDigits, digitPins, segmentPins, resistorsOnSegments);
  sevseg.setBrightness(70);
  bool updateWithDelays = false; // Default 'false' is Recommended
  bool leadingZeros = false; // Use 'true' if you'd like to keep the leading zeros
  bool disableDecPoint = false;
  //Serial.begin(9600);

  buttonMode = 1;
  lastButtonState = HIGH;
  currentButtonState = HIGH;
  totalEnergy = 0;
  currPower = 0;

  for (int i = 0; i < powerReadings; i++){
    readings[i] = 0;
  }

  init_time = millis();
  final_time = 0;
  
  
}

void loop() {
  // read raw ADC values (0 to 1023)
  Vout_adc = analogRead(Vout_sens);
  Vdc_adc = analogRead(Vdc_sens);
  Vgrid_adc = analogRead(Vgrid_sens);
  Iout_adc = analogRead(Iout_sens);

  //ADC -> voltage input into microcontroller
  Vard_map_inv = Vard_slope*Vout_adc;
  Vard_map_dc = Vard_slope*Vdc_adc;
  Vard_map_grid = Vard_slope*Vgrid_adc;
  Vard_map_i = Vard_slope*Iout_adc;

  Vout_map = (Vard_map_inv-inv_0)*inv_slope;  //sense_out = (inv_25-inv_0)/25 x input + inv_0
  Vdc_map = Vard_map_dc*dc_slope;             //sense_out = (dc_25 - dc_0)/25 x input
  Vgrid_map = (Vard_map_grid-grid_0)*grid_slope;
  Iout_map = (Vard_map_i-i_0)*i_slope;        //sense_out = (i_30-i_0)/.03 x input + i_0

  /*************************************************************************/
  /*************************************************************************/

  //Serial Monitor / Plotter
  //Serial.println(Vdc_map);
   currentButtonState = digitalRead(BUTTON_PIN);
    if(lastButtonState == HIGH && currentButtonState == LOW){
      pressedTime = millis();
    }
    else if (lastButtonState == LOW && currentButtonState == HIGH){      
      releasedTime = millis();

      long pressDuration = releasedTime - pressedTime;

      if(pressDuration > RESET_PRESS_TIME) {
        totalEnergy = 0;
        Serial.println("reset");
      }

      else if(pressDuration < RESET_PRESS_TIME){
        if(buttonMode == 1){
          buttonMode = 2;
          //Serial.println("Mode 2");
        }
        else if (buttonMode == 2){
          buttonMode = 1;
          //Serial.println("Mode 1");
        }
        else
          buttonMode = 1;
        //sevseg.setNumberF(10.345, 1);
      }
    }
     switch(buttonMode){
      case 1:
        sevseg.setNumberF(avgPower_display, 1);
        break;
      case 2:
        sevseg.setNumberF(totalEnergy, 1);
        break;
      default:
        sevseg.setNumberF(11.1111, 1);
        break;  
     }
    currPower = Vout_map*Iout_map;  //p=IV 
    //Serial.println(currPower);
    
    totPower = totPower - readings[readIndex];
    readings[readIndex] = currPower;
    totPower = totPower + readings[readIndex];
    readIndex = readIndex + 1;
    if (readIndex >= powerReadings ){
      readIndex = 0;
    }
    avgPower = totPower / powerReadings;
   // delay(1);
    //Serial.println(Iout_map);
    
    lastButtonState = currentButtonState;
    sevseg.refreshDisplay(); 
    iteration++;
    iteration = iteration % 1000;
    if(iteration==0){
      avgPower_display = avgPower;
      //Serial.println(avgPower_display);
      
      final_time = millis();

      total_time_elapsed = final_time - init_time;
      //Serial.println(total_time_elapsed);
      totalEnergy = totalEnergy + (total_time_elapsed)*(avgPower)/(3600000); //division by 3.6e6 converts total time elapsed from ms to hours
      
      init_time = millis();
    }
}
```

Combind code WITH updated library parameters
```
// Team 3 (TREEE)
// Voltage and Current Sensing

#include "SevSeg.h"
SevSeg sevseg; 

/***********************************************************************/
/******************* SENSE AND CALIBRATION VARIABLES *******************/
/***********************************************************************/

// defining ADC pins being used
#define Vout_sens A5
#define Vdc_sens A4
#define Vgrid_sens A3
#define Iout_sens A2

// store raw ADC in these variables
float Vout_adc;
float Vdc_adc;
float Vgrid_adc;
float Iout_adc;

// non-ideal mapping variables for AC, DC sensing circuit
float inv_0; //output of inverter sensing circuit when -25V potential is applied
float inv_25; //output of inverter sensing circuit when +25V potential is applied
float dc_0; //output of DC sensing circuit when 0V is applied
float dc_25; //output of DC sensing circuit when 25V is applied
float grid_0;
float grid_25;
float i_0; //output of current sensor when 0A is applied to input
float i_30; //output of current sensor when 30mA is applied to input

// non-ideal mapping variables for Arduino
//float ard_0; //ADC value of microcontroller when 0V is applied to microcon
//float ard_5; //ADC value of microcontroller when 5V is applied to microcon

//mapping ADC value to microcontroller input value
float Vard_map_inv;
float Vard_map_dc;
float Vard_map_grid; 
float Vard_map_i;

//slopes based on calibration values
float Vard_slope;
float inv_slope;
float dc_slope;
float grid_slope;
float i_slope;

// original values
float Vout_map;   //inverter output
float Vdc_map;    //DC-DC output
float Vgrid_map;  //grid output
float Iout_map;   //current output

float A; //our mapping value-- 170V (170VAC and 170VDC)

/*************************************************************************/
/******************* CALCULATION AND DISPLAY VARIABLES *******************/
/*************************************************************************/

//intiliazing values
const int BUTTON_PIN = A0;
const int RESET_PRESS_TIME = 2000;

int buttonMode = 1;
int lastButtonState;  //previous state of button
int currentButtonState; //current state of button
unsigned long pressedTime = 0;
unsigned long releasedTime = 0;
unsigned long final_time = 0; //for energy calculation
unsigned long init_time = 0;  //for energy calculation
unsigned long total_time_elapsed = 0;

int iteration;                  //loop for printing delay
float totPower;                 //total power used to calculate average power
float totalEnergy;              //total energy
float currPower;                //current power
float avgPower_display;         //average power being displayed since it is not constantly changing

const int powerReadings = 10;   // size of array
float readings[powerReadings];  //values from current sensor
int readIndex = 0;              //index of current value
float avgPower = 0;             //average power

/*************************************************************************/
/*************************************************************************/
/*************************************************************************/

void setup() {
  
  // comment this out later: for display
  //Serial.begin(9600);

  // configure inputs/outputs
  pinMode(Vout_sens, INPUT);
  pinMode(Vdc_sens, INPUT);
  pinMode(Vgrid_sens, INPUT);
  pinMode(Iout_sens, INPUT);

  //assign non-ideal mapping vars for Arduino
    //ideal Arduino mapping (ADC,V): (1023,5),(0,0)
    // x is ard_5, y is ard_0
    //non-ideal Arduino mapping: (x,5),(y,0)
    //adjusted slope: instead of (5/1023)(ADC), use ((5/(x-y))(ADC)+y)
      //((5/(x-y))*(ADC)+y)

  A = 170; //mapping our AC to -170V to 170V, and our DC to 0V to 170V

  /* CALIBRATION VALUES */
  //ard_5 = 1023;
  //ard_0 = 0;
  inv_0 = 1.65327; //placeholder
  inv_25 = 1.88415; //placeholder
  dc_0 = 0.000085;
  dc_25 = 0.693770;
  grid_0 = 1.65327;
  grid_25 = 1.88415; //NOTE: these current calibration points have 170V correspond with 3.3V (not a perfect 4 or 5V due to nonidealities)
  i_0 = 2.511;
  i_30 = 2.517;

  /* calibrated slopes */
  Vard_slope = 5.0/1023;
  inv_slope = 25.0/(inv_25-inv_0);
  dc_slope = 25.0/(dc_25-dc_0);
  grid_slope = 25.0/(grid_25-grid_0);
  i_slope = 0.03/(i_30-i_0); //30mA

  /*************************************************************************/
  /*************************************************************************/

  // for display
  byte numDigits = 4;
  
  byte digitPins[] = {2, 3, 4, 5};
  byte digit_port[] = {1, 1, 1, 1};
  byte segmentPins[] = {1, 2, 3, 5, 6, 0, 7, 4};
  byte segment_port[] = {1, 3, 3, 3, 3, 1, 3, 3};
  
  pinMode(BUTTON_PIN, INPUT_PULLUP);
  bool resistorsOnSegments = true; 
  bool updateWithDelaysIn = true;
  byte hardwareConfig = COMMON_CATHODE; 
  sevseg.begin(hardwareConfig, numDigits, digitPins, segmentPins, digit_port, segment_port, resistorsOnSegments);
  sevseg.setBrightness(70);
  bool updateWithDelays = false; // Default 'false' is Recommended
  bool leadingZeros = false; // Use 'true' if you'd like to keep the leading zeros
  bool disableDecPoint = false;
  //Serial.begin(9600);

  buttonMode = 1;
  lastButtonState = HIGH;
  currentButtonState = HIGH;
  totalEnergy = 0;
  currPower = 0;

  for (int i = 0; i < powerReadings; i++){
    readings[i] = 0;
  }

  init_time = millis();
  final_time = 0;
  
  
}

void loop() {
  // read raw ADC values (0 to 1023)
  Vout_adc = analogRead(Vout_sens);
  Vdc_adc = analogRead(Vdc_sens);
  Vgrid_adc = analogRead(Vgrid_sens);
  Iout_adc = analogRead(Iout_sens);

  //ADC -> voltage input into microcontroller
  Vard_map_inv = Vard_slope*Vout_adc;
  Vard_map_dc = Vard_slope*Vdc_adc;
  Vard_map_grid = Vard_slope*Vgrid_adc;
  Vard_map_i = Vard_slope*Iout_adc;

  Vout_map = (Vard_map_inv-inv_0)*inv_slope;  //sense_out = (inv_25-inv_0)/25 x input + inv_0
  Vdc_map = Vard_map_dc*dc_slope;             //sense_out = (dc_25 - dc_0)/25 x input
  Vgrid_map = (Vard_map_grid-grid_0)*grid_slope;
  Iout_map = (Vard_map_i-i_0)*i_slope;        //sense_out = (i_30-i_0)/.03 x input + i_0

  /*************************************************************************/
  /*************************************************************************/

  //Serial Monitor / Plotter
  //Serial.println(Vdc_map);
   currentButtonState = digitalRead(BUTTON_PIN);
    if(lastButtonState == HIGH && currentButtonState == LOW){
      pressedTime = millis();
    }
    else if (lastButtonState == LOW && currentButtonState == HIGH){      
      releasedTime = millis();

      long pressDuration = releasedTime - pressedTime;

      if(pressDuration > RESET_PRESS_TIME) {
        totalEnergy = 0;
        //Serial.println("reset");
      }

      else if(pressDuration < RESET_PRESS_TIME){
        if(buttonMode == 1){
          buttonMode = 2;
          //Serial.println("Mode 2");
        }
        else if (buttonMode == 2){
          buttonMode = 1;
          //Serial.println("Mode 1");
        }
        else
          buttonMode = 1;
        //sevseg.setNumberF(10.345, 1);
      }
    }
     switch(buttonMode){
      case 1:
        sevseg.setNumberF(avgPower_display, 1);
        break;
      case 2:
        sevseg.setNumberF(totalEnergy, 1);
        break;
      default:
        sevseg.setNumberF(11.1111, 1);
        break;  
     }
    currPower = Vout_map*Iout_map;  //p=IV 
    //Serial.println(currPower);
    
    totPower = totPower - readings[readIndex];
    readings[readIndex] = currPower;
    totPower = totPower + readings[readIndex];
    readIndex = readIndex + 1;
    if (readIndex >= powerReadings ){
      readIndex = 0;
    }
    avgPower = totPower / powerReadings;
   // delay(1);
    //Serial.println(Iout_map);
    
    lastButtonState = currentButtonState;
    sevseg.refreshDisplay(); 
    iteration++;
    iteration = iteration % 1000;
    if(iteration==0){
      avgPower_display = avgPower;
      //Serial.println(avgPower_display);
      
      final_time = millis();

      total_time_elapsed = final_time - init_time;
      //Serial.println(total_time_elapsed);
      totalEnergy = totalEnergy + (total_time_elapsed)*(avgPower)/(3600000); //division by 3.6e6 converts total time elapsed from ms to hours
      
      init_time = millis();
    }
}
```

Revised SevSeg.cpp file (for Atmega328P-- the ports for 32U4 are commented out for now)
```
/* SevSeg Library
 *
 * Copyright 2020 Dean Reading
 *
 * This library allows an Arduino to easily display numbers and letters on a
 * 7-segment display without a separate 7-segment display controller.
 *
 * See the included readme for instructions.
 * https://github.com/DeanIsMe/SevSeg
 */

#include "SevSeg.h"

#define BLANK_IDX 36 // Must match with 'digitCodeMap'
#define DASH_IDX 37
#define PERIOD_IDX 38
#define ASTERISK_IDX 39
#define UNDERSCORE_IDX 40

static const int32_t powersOf10[] = {
  1, // 10^0
  10,
  100,
  1000,
  10000,
  100000,
  1000000,
  10000000,
  100000000,
  1000000000
}; // 10^9

static const int32_t powersOf16[] = {
  0x1, // 16^0
  0x10,
  0x100,
  0x1000,
  0x10000,
  0x100000,
  0x1000000,
  0x10000000
}; // 16^7

// digitCodeMap indicate which segments must be illuminated to display
// each number.
static const uint8_t digitCodeMap[] = {
  // GFEDCBA  Segments      7-segment map:
  0b00111111, // 0   "0"          AAA
  0b00000110, // 1   "1"         F   B
  0b01011011, // 2   "2"         F   B
  0b01001111, // 3   "3"          GGG
  0b01100110, // 4   "4"         E   C
  0b01101101, // 5   "5"         E   C
  0b01111101, // 6   "6"          DDD
  0b00000111, // 7   "7"
  0b01111111, // 8   "8"
  0b01101111, // 9   "9"
  0b01110111, // 65  'A'
  0b01111100, // 66  'b'
  0b00111001, // 67  'C'
  0b01011110, // 68  'd'
  0b01111001, // 69  'E'
  0b01110001, // 70  'F'
  0b00111101, // 71  'G'
  0b01110110, // 72  'H'
  0b00110000, // 73  'I'
  0b00001110, // 74  'J'
  0b01110110, // 75  'K'  Same as 'H'
  0b00111000, // 76  'L'
  0b00000000, // 77  'M'  NO DISPLAY
  0b01010100, // 78  'n'
  0b00111111, // 79  'O'
  0b01110011, // 80  'P'
  0b01100111, // 81  'q'
  0b01010000, // 82  'r'
  0b01101101, // 83  'S'
  0b01111000, // 84  't'
  0b00111110, // 85  'U'
  0b00111110, // 86  'V'  Same as 'U'
  0b00000000, // 87  'W'  NO DISPLAY
  0b01110110, // 88  'X'  Same as 'H'
  0b01101110, // 89  'y'
  0b01011011, // 90  'Z'  Same as '2'
  0b00000000, // 32  ' '  BLANK
  0b01000000, // 45  '-'  DASH
  0b10000000, // 46  '.'  PERIOD
  0b01100011, // 42 '*'  DEGREE ..
  0b00001000, // 95 '_'  UNDERSCORE
};

// Constant pointers to constant data
const uint8_t * const numeralCodes = digitCodeMap;
const uint8_t * const alphaCodes = digitCodeMap + 10;

// SevSeg Constructor
/******************************************************************************/
SevSeg::SevSeg() {
  // Initial value
  ledOnTime = 2000; // Corresponds to a brightness of 100
  waitOffTime = 0;
  waitOffActive = false;
  numDigits = 0;
  prevUpdateIdx = 0;
  prevUpdateTime = 0;
  resOnSegments = 0;
  updateWithDelays = 0;
}


// begin
/******************************************************************************/
// Saves the input pin numbers to the class and sets up the pins to be used.
// If you use current-limiting resistors on your segment pins instead of the
// digit pins, then set resOnSegments as true.
// Set updateWithDelays to true if you want to use the 'pre-2017' update method
// In that case, the processor is occupied with delay functions while refreshing
// leadingZerosIn indicates whether leading zeros should be displayed
// disableDecPoint is true when the decimal point segment is not connected, in
// which case there are only 7 segments.

/************
  digit_port & segment_port added:
  1: Port B
  2: Port C
  3: Port D
  4: Port E
  5: Port F

  values in digitPinsIn[] and segmentPinsIn[] will now correspond to # of pin on port
************/

void SevSeg::begin(uint8_t hardwareConfig, uint8_t numDigitsIn, const uint8_t digitPinsIn[],
                   const uint8_t segmentPinsIn[], const int8_t digit_port_In[], const int8_t segment_port_In[],
                   bool resOnSegmentsIn,bool updateWithDelaysIn, bool leadingZerosIn, bool disableDecPoint) {

  resOnSegments = resOnSegmentsIn;
  updateWithDelays = updateWithDelaysIn;
  leadingZeros = leadingZerosIn;

  numDigits = numDigitsIn;
  numSegments = disableDecPoint ? 7 : 8; // Ternary 'if' statement
  //Limit the max number of digits to prevent overflowing
  if (numDigits > MAXNUMDIGITS) numDigits = MAXNUMDIGITS;

  switch (hardwareConfig) {

    case 0: // Common cathode
      digitOnVal = LOW;
      segmentOnVal = HIGH;
      break;

    case 1: // Common anode
      digitOnVal = HIGH;
      segmentOnVal = LOW;
      break;

    case 2: // With active-high, low-side switches (most commonly N-type FETs)
      digitOnVal = HIGH;
      segmentOnVal = HIGH;
      break;

    case 3: // With active low, high side switches (most commonly P-type FETs)
      digitOnVal = LOW;
      segmentOnVal = LOW;
      break;
  }

  // define the Off-Values depending on the On-Values
  if (digitOnVal == HIGH){
    digitOffVal = LOW;
  } else {
    digitOffVal = HIGH;
  }
  // define the Off-Values depending on the On-Values
  if (segmentOnVal == HIGH){
    segmentOffVal = LOW;
  } else {
    segmentOffVal = HIGH;
  }

  // Save the input pin numbers to library variables
  for (uint8_t segmentNum = 0 ; segmentNum < numSegments ; segmentNum++) {
    segmentPins[segmentNum] = segmentPinsIn[segmentNum];
    segment_port[segmentNum] = segment_port_In[segmentNum];
  }

  for (uint8_t digitNum = 0 ; digitNum < numDigits ; digitNum++) {
    digitPins[digitNum] = digitPinsIn[digitNum];
    digit_port[digitNum] = digit_port_In[digitNum];
  }
  

/************
  case statements for assigning ports
  1: Port B
  2: Port C
  3: Port D
  4: Port E
  5: Port F

  new functions written below (pinMode_new, digitalWrite_new-- include another parameter, port value)
************/

  // Set the pins as outputs, and turn them off
  for (uint8_t digit = 0 ; digit < numDigits ; digit++) {
    pinMode_new(digitPins[digit], digit_port[digit], HIGH); //set as output
    digitalWrite_new(digitPins[digit], digit_port[digit], digitOffVal);
    //pinMode(digitPins[digit], OUTPUT);
    //digitalWrite(digitPins[digit], digitOffVal);
  }

  for (uint8_t segmentNum = 0 ; segmentNum < numSegments ; segmentNum++) {
    pinMode_new(segmentPins[segmentNum], segment_port[segmentNum], HIGH); //set as output
    digitalWrite_new(segmentPins[segmentNum], segment_port[segmentNum], segmentOffVal);
    //pinMode(segmentPins[segmentNum], OUTPUT);
    //digitalWrite(segmentPins[segmentNum], segmentOffVal);
  }

  blank(); // Initialise the display
}

/************
 create functions to replace pinMode and digitalWrite

 pin_num: pin on the port
 port_num: values corresponding with specific ports
 in_out: input = 0, output = 1 set to bit
 lo_hi: output 1 or output 0
************/
void SevSeg::pinMode_new(uint8_t pin_num, uint8_t port_num, uint8_t in_out){
  switch(port_num){
      case 1:
        if(in_out) //if output
          DDRB |= 1<<pin_num; //bit-shifted to appropriate bit
        else //if input
          DDRB &= 0<<pin_num;
        break;
      case 2:
        if(in_out)
          DDRC |= 1<<pin_num;
        else
          DDRC &= 0<<pin_num;
        break;
      case 3:
        if(in_out)
          DDRD |= 1<<pin_num;
        else 
          DDRD &= 0<<pin_num;
        break;
      /*case 4:
        if(in_out)
          DDRE |= 1<<pin_num; 
        else 
          DDRE &= 0<<pin_num;
        break;*/
      /*case 5:
        if(in_out)
          DDRF |= 1<<pin_num; 
        else
          DDRF &= 0<<pin_num;
        break;*/
    }
}
void SevSeg::digitalWrite_new(uint8_t pin_num, uint8_t port_num, uint8_t low_hi){
  switch(port_num){
      case 1:
        if(low_hi) //if output
          PORTB |= 1<<pin_num; //bit-shifted to appropriate bit
        else //if input
          PORTB &= 0<<pin_num;
        break;
      case 2:
        if(low_hi)
          PORTC |= 1<<pin_num;
        else
          PORTC &= 0<<pin_num;
        break;
      case 3:
        if(low_hi)
          PORTD |= 1<<pin_num;
        else 
          PORTD &= 0<<pin_num;
        break;
      /*case 4:
        if(low_hi)
          PORTE |= 1<<pin_num; 
        else 
          PORTE &= 0<<pin_num;
        break;*/
      /*case 5:
        if(low_hi)
          PORTF |= 1<<pin_num; 
        else
          PORTF &= 0<<pin_num;
        break;*/
    }
}

// refreshDisplay
/******************************************************************************/
// Turns on the segments specified in 'digitCodes[]'
// There are 4 versions of this function, with the choice depending on the
// location of the current-limiting resistors, and whether or not you wish to
// use 'update delays' (the standard method until 2017).
// For resistors on *digits* we will cycle through all 8 segments (7 + period),
//    turning on the *digits* as appropriate for a given segment, before moving on
//    to the next segment.
// For resistors on *segments* we will cycle through all __ # of digits,
//    turning on the *segments* as appropriate for a given digit, before moving on
//    to the next digit.
// If using update delays, refreshDisplay has a delay between each digit/segment
//    as it cycles through. It exits with all LEDs off.
// If not using updateDelays, refreshDisplay exits with a single digit/segment
//    on. It will move to the next digit/segment after being called again (if
//    enough time has passed).

void SevSeg::refreshDisplay() {

  if (!updateWithDelays) {
    uint32_t us = micros();

    // Exit if it's not time for the next display change
    if (waitOffActive) {
      if ((us - prevUpdateTime) < waitOffTime) return;
    }
    else {
      if ((us - prevUpdateTime) < ledOnTime) return;
    }
    prevUpdateTime = us;

    if (!resOnSegments) {
      /**********************************************/
      // RESISTORS ON DIGITS, UPDATE WITHOUT DELAYS

      if (waitOffActive) {
        waitOffActive = false;
      }
      else {
        // Turn all lights off for the previous segment
        segmentOff(prevUpdateIdx);

        if (waitOffTime) {
          // Wait a delay with all lights off
          waitOffActive = true;
          return;
        }
      }

      prevUpdateIdx++;
      if (prevUpdateIdx >= numSegments) prevUpdateIdx = 0;

      // Illuminate the required digits for the new segment
      segmentOn(prevUpdateIdx);
    }
    else {
      /**********************************************/
      // RESISTORS ON SEGMENTS, UPDATE WITHOUT DELAYS

      if (waitOffActive) {
        waitOffActive = false;
      }
      else {
        // Turn all lights off for the previous digit
        digitOff(prevUpdateIdx);

        if (waitOffTime) {
          // Wait a delay with all lights off
          waitOffActive = true;
          return;
        }
      }

      prevUpdateIdx++;
      if (prevUpdateIdx >= numDigits) prevUpdateIdx = 0;

      // Illuminate the required segments for the new digit
      digitOn(prevUpdateIdx);
    }
  }

  else {
    if (!resOnSegments) {
      /**********************************************/
      // RESISTORS ON DIGITS, UPDATE WITH DELAYS
      for (uint8_t segmentNum = 0 ; segmentNum < numSegments ; segmentNum++) {

        // Illuminate the required digits for this segment
        segmentOn(segmentNum);

        // Wait with lights on (to increase brightness)
        delayMicroseconds(ledOnTime);

        // Turn all lights off
        segmentOff(segmentNum);

        // Wait with all lights off if required
        if (waitOffTime) delayMicroseconds(waitOffTime);
      }
    }
    else {
      /**********************************************/
      // RESISTORS ON SEGMENTS, UPDATE WITH DELAYS
      for (uint8_t digitNum = 0 ; digitNum < numDigits ; digitNum++) {

        // Illuminate the required segments for this digit
        digitOn(digitNum);

        // Wait with lights on (to increase brightness)
        delayMicroseconds(ledOnTime);

        // Turn all lights off
        digitOff(digitNum);

        // Wait with all lights off if required
        if (waitOffTime) delayMicroseconds(waitOffTime);
      }
    }
  }
}

// segmentOn
/******************************************************************************/
// Turns a segment on, as well as all corresponding digit pins
// (according to digitCodes[])
void SevSeg::segmentOn(uint8_t segmentNum) {
  digitalWrite_new(segmentPins[segmentNum], segment_port[segmentNum], segmentOnVal);
  //digitalWrite(segmentPins[segmentNum], segmentOnVal);
  
  for (uint8_t digitNum = 0 ; digitNum < numDigits ; digitNum++) {
    if (digitCodes[digitNum] & (1 << segmentNum)) { // Check a single bit
      digitalWrite_new(digitPins[digitNum], digitPins[digitNum], digitOnVal);
      //digitalWrite(digitPins[digitNum], digitOnVal);
    }
  }
}

// segmentOff
/******************************************************************************/
// Turns a segment off, as well as all digit pins
void SevSeg::segmentOff(uint8_t segmentNum) {
  for (uint8_t digitNum = 0 ; digitNum < numDigits ; digitNum++) {
    digitalWrite_new(digitPins[digitNum], digitPins[digitNum], digitOnVal);
    //digitalWrite(digitPins[digitNum], digitOffVal);
  }
  digitalWrite_new(segmentPins[segmentNum], segment_port[segmentNum], segmentOnVal);
  //digitalWrite(segmentPins[segmentNum], segmentOffVal);
}

// digitOn
/******************************************************************************/
// Turns a digit on, as well as all corresponding segment pins
// (according to digitCodes[])
void SevSeg::digitOn(uint8_t digitNum) {
  digitalWrite_new(digitPins[digitNum], digitPins[digitNum], digitOnVal);
  //digitalWrite(digitPins[digitNum], digitOnVal);
  for (uint8_t segmentNum = 0 ; segmentNum < numSegments ; segmentNum++) {
    if (digitCodes[digitNum] & (1 << segmentNum)) { // Check a single bit
      digitalWrite_new(segmentPins[segmentNum], segment_port[segmentNum], segmentOnVal);
      //digitalWrite(segmentPins[segmentNum], segmentOnVal);
    }
  }
}

// digitOff
/******************************************************************************/
// Turns a digit off, as well as all segment pins
void SevSeg::digitOff(uint8_t digitNum) {
  for (uint8_t segmentNum = 0 ; segmentNum < numSegments ; segmentNum++) {
    digitalWrite_new(segmentPins[segmentNum], segment_port[segmentNum], segmentOnVal);
    //digitalWrite(segmentPins[segmentNum], segmentOffVal);
  }
  digitalWrite_new(digitPins[digitNum], digitPins[digitNum], digitOnVal);
  //digitalWrite(digitPins[digitNum], digitOffVal);
}

// setBrightness
/******************************************************************************/
// Sets ledOnTime according to the brightness given. Standard brightness range
// is 0 to 100. Flickering is more likely at brightness > 100, and < -100.
// A positive brightness introduces a delay while the LEDs are on, and a
// negative brightness introduces a delay while the LEDs are off.
void SevSeg::setBrightness(int16_t brightness) {
  brightness = constrain(brightness, -200, 200);
  if (brightness > 0) {
    ledOnTime = map(brightness, 0, 100, 1, 2000);
    waitOffTime = 0;
    waitOffActive = false;
  }
  else {
    ledOnTime = 0;
    waitOffTime = map(brightness, 0, -100, 1, 2000);
  }
}


// setNumber
/******************************************************************************/
// Receives an integer and passes it to 'setNewNum'.
void SevSeg::setNumber(int32_t numToShow, int8_t decPlaces, bool hex) { //int32_t
  setNewNum(numToShow, decPlaces, hex);
}

// setNumberF
/******************************************************************************/
// Receives a float, prepares it, and passes it to 'setNewNum'.
void SevSeg::setNumberF(float numToShow, int8_t decPlaces, bool hex) { //float
  int8_t decPlacesPos = constrain(decPlaces, 0, MAXNUMDIGITS);
  if (hex) {
    numToShow = numToShow * powersOf16[decPlacesPos];
  }
  else {
    numToShow = numToShow * powersOf10[decPlacesPos];
  }
  // Modify the number so that it is rounded to an integer correctly
  numToShow += (numToShow >= 0.f) ? 0.5f : -0.5f;
  setNewNum((int32_t)numToShow, (int8_t)decPlaces, hex);
}

// setNewNum
/******************************************************************************/
// Changes the number that will be displayed.
void SevSeg::setNewNum(int32_t numToShow, int8_t decPlaces, bool hex) {
  uint8_t digits[MAXNUMDIGITS];
  findDigits(numToShow, decPlaces, hex, digits);
  setDigitCodes(digits, decPlaces);
}


// setSegments
/******************************************************************************/
// Sets the 'digitCodes' that are required to display the desired segments.
// Using this function, one can display any arbitrary set of segments (like
// letters, symbols or animated cursors). See setDigitCodes() for common
// numeric examples.
//
// Bit-segment mapping:  0bHGFEDCBA
//      Visual mapping:
//                        AAAA          0000
//                       F    B        5    1
//                       F    B        5    1
//                        GGGG          6666
//                       E    C        4    2
//                       E    C        4    2        (Segment H is often called
//                        DDDD  H       3333  7      DP, for Decimal Point)
void SevSeg::setSegments(const uint8_t segs[]) {
  for (uint8_t digit = 0; digit < numDigits; digit++) {
    digitCodes[digit] = segs[digit];
  }
}

// setSegmentsDigit
/******************************************************************************/
// Like setSegments above, but only manipulates the segments for one digit
// digitNum is 0-indexed.
void SevSeg::setSegmentsDigit(const uint8_t digitNum, const uint8_t segs) {
  if (digitNum < numDigits) {
    digitCodes[digitNum] = segs;
  }
}

// getSegments
/******************************************************************************/
// Gets the 'digitCodes' of currently displayed segments.
// Using this function, one can get the current set of segments (placed
// elsewhere) and manipulate them to obtain effects, for example blink of
// only some digits.
// See setSegments() for bit-segment mapping
//
void SevSeg::getSegments(uint8_t segs[]) {
  for (uint8_t digit = 0; digit < numDigits; digit++) {
    segs[digit] = digitCodes[digit];
  }
}

// setChars
/******************************************************************************/
// Displays the string on the display, as best as possible.
// Only alphanumeric characters plus '-' and ' ' are supported
void SevSeg::setChars(const char str[]) {
  for (uint8_t digit = 0; digit < numDigits; digit++) {
    digitCodes[digit] = 0;
  }

  uint8_t strIdx = 0; // Current position within str[]
  for (uint8_t digitNum = 0; digitNum < numDigits; digitNum++) {
    char ch = str[strIdx];
    if (ch == '\0') break; // NULL string terminator
    if (ch >= '0' && ch <= '9') { // Numerical
      digitCodes[digitNum] = numeralCodes[ch - '0'];
    }
    else if (ch >= 'A' && ch <= 'Z') {
      digitCodes[digitNum] = alphaCodes[ch - 'A'];
    }
    else if (ch >= 'a' && ch <= 'z') {
      digitCodes[digitNum] = alphaCodes[ch - 'a'];
    }
    else if (ch == ' ') {
      digitCodes[digitNum] = digitCodeMap[BLANK_IDX];
    }
    else if (ch == '.') {
      digitCodes[digitNum] = digitCodeMap[PERIOD_IDX];
    }
    else if (ch == '*') {
      digitCodes[digitNum] = digitCodeMap[ASTERISK_IDX];
    }
    else if (ch == '_') {
      digitCodes[digitNum] = digitCodeMap[UNDERSCORE_IDX];
    }
    else {
      // Every unknown character is shown as a dash
      digitCodes[digitNum] = digitCodeMap[DASH_IDX];
    }

    strIdx++;
    // Peek at next character. If it's a period, add it to this digit
    if (str[strIdx] == '.') {
      digitCodes[digitNum] |= digitCodeMap[PERIOD_IDX];
      strIdx++;
    }
  }
}

// blank
/******************************************************************************/
void SevSeg::blank(void) {
  for (uint8_t digitNum = 0 ; digitNum < numDigits ; digitNum++) {
    digitCodes[digitNum] = digitCodeMap[BLANK_IDX];
  }
  segmentOff(0);
  digitOff(0);
}

// findDigits
/******************************************************************************/
// Decides what each digit will display.
// Enforces the upper and lower limits on the number to be displayed.
// digits[] is an output
void SevSeg::findDigits(int32_t numToShow, int8_t decPlaces, bool hex, uint8_t digits[]) {
  const int32_t * powersOfBase = hex ? powersOf16 : powersOf10;
  const int32_t maxNum = powersOfBase[numDigits] - 1;
  const int32_t minNum = -(powersOfBase[numDigits - 1] - 1);

  // If the number is out of range, just display dashes
  if (numToShow > maxNum || numToShow < minNum) {
    for (uint8_t digitNum = 0 ; digitNum < numDigits ; digitNum++) {
      digits[digitNum] = DASH_IDX;
    }
  }
  else {
    uint8_t digitNum = 0;

    // Convert all number to positive values
    if (numToShow < 0) {
      digits[0] = DASH_IDX;
      digitNum = 1; // Skip the first iteration
      numToShow = -numToShow;
    }

    // Find all digits for base's representation, starting with the most
    // significant digit
    for ( ; digitNum < numDigits ; digitNum++) {
      int32_t factor = powersOfBase[numDigits - 1 - digitNum];
      digits[digitNum] = numToShow / factor;
      numToShow -= digits[digitNum] * factor;
    }

    // Find unnnecessary leading zeros and set them to BLANK
    if (decPlaces < 0) decPlaces = 0;
    if (!leadingZeros) {
      for (digitNum = 0 ; digitNum < (numDigits - 1 - decPlaces) ; digitNum++) {
        if (digits[digitNum] == 0) {
          digits[digitNum] = BLANK_IDX;
        }
        // Exit once the first non-zero number is encountered
        else if (digits[digitNum] <= 9) {
          break;
        }
      }
    }

  }
}


// setDigitCodes
/******************************************************************************/
// Sets the 'digitCodes' that are required to display the input numbers
void SevSeg::setDigitCodes(const uint8_t digits[], int8_t decPlaces) {

  // Set the digitCode for each digit in the display
  for (uint8_t digitNum = 0 ; digitNum < numDigits ; digitNum++) {
    digitCodes[digitNum] = digitCodeMap[digits[digitNum]];
    // Set the decimal point segment
    if (decPlaces >= 0) {
      if (digitNum == numDigits - 1 - decPlaces) {
        digitCodes[digitNum] |= digitCodeMap[PERIOD_IDX];
      }
    }
  }
}

/// END ///
```

Revised SevSeg.h file (for Atmega328P, not for 32U4)
```
/* SevSeg Library
 *
 * Copyright 2020 Dean Reading
 *
 * This library allows an Arduino to easily display numbers and letters on a
 * 7-segment display without a separate 7-segment display controller.
 *
 * See the included readme for instructions.
 * https://github.com/DeanIsMe/SevSeg
 */

#ifndef MAXNUMDIGITS
#define MAXNUMDIGITS 8 // Can be increased, but the max number is 2^31
#endif

#ifndef SevSeg_h
#define SevSeg_h

#if defined(ARDUINO) && ARDUINO >= 100
#include "Arduino.h"
#else
#include "WProgram.h"
#endif

// Use defines to link the hardware configurations to the correct numbers
#define COMMON_CATHODE 0
#define COMMON_ANODE 1
#define N_TRANSISTORS 2
#define P_TRANSISTORS 3
#define NP_COMMON_CATHODE 1
#define NP_COMMON_ANODE 0


class SevSeg
{
public:
  SevSeg();

  void refreshDisplay();
  void begin(uint8_t hardwareConfig, uint8_t numDigitsIn, const uint8_t digitPinsIn[],
          const uint8_t segmentPinsIn[], const int8_t digit_port_In[], const int8_t segment_port_In[],
          bool resOnSegmentsIn=0, bool updateWithDelaysIn=0, bool leadingZerosIn=0,
      bool disableDecPoint=0);
  void pinMode_new(uint8_t pin_num, uint8_t port_num, uint8_t in_out);
  void digitalWrite_new(uint8_t pin_num, uint8_t port_num, uint8_t low_hi);
  void setBrightness(int16_t brightnessIn); // A number from 0..100

  void setNumber(int32_t numToShow, int8_t decPlaces=-1, bool hex=0);
  void setNumberF(float numToShow, int8_t decPlaces=-1, bool hex=0);

  void setSegments(const uint8_t segs[]);
  void getSegments(uint8_t segs[]);
  void setSegmentsDigit(const uint8_t digitNum, const uint8_t segs);
  void setChars(const char str[]);
  void blank(void);

private:
  void setNewNum(int32_t numToShow, int8_t decPlaces, bool hex=0);
  void findDigits(int32_t numToShow, int8_t decPlaces, bool hex, uint8_t digits[]);
  void setDigitCodes(const uint8_t nums[], int8_t decPlaces);
  void segmentOn(uint8_t segmentNum);
  void segmentOff(uint8_t segmentNum);
  void digitOn(uint8_t digitNum);
  void digitOff(uint8_t digitNum);

  uint8_t digitOnVal,digitOffVal,segmentOnVal,segmentOffVal;
  bool resOnSegments, updateWithDelays, leadingZeros;
  uint8_t digitPins[MAXNUMDIGITS];
  uint8_t segment_port[MAXNUMDIGITS]; //new var
  uint8_t segmentPins[8];
  uint8_t digit_port[8]; //new var
  uint8_t numDigits;
  uint8_t numSegments;
  uint8_t prevUpdateIdx; // The previously updated segment or digit
  uint8_t digitCodes[MAXNUMDIGITS]; // The active setting of each segment of each digit
  uint32_t prevUpdateTime; // The time (millis()) when the display was last updated
  uint16_t ledOnTime; // The time (us) to wait with LEDs on
  uint16_t waitOffTime; // The time (us) to wait with LEDs off
  bool waitOffActive; // Whether  the program is waiting with LEDs off
};

#endif //SevSeg_h
/// END ///
```

* the new library still has not been tested yet
  * note to self: in this file path C:\Users\tiffa\Documents\Arduino\sketchbook\libraries
* the combined code has been revised to account for the new library parameters (ONLY testing library-- haven't changed other parameters)
* for atmega328p .h and .cpp files, I needed to get rid of all other ports except B, C, and D-- b/c atmega does not have those other ports that the atmega32U4 has
    * I commented out those cases
* the code compiles... so now let's find out if it actually works tm :') this will be terrible to debug

<a name="2021/11/03"></a>
# 2021/11/03
note about pins
* "PIN_A4" for analogs-- does have correspondence
  * https://forum.arduino.cc/t/leonardo-analog-pins-as-digital/676695/3
* "PE2" for digitals-- 
  * https://www.arduino.cc/en/Hacking/PinMapping32u4 
* ditch the library-revision idea, since elisa found out how we can reference the digital pins

<a name="2021/11/04"></a>
# 2021/11/04
* cannot reference the pins (tried it, pins_arduino file, etc)
* revise library 

https://create.arduino.cc/projecthub/aboda243/how-to-use-4-digit-7-segment-without-library-b8e014

<a name="2021/11/07"></a>
# 2021/11/07
## Objective:
* set up code to test if sensing portion of code still works (analogRead)
* write own 4-digit 7-segment code 

* in-person w/ PCB:
  * test simple_turn_on_LEDs code (find out if LEDs even light up)
  * test Arduino_sense_code_4_pins_test file (find out if sensing code still works with coding)-- C:\Users\tiffa\Desktop\UIUC_semester_5\ECE 445\Arduino_sense_code_4_pins_test
    * sweep 0V to 15V input into grid input (check LED output)

Arduino_sense_code_4_pins_test:
```
// Team 3 (TREEE)
// Voltage and Current Sensing


// defining ADC pins being used
#define Vout_sens A5
#define Vdc_sens A4
#define Vgrid_sens A3
#define Iout_sens A2

// store raw ADC in these variables
float Vout_adc;
float Vdc_adc;
float Vgrid_adc;
float Iout_adc;

// non-ideal mapping variables for AC, DC sensing circuit
float inv_0; //output of inverter sensing circuit when -25V potential is applied
float inv_25; //output of inverter sensing circuit when +25V potential is applied
float dc_0; //output of DC sensing circuit when 0V is applied
float dc_25; //output of DC sensing circuit when 25V is applied
float grid_0;
float grid_25;
float i_0; //output of current sensor when 0A is applied to input
float i_30; //output of current sensor when 30mA is applied to input

// non-ideal mapping variables for Arduino
//float ard_0; //ADC value of microcontroller when 0V is applied to microcon
//float ard_5; //ADC value of microcontroller when 5V is applied to microcon

//mapping ADC value to microcontroller input value
float Vard_map_inv;
float Vard_map_dc;
float Vard_map_grid; 
float Vard_map_i;

//slopes based on calibration values
float Vard_slope;
float inv_slope;
float dc_slope;
float grid_slope;
float i_slope;

// original values
float Vout_map;   //inverter output
float Vdc_map;    //DC-DC output
float Vgrid_map;  //grid output
float Iout_map;   //current output

void setup() {
  
  // comment this out later: for display
  //Serial.begin(2000000);

  // comment this out later: testing analogRead with Atmega32U4
  DDRB |= 00110000; //set PB4 and PB5 as outputs

  // configure inputs/outputs
  pinMode(Vout_sens, INPUT);
  pinMode(Vdc_sens, INPUT);
  pinMode(Vgrid_sens, INPUT);
  pinMode(Iout_sens, INPUT);

  //assign non-ideal mapping vars for Arduino
    //ideal Arduino mapping (ADC,V): (1023,5),(0,0)
    // x is ard_5, y is ard_0
    //non-ideal Arduino mapping: (x,5),(y,0)
    //adjusted slope: instead of (5/1023)(ADC), use ((5/(x-y))(ADC)+y)
      //((5/(x-y))*(ADC)+y)

  /* CALIBRATION VALUES */
  //ard_5 = 1023;
  //ard_0 = 0;
  inv_0 = 1.65327; //placeholder
  inv_25 = 1.88415; //placeholder
  dc_0 = 0.000085;
  dc_25 = 0.693770;
  grid_0 = 1.65327;
  grid_25 = 1.88415; //NOTE: these current calibration points have 170V correspond with 3.3V (not a perfect 4 or 5V due to nonidealities)
  i_0 = 2.511;
  i_30 = 2.517;

  /* calibrated slopes */
  Vard_slope = 5.0/1023;
  inv_slope = 25.0/(inv_25-inv_0);
  dc_slope = 25.0/(dc_25-dc_0);
  grid_slope = 25.0/(grid_25-grid_0);
  i_slope = 0.03/(i_30-i_0); //30mA
}

void loop() {
  // read raw ADC values (0 to 1023)
  Vout_adc = analogRead(Vout_sens);
  Vdc_adc = analogRead(Vdc_sens);
  Vgrid_adc = analogRead(Vgrid_sens);
  Iout_adc = analogRead(Iout_sens);

  //ADC -> voltage input into microcontroller
  Vard_map_inv = Vard_slope*Vout_adc;
  Vard_map_dc = Vard_slope*Vdc_adc;
  Vard_map_grid = Vard_slope*Vgrid_adc;
  Vard_map_i = Vard_slope*Iout_adc;

  Vout_map = (Vard_map_inv-inv_0)*inv_slope;  //sense_out = (inv_25-inv_0)/25 x input + inv_0
  Vdc_map = Vard_map_dc*dc_slope;             //sense_out = (dc_25 - dc_0)/25 x input
  Vgrid_map = (Vard_map_grid-grid_0)*grid_slope;
  Iout_map = (Vard_map_i-i_0)*i_slope;        //sense_out = (i_30-i_0)/.03 x input + i_0

  if(Vgrid_map > 15){ //test if analogRead works. turn on PB4 and PB5 (LEDs) if voltage applied is greater than 15 
    PORTB |= 00110000;
  }
  else{ //turn off LEDs if analogRead ain't working
    PORTB &= 11001111;
  }

  //Serial Monitor / Plotter
  //Serial.println(Vdc_map);
}
```

* in-person w/ Arduino Uno:
  * as I was going through the library revision I had done for SevSeg, I found that I had incorrectly called the digitalWrite_new functions multiple times, so I will try with this library revision one more time
  * upload and test with file-- C:\Users\tiffa\Desktop\UIUC_semester_5\ECE 445\Display_Code_Combined_3_newlib
  * note again to self-- path location-- C:\Users\tiffa\Documents\Arduino\sketchbook\libraries

Revised revision of Sevseg library
```
/* SevSeg Library
 *
 * Copyright 2020 Dean Reading
 *
 * This library allows an Arduino to easily display numbers and letters on a
 * 7-segment display without a separate 7-segment display controller.
 *
 * See the included readme for instructions.
 * https://github.com/DeanIsMe/SevSeg
 */

#include "SevSeg.h"

#define BLANK_IDX 36 // Must match with 'digitCodeMap'
#define DASH_IDX 37
#define PERIOD_IDX 38
#define ASTERISK_IDX 39
#define UNDERSCORE_IDX 40

static const int32_t powersOf10[] = {
  1, // 10^0
  10,
  100,
  1000,
  10000,
  100000,
  1000000,
  10000000,
  100000000,
  1000000000
}; // 10^9

static const int32_t powersOf16[] = {
  0x1, // 16^0
  0x10,
  0x100,
  0x1000,
  0x10000,
  0x100000,
  0x1000000,
  0x10000000
}; // 16^7

// digitCodeMap indicate which segments must be illuminated to display
// each number.
static const uint8_t digitCodeMap[] = {
  // GFEDCBA  Segments      7-segment map:
  0b00111111, // 0   "0"          AAA
  0b00000110, // 1   "1"         F   B
  0b01011011, // 2   "2"         F   B
  0b01001111, // 3   "3"          GGG
  0b01100110, // 4   "4"         E   C
  0b01101101, // 5   "5"         E   C
  0b01111101, // 6   "6"          DDD
  0b00000111, // 7   "7"
  0b01111111, // 8   "8"
  0b01101111, // 9   "9"
  0b01110111, // 65  'A'
  0b01111100, // 66  'b'
  0b00111001, // 67  'C'
  0b01011110, // 68  'd'
  0b01111001, // 69  'E'
  0b01110001, // 70  'F'
  0b00111101, // 71  'G'
  0b01110110, // 72  'H'
  0b00110000, // 73  'I'
  0b00001110, // 74  'J'
  0b01110110, // 75  'K'  Same as 'H'
  0b00111000, // 76  'L'
  0b00000000, // 77  'M'  NO DISPLAY
  0b01010100, // 78  'n'
  0b00111111, // 79  'O'
  0b01110011, // 80  'P'
  0b01100111, // 81  'q'
  0b01010000, // 82  'r'
  0b01101101, // 83  'S'
  0b01111000, // 84  't'
  0b00111110, // 85  'U'
  0b00111110, // 86  'V'  Same as 'U'
  0b00000000, // 87  'W'  NO DISPLAY
  0b01110110, // 88  'X'  Same as 'H'
  0b01101110, // 89  'y'
  0b01011011, // 90  'Z'  Same as '2'
  0b00000000, // 32  ' '  BLANK
  0b01000000, // 45  '-'  DASH
  0b10000000, // 46  '.'  PERIOD
  0b01100011, // 42 '*'  DEGREE ..
  0b00001000, // 95 '_'  UNDERSCORE
};

// Constant pointers to constant data
const uint8_t * const numeralCodes = digitCodeMap;
const uint8_t * const alphaCodes = digitCodeMap + 10;

// SevSeg Constructor
/******************************************************************************/
SevSeg::SevSeg() {
  // Initial value
  ledOnTime = 2000; // Corresponds to a brightness of 100
  waitOffTime = 0;
  waitOffActive = false;
  numDigits = 0;
  prevUpdateIdx = 0;
  prevUpdateTime = 0;
  resOnSegments = 0;
  updateWithDelays = 0;
}


// begin
/******************************************************************************/
// Saves the input pin numbers to the class and sets up the pins to be used.
// If you use current-limiting resistors on your segment pins instead of the
// digit pins, then set resOnSegments as true.
// Set updateWithDelays to true if you want to use the 'pre-2017' update method
// In that case, the processor is occupied with delay functions while refreshing
// leadingZerosIn indicates whether leading zeros should be displayed
// disableDecPoint is true when the decimal point segment is not connected, in
// which case there are only 7 segments.

/************
  digit_port & segment_port added:
  1: Port B
  2: Port C
  3: Port D
  4: Port E
  5: Port F

  values in digitPinsIn[] and segmentPinsIn[] will now correspond to # of pin on port
************/

void SevSeg::begin(uint8_t hardwareConfig, uint8_t numDigitsIn, const uint8_t digitPinsIn[],
                   const uint8_t segmentPinsIn[], const uint8_t digit_port_In[], const uint8_t segment_port_In[],
                   bool resOnSegmentsIn,bool updateWithDelaysIn, bool leadingZerosIn, bool disableDecPoint) {

  resOnSegments = resOnSegmentsIn;
  updateWithDelays = updateWithDelaysIn;
  leadingZeros = leadingZerosIn;

  numDigits = numDigitsIn;
  numSegments = disableDecPoint ? 7 : 8; // Ternary 'if' statement
  //Limit the max number of digits to prevent overflowing
  if (numDigits > MAXNUMDIGITS) numDigits = MAXNUMDIGITS;

  switch (hardwareConfig) {

    case 0: // Common cathode
      digitOnVal = LOW;
      segmentOnVal = HIGH;
      break;

    case 1: // Common anode
      digitOnVal = HIGH;
      segmentOnVal = LOW;
      break;

    case 2: // With active-high, low-side switches (most commonly N-type FETs)
      digitOnVal = HIGH;
      segmentOnVal = HIGH;
      break;

    case 3: // With active low, high side switches (most commonly P-type FETs)
      digitOnVal = LOW;
      segmentOnVal = LOW;
      break;
  }

  // define the Off-Values depending on the On-Values
  if (digitOnVal == HIGH){
    digitOffVal = LOW;
  } else {
    digitOffVal = HIGH;
  }
  // define the Off-Values depending on the On-Values
  if (segmentOnVal == HIGH){
    segmentOffVal = LOW;
  } else {
    segmentOffVal = HIGH;
  }

  // Save the input pin numbers to library variables
  for (uint8_t segmentNum = 0 ; segmentNum < numSegments ; segmentNum++) {
    segmentPins[segmentNum] = segmentPinsIn[segmentNum];
    segment_port[segmentNum] = segment_port_In[segmentNum];
  }

  for (uint8_t digitNum = 0 ; digitNum < numDigits ; digitNum++) {
    digitPins[digitNum] = digitPinsIn[digitNum];
    digit_port[digitNum] = digit_port_In[digitNum];
  }
  

/************
  case statements for assigning ports
  1: Port B
  2: Port C
  3: Port D
  4: Port E
  5: Port F

  new functions written below (pinMode_new, digitalWrite_new-- include another parameter, port value)
************/

  // Set the pins as outputs, and turn them off
  for (uint8_t digit = 0 ; digit < numDigits ; digit++) {
    pinMode_new(digitPins[digit], digit_port[digit], HIGH); //set as output
    digitalWrite_new(digitPins[digit], digit_port[digit], digitOffVal);
    //pinMode(digitPins[digit], OUTPUT);
    //digitalWrite(digitPins[digit], digitOffVal);
  }

  for (uint8_t segmentNum = 0 ; segmentNum < numSegments ; segmentNum++) {
    pinMode_new(segmentPins[segmentNum], segment_port[segmentNum], HIGH); //set as output
    digitalWrite_new(segmentPins[segmentNum], segment_port[segmentNum], segmentOffVal);
    //pinMode(segmentPins[segmentNum], OUTPUT);
    //digitalWrite(segmentPins[segmentNum], segmentOffVal);
  }

  blank(); // Initialise the display
}

/************
 create functions to replace pinMode and digitalWrite

 pin_num: pin on the port
 port_num: values corresponding with specific ports
 in_out: input = 0, output = 1 set to bit
 lo_hi: output 1 or output 0
************/
void SevSeg::pinMode_new(uint8_t pin_num, uint8_t port_num, uint8_t in_out){
  switch(port_num){
      case 1:
        if(in_out) //if output
          DDRB |= 1<<pin_num; //bit-shifted to appropriate bit
        else //if input
          DDRB &= 0<<pin_num;
        break;
      case 2:
        if(in_out)
          DDRC |= 1<<pin_num;
        else
          DDRC &= 0<<pin_num;
        break;
      case 3:
        if(in_out)
          DDRD |= 1<<pin_num;
        else 
          DDRD &= 0<<pin_num;
        break;
      /*case 4:
        if(in_out)
          DDRE |= 1<<pin_num; 
        else 
          DDRE &= 0<<pin_num;
        break;*/
      /*case 5:
        if(in_out)
          DDRF |= 1<<pin_num; 
        else
          DDRF &= 0<<pin_num;
        break;*/
    }
}
void SevSeg::digitalWrite_new(uint8_t pin_num, uint8_t port_num, uint8_t low_hi){
  switch(port_num){
      case 1:
        if(low_hi) //if output
          PORTB |= 1<<pin_num; //bit-shifted to appropriate bit
        else //if input
          PORTB &= 0<<pin_num;
        break;
      case 2:
        if(low_hi)
          PORTC |= 1<<pin_num;
        else
          PORTC &= 0<<pin_num;
        break;
      case 3:
        if(low_hi)
          PORTD |= 1<<pin_num;
        else 
          PORTD &= 0<<pin_num;
        break;
      /*case 4:
        if(low_hi)
          PORTE |= 1<<pin_num; 
        else 
          PORTE &= 0<<pin_num;
        break;*/
      /*case 5:
        if(low_hi)
          PORTF |= 1<<pin_num; 
        else
          PORTF &= 0<<pin_num;
        break;*/
    }
}

// refreshDisplay
/******************************************************************************/
// Turns on the segments specified in 'digitCodes[]'
// There are 4 versions of this function, with the choice depending on the
// location of the current-limiting resistors, and whether or not you wish to
// use 'update delays' (the standard method until 2017).
// For resistors on *digits* we will cycle through all 8 segments (7 + period),
//    turning on the *digits* as appropriate for a given segment, before moving on
//    to the next segment.
// For resistors on *segments* we will cycle through all __ # of digits,
//    turning on the *segments* as appropriate for a given digit, before moving on
//    to the next digit.
// If using update delays, refreshDisplay has a delay between each digit/segment
//    as it cycles through. It exits with all LEDs off.
// If not using updateDelays, refreshDisplay exits with a single digit/segment
//    on. It will move to the next digit/segment after being called again (if
//    enough time has passed).

void SevSeg::refreshDisplay() {

  if (!updateWithDelays) {
    uint32_t us = micros();

    // Exit if it's not time for the next display change
    if (waitOffActive) {
      if ((us - prevUpdateTime) < waitOffTime) return;
    }
    else {
      if ((us - prevUpdateTime) < ledOnTime) return;
    }
    prevUpdateTime = us;

    if (!resOnSegments) {
      /**********************************************/
      // RESISTORS ON DIGITS, UPDATE WITHOUT DELAYS

      if (waitOffActive) {
        waitOffActive = false;
      }
      else {
        // Turn all lights off for the previous segment
        segmentOff(prevUpdateIdx);

        if (waitOffTime) {
          // Wait a delay with all lights off
          waitOffActive = true;
          return;
        }
      }

      prevUpdateIdx++;
      if (prevUpdateIdx >= numSegments) prevUpdateIdx = 0;

      // Illuminate the required digits for the new segment
      segmentOn(prevUpdateIdx);
    }
    else {
      /**********************************************/
      // RESISTORS ON SEGMENTS, UPDATE WITHOUT DELAYS

      if (waitOffActive) {
        waitOffActive = false;
      }
      else {
        // Turn all lights off for the previous digit
        digitOff(prevUpdateIdx);

        if (waitOffTime) {
          // Wait a delay with all lights off
          waitOffActive = true;
          return;
        }
      }

      prevUpdateIdx++;
      if (prevUpdateIdx >= numDigits) prevUpdateIdx = 0;

      // Illuminate the required segments for the new digit
      digitOn(prevUpdateIdx);
    }
  }

  else {
    if (!resOnSegments) {
      /**********************************************/
      // RESISTORS ON DIGITS, UPDATE WITH DELAYS
      for (uint8_t segmentNum = 0 ; segmentNum < numSegments ; segmentNum++) {

        // Illuminate the required digits for this segment
        segmentOn(segmentNum);

        // Wait with lights on (to increase brightness)
        delayMicroseconds(ledOnTime);

        // Turn all lights off
        segmentOff(segmentNum);

        // Wait with all lights off if required
        if (waitOffTime) delayMicroseconds(waitOffTime);
      }
    }
    else {
      /**********************************************/
      // RESISTORS ON SEGMENTS, UPDATE WITH DELAYS
      for (uint8_t digitNum = 0 ; digitNum < numDigits ; digitNum++) {

        // Illuminate the required segments for this digit
        digitOn(digitNum);

        // Wait with lights on (to increase brightness)
        delayMicroseconds(ledOnTime);

        // Turn all lights off
        digitOff(digitNum);

        // Wait with all lights off if required
        if (waitOffTime) delayMicroseconds(waitOffTime);
      }
    }
  }
}

// segmentOn
/******************************************************************************/
// Turns a segment on, as well as all corresponding digit pins
// (according to digitCodes[])
void SevSeg::segmentOn(uint8_t segmentNum) {
  digitalWrite_new(segmentPins[segmentNum], segment_port[segmentNum], segmentOnVal);
  //digitalWrite(segmentPins[segmentNum], segmentOnVal);
  
  for (uint8_t digitNum = 0 ; digitNum < numDigits ; digitNum++) {
    if (digitCodes[digitNum] & (1 << segmentNum)) { // Check a single bit
      digitalWrite_new(digitPins[digitNum], digit_port[digitNum], digitOnVal);
      //digitalWrite(digitPins[digitNum], digitOnVal);
    }
  }
}

// segmentOff
/******************************************************************************/
// Turns a segment off, as well as all digit pins
void SevSeg::segmentOff(uint8_t segmentNum) {
  for (uint8_t digitNum = 0 ; digitNum < numDigits ; digitNum++) {
    digitalWrite_new(digitPins[digitNum], digit_port[digitNum], digitOffVal);
    //digitalWrite(digitPins[digitNum], digitOffVal);
  }
  digitalWrite_new(segmentPins[segmentNum], segment_port[segmentNum], segmentOffVal);
  //digitalWrite(segmentPins[segmentNum], segmentOffVal);
}

// digitOn
/******************************************************************************/
// Turns a digit on, as well as all corresponding segment pins
// (according to digitCodes[])
void SevSeg::digitOn(uint8_t digitNum) {
  digitalWrite_new(digitPins[digitNum], digit_port[digitNum], digitOnVal);
  //digitalWrite(digitPins[digitNum], digitOnVal);
  for (uint8_t segmentNum = 0 ; segmentNum < numSegments ; segmentNum++) {
    if (digitCodes[digitNum] & (1 << segmentNum)) { // Check a single bit
      digitalWrite_new(segmentPins[segmentNum], segment_port[segmentNum], segmentOnVal);
      //digitalWrite(segmentPins[segmentNum], segmentOnVal);
    }
  }
}

// digitOff
/******************************************************************************/
// Turns a digit off, as well as all segment pins
void SevSeg::digitOff(uint8_t digitNum) {
  for (uint8_t segmentNum = 0 ; segmentNum < numSegments ; segmentNum++) {
    digitalWrite_new(segmentPins[segmentNum], segment_port[segmentNum], segmentOffVal);
    //digitalWrite(segmentPins[segmentNum], segmentOffVal);
  }
  digitalWrite_new(digitPins[digitNum], digit_port[digitNum], digitOffVal);
  //digitalWrite(digitPins[digitNum], digitOffVal);
}

// setBrightness
/******************************************************************************/
// Sets ledOnTime according to the brightness given. Standard brightness range
// is 0 to 100. Flickering is more likely at brightness > 100, and < -100.
// A positive brightness introduces a delay while the LEDs are on, and a
// negative brightness introduces a delay while the LEDs are off.
void SevSeg::setBrightness(int16_t brightness) {
  brightness = constrain(brightness, -200, 200);
  if (brightness > 0) {
    ledOnTime = map(brightness, 0, 100, 1, 2000);
    waitOffTime = 0;
    waitOffActive = false;
  }
  else {
    ledOnTime = 0;
    waitOffTime = map(brightness, 0, -100, 1, 2000);
  }
}


// setNumber
/******************************************************************************/
// Receives an integer and passes it to 'setNewNum'.
void SevSeg::setNumber(int32_t numToShow, int8_t decPlaces, bool hex) { //int32_t
  setNewNum(numToShow, decPlaces, hex);
}

// setNumberF
/******************************************************************************/
// Receives a float, prepares it, and passes it to 'setNewNum'.
void SevSeg::setNumberF(float numToShow, int8_t decPlaces, bool hex) { //float
  int8_t decPlacesPos = constrain(decPlaces, 0, MAXNUMDIGITS);
  if (hex) {
    numToShow = numToShow * powersOf16[decPlacesPos];
  }
  else {
    numToShow = numToShow * powersOf10[decPlacesPos];
  }
  // Modify the number so that it is rounded to an integer correctly
  numToShow += (numToShow >= 0.f) ? 0.5f : -0.5f;
  setNewNum((int32_t)numToShow, (int8_t)decPlaces, hex);
}

// setNewNum
/******************************************************************************/
// Changes the number that will be displayed.
void SevSeg::setNewNum(int32_t numToShow, int8_t decPlaces, bool hex) {
  uint8_t digits[MAXNUMDIGITS];
  findDigits(numToShow, decPlaces, hex, digits);
  setDigitCodes(digits, decPlaces);
}


// setSegments
/******************************************************************************/
// Sets the 'digitCodes' that are required to display the desired segments.
// Using this function, one can display any arbitrary set of segments (like
// letters, symbols or animated cursors). See setDigitCodes() for common
// numeric examples.
//
// Bit-segment mapping:  0bHGFEDCBA
//      Visual mapping:
//                        AAAA          0000
//                       F    B        5    1
//                       F    B        5    1
//                        GGGG          6666
//                       E    C        4    2
//                       E    C        4    2        (Segment H is often called
//                        DDDD  H       3333  7      DP, for Decimal Point)
void SevSeg::setSegments(const uint8_t segs[]) {
  for (uint8_t digit = 0; digit < numDigits; digit++) {
    digitCodes[digit] = segs[digit];
  }
}

// setSegmentsDigit
/******************************************************************************/
// Like setSegments above, but only manipulates the segments for one digit
// digitNum is 0-indexed.
void SevSeg::setSegmentsDigit(const uint8_t digitNum, const uint8_t segs) {
  if (digitNum < numDigits) {
    digitCodes[digitNum] = segs;
  }
}

// getSegments
/******************************************************************************/
// Gets the 'digitCodes' of currently displayed segments.
// Using this function, one can get the current set of segments (placed
// elsewhere) and manipulate them to obtain effects, for example blink of
// only some digits.
// See setSegments() for bit-segment mapping
//
void SevSeg::getSegments(uint8_t segs[]) {
  for (uint8_t digit = 0; digit < numDigits; digit++) {
    segs[digit] = digitCodes[digit];
  }
}

// setChars
/******************************************************************************/
// Displays the string on the display, as best as possible.
// Only alphanumeric characters plus '-' and ' ' are supported
void SevSeg::setChars(const char str[]) {
  for (uint8_t digit = 0; digit < numDigits; digit++) {
    digitCodes[digit] = 0;
  }

  uint8_t strIdx = 0; // Current position within str[]
  for (uint8_t digitNum = 0; digitNum < numDigits; digitNum++) {
    char ch = str[strIdx];
    if (ch == '\0') break; // NULL string terminator
    if (ch >= '0' && ch <= '9') { // Numerical
      digitCodes[digitNum] = numeralCodes[ch - '0'];
    }
    else if (ch >= 'A' && ch <= 'Z') {
      digitCodes[digitNum] = alphaCodes[ch - 'A'];
    }
    else if (ch >= 'a' && ch <= 'z') {
      digitCodes[digitNum] = alphaCodes[ch - 'a'];
    }
    else if (ch == ' ') {
      digitCodes[digitNum] = digitCodeMap[BLANK_IDX];
    }
    else if (ch == '.') {
      digitCodes[digitNum] = digitCodeMap[PERIOD_IDX];
    }
    else if (ch == '*') {
      digitCodes[digitNum] = digitCodeMap[ASTERISK_IDX];
    }
    else if (ch == '_') {
      digitCodes[digitNum] = digitCodeMap[UNDERSCORE_IDX];
    }
    else {
      // Every unknown character is shown as a dash
      digitCodes[digitNum] = digitCodeMap[DASH_IDX];
    }

    strIdx++;
    // Peek at next character. If it's a period, add it to this digit
    if (str[strIdx] == '.') {
      digitCodes[digitNum] |= digitCodeMap[PERIOD_IDX];
      strIdx++;
    }
  }
}

// blank
/******************************************************************************/
void SevSeg::blank(void) {
  for (uint8_t digitNum = 0 ; digitNum < numDigits ; digitNum++) {
    digitCodes[digitNum] = digitCodeMap[BLANK_IDX];
  }
  segmentOff(0);
  digitOff(0);
}

// findDigits
/******************************************************************************/
// Decides what each digit will display.
// Enforces the upper and lower limits on the number to be displayed.
// digits[] is an output
void SevSeg::findDigits(int32_t numToShow, int8_t decPlaces, bool hex, uint8_t digits[]) {
  const int32_t * powersOfBase = hex ? powersOf16 : powersOf10;
  const int32_t maxNum = powersOfBase[numDigits] - 1;
  const int32_t minNum = -(powersOfBase[numDigits - 1] - 1);

  // If the number is out of range, just display dashes
  if (numToShow > maxNum || numToShow < minNum) {
    for (uint8_t digitNum = 0 ; digitNum < numDigits ; digitNum++) {
      digits[digitNum] = DASH_IDX;
    }
  }
  else {
    uint8_t digitNum = 0;

    // Convert all number to positive values
    if (numToShow < 0) {
      digits[0] = DASH_IDX;
      digitNum = 1; // Skip the first iteration
      numToShow = -numToShow;
    }

    // Find all digits for base's representation, starting with the most
    // significant digit
    for ( ; digitNum < numDigits ; digitNum++) {
      int32_t factor = powersOfBase[numDigits - 1 - digitNum];
      digits[digitNum] = numToShow / factor;
      numToShow -= digits[digitNum] * factor;
    }

    // Find unnnecessary leading zeros and set them to BLANK
    if (decPlaces < 0) decPlaces = 0;
    if (!leadingZeros) {
      for (digitNum = 0 ; digitNum < (numDigits - 1 - decPlaces) ; digitNum++) {
        if (digits[digitNum] == 0) {
          digits[digitNum] = BLANK_IDX;
        }
        // Exit once the first non-zero number is encountered
        else if (digits[digitNum] <= 9) {
          break;
        }
      }
    }

  }
}


// setDigitCodes
/******************************************************************************/
// Sets the 'digitCodes' that are required to display the input numbers
void SevSeg::setDigitCodes(const uint8_t digits[], int8_t decPlaces) {

  // Set the digitCode for each digit in the display
  for (uint8_t digitNum = 0 ; digitNum < numDigits ; digitNum++) {
    digitCodes[digitNum] = digitCodeMap[digits[digitNum]];
    // Set the decimal point segment
    if (decPlaces >= 0) {
      if (digitNum == numDigits - 1 - decPlaces) {
        digitCodes[digitNum] |= digitCodeMap[PERIOD_IDX];
      }
    }
  }
}

/// END ///
```
<a name="2021/11/08"></a>
# 2021/11/08
## Objective:
* debug the revised 7-segment 

issue 1
* PORTx or DDRx &= 0<< pin number 
* this appears in digitalWrite_new and pinMode_new 
* this codes does NOT work, because all the values are being cleared
* added function: selectively_clear

Revised Section 
```
/************
 create functions to replace pinMode and digitalWrite

 selectively_clear is a function that returns an 8-bit binary value that
    is used to selectively clear a specific bit

 pin_num: pin on the port
 port_num: values corresponding with specific ports
 in_out: input = 0, output = 1 set to bit
 lo_hi: output 1 or output 0
************/

uint8_t SevSeg::selectively_clear(uint8_t pin_num){
  switch(pin_num){
    case 0:
      return B11111110;
    case 1:
      return B11111101;
    case 2:
      return B11111011;
    case 3:
      return B11110111;
    case 4:
      return B11101111;
    case 5:
      return B11011111;
    case 6:
      return B10111111;
    case 7:
      return B01111111;
    default:
      return B11111111;
  }
}

void SevSeg::pinMode_new(uint8_t pin_num, uint8_t port_num, uint8_t in_out){
  switch(port_num){
      case 1:
        if(in_out) //if output
          DDRB |= 1<<pin_num; //bit-shifted to appropriate bit
        else //if input
          DDRB &= selectively_clear(pin_num);
        break;
      case 2:
        if(in_out)
          DDRC |= 1<<pin_num;
        else
          DDRC &= selectively_clear(pin_num);
        break;
      case 3:
        if(in_out)
          DDRD |= 1<<pin_num;
        else 
          DDRD &= selectively_clear(pin_num);
        break;
      /*case 4:
        if(in_out)
          DDRE |= 1<<pin_num; 
        else 
          DDRE &= selectively_clear(pin_num);
        break;*/
      /*case 5:
        if(in_out)
          DDRF |= 1<<pin_num; 
        else
          DDRF &= selectively_clear(pin_num);
        break;*/
    }
}
void SevSeg::digitalWrite_new(uint8_t pin_num, uint8_t port_num, uint8_t low_hi){
  switch(port_num){
      case 1:
        if(low_hi) //if high (1)
          PORTB |= 1<<pin_num; //bit-shifted to appropriate bit
        else //if low (0)
	  PORTB &= selectively_clear(pin_num);
        break;
      case 2:
        if(low_hi)
          PORTC |= 1<<pin_num;
        else
          PORTC &= selectively_clear(pin_num);
        break;
      case 3:
        if(low_hi)
          PORTD |= 1<<pin_num;
        else 
          PORTD &= selectively_clear(pin_num);
        break;
      /*case 4:
        if(low_hi)
          PORTE |= 1<<pin_num; 
        else 
          PORTE &= selectively_clear(pin_num);
        break;*/
      /*case 5:
        if(low_hi)
          PORTF |= 1<<pin_num; 
        else
          PORTF &= selectively_clear(pin_num);
        break;*/
      default:
	PORTB |= B11111111;
	break;
    }
}
```

* in progress:
  * C:\Users\tiffa\Desktop\UIUC_semester_5\ECE 445\Arduino_sense_code_4_pins_test
  * seeing if analog read works
  * analog read does work (PB4 and PB7 LED test)
    * LED pads didn't reflect actual values for some reason-- had to probe actual microcontroller pins (ik, bad, possible short)
    * analog read works

testing code in C:\Users\tiffa\Desktop\UIUC_semester_5\ECE 445\Arduino_sense_code_4_pins_test
```
  if(Vgrid_map > 10){ //test if analogRead works. turn on PB4, turn off PB7 (LEDs) if voltage applied is greater than 10V
    PORTB |= B00010000;
    PORTB &= B01111111;
  }
  else{ //turn off PB4, turn on PB7 if input voltage is less than 10V
    PORTB &= B11101111;
    PORTB |= B10000000;
  }
  ```

* need to make this work with the atmega32u4 now 
  * remove commented-out cases for pinMode_new and digitalWrite_new (additional ports now)
  * display main code: change pin and port values 
  * changed A0 button to A1 
  * added code LED changes depending on mode (DDRB and PORTB)
  * note: PE2 does not need additional configuration (verified by test code turning it on with DDRE and PORTE command)
  * tested code for "all segments off"-- testing the function from library with all digit pins turned on and all segment pins turned off, and vice versa
    * this works-- so issue is likely the BC337 transistors
      * did continuity test with transistors-- they seem fine, though 
  * gotta be a software issue... but i can't figure it out...

<a name="2021/11/09"></a>
# 2021/11/09
* BJTs were previously backwards
* BC337 -> changed to 2N4401 transistors (what the supply center had)-- the 2N4401 had the correct / lined up footprint direction
* hardwareConfig = N_TRANSISTORS;
  * hardware configuration change-- previously was CATHODE
* all LEDs lighting up now (but not updating fast enough-- problem for future me)

To do tomorrow / later:
* need to find a way to decrease update time (right now, it's cycling through the numbers too slowly)
* solder button onto PCB, and test with grid input (change code temporarily)

<a name="2021/11/10"></a>
# 2021/11/10
* changed brightness value, which decreased flickering time 
* issue: microcontroller is outputting incorrect voltages ("HIGH" is 1.5V, "LOW" is 0.5V)

<a name="2021/11/11"></a>
# 2021/11/11
* outputting incorrect voltages issue-- it's actually having a duty cycle (square wave with duty cycle), after looking at signal on oscilloscope
  * solution: change to internal RC oscillator, not with external 
* test the display (use test points for Vout and Iout): using data from 11/02
* testing DC inputs from 11/02
  * works
* testing AC inputs from 11/02
  * issue: when using the test points as microcontroller inputs, the voltage AC waveform decreased and flattened at the tops 
  * this may be because we are using the test points as direct inputs
* also, update time is slower than I want it to be-- so change brightness to 0

to-do next:
* test the sensing circuit recently soldered in 
  * current sensor
  * voltage grid 

<a name="2021/11/14"></a>
# 2021/11/14
* Current calibrate (same circuit from before):
  * normal resistor circuit-- 6.6V supply, 220-Ohm resistance, resulting in 30mA
  * 30mA -> 2.5165V
  * 0mA -> 2.5108V
    * slope = 0.19 V/A
    * previous calibration slope = 0.2 V/A
    * sanity check slope: ideal = (5/20) = 0.25 V/A
* Grid calibrate
  * 25V -> 2.3V
  * 0V -> 1.91V
    * slope = 0.0156 V/V
    * previous calibration slope = 0.0092352 V/V
    * sanity check slope: ideal = (4/340) = 0.01176 V/A, (4.5/340) = 0.013235 V/V
* DC calibrate
  * 25V -> 0.6V
  * 0V -> 0.029V
    * slope = 0.02284 V/V
    * previous calibration slope = 0.0277474 V/V
    * sanity check slope: ideal = (4.737/170) = 0.02786 V/V
* Inverter calibrate
  * 25V -> 1.5V
  * 0V -> 1.26V
    * slope = 0.0096 V/V
    * no previous calibration slope
    * sanity check slope: ideal = (4/340) = 0.01176 V/A, (4.5/340) = 0.013235 V/V

<a name="2021/11/15"></a>
# 2021/11/15
* LEDs without delays in 

new calibration values 
```
  //ard_5 = 1023;
  //ard_0 = 0;
  inv_0 = 1.588; 
  inv_25 = 1.885;
  dc_0 = 0.029;
  dc_25 = 0.6;
  grid_0 = 1.91;
  grid_25 = 2.3; 
  i_0 = 2.488;
  i_30 = 2.4955;
```

* multi-threaded (multicore)
  * cannot do multi-core because of the nature of the microcontroller 
  * https://create.arduino.cc/projecthub/reanimationxp/how-to-multithread-an-arduino-protothreading-tutorial-dd2c37 
  * reason for this is b/c inverter signal is distorted due to delay caused by display code 

<a name="2021/11/16"></a>
# 2021/11/16
* Sevseg library-- replace
```
DDRx &= selectively_clear(pin_num);
PORTx
```
with
```
DDRx &= ~(1<<pin_num);
PORTx 
```
and remove selectivelyClear function 

```
/************
  case statements for assigning ports
  1: Port B
  2: Port C
  3: Port D
  4: Port E
  5: Port F

  new functions written below (pinMode_new, digitalWrite_new-- include another parameter, port value)
************/

/************
 create functions to replace pinMode and digitalWrite

 pin_num: pin on the port
 port_num: values corresponding with specific ports
 in_out: input = 0, output = 1 set to bit
 lo_hi: output 1 or output 0
************/

void SevSeg::pinMode_new(uint8_t pin_num, uint8_t port_num, uint8_t in_out){
  switch(port_num){
      case 1:
        if(in_out) //if output
          DDRB |= 1<<pin_num; //bit-shifted to appropriate bit
        else //if input
          DDRB &= ~(1<<pin_num);
        break;
      case 2:
        if(in_out)
          DDRC |= 1<<pin_num;
        else
          DDRC &= ~(1<<pin_num);
        break;
      case 3:
        if(in_out)
          DDRD |= 1<<pin_num;
        else 
          DDRD &= ~(1<<pin_num);
        break;
      case 4:
        if(in_out)
          DDRE |= 1<<pin_num; 
        else 
          DDRE &= ~(1<<pin_num);
        break;
      case 5:
        if(in_out)
          DDRF |= 1<<pin_num; 
        else
          DDRF &= ~(1<<pin_num);
        break;
    }
}
void SevSeg::digitalWrite_new(uint8_t pin_num, uint8_t port_num, uint8_t low_hi){
  switch(port_num){
      case 1:
        if(low_hi) //if high (1)
          PORTB |= 1<<pin_num; //bit-shifted to appropriate bit
        else //if low (0)
	  PORTB &= ~(1<<pin_num);
        break;
      case 2:
        if(low_hi)
          PORTC |= 1<<pin_num;
        else
          PORTC &= ~(1<<pin_num);
        break;
      case 3:
        if(low_hi)
          PORTD |= 1<<pin_num;
        else 
          PORTD &= ~(1<<pin_num);
        break;
      case 4:
        if(low_hi)
          PORTE |= 1<<pin_num; 
        else 
          PORTE &= ~(1<<pin_num);
        break;
      case 5:
        if(low_hi)
          PORTF |= 1<<pin_num; 
        else
          PORTF &= ~(1<<pin_num);
        break;
    }
}

```

* TEST THIS NEW SEVSEG LIBRARY REVISION FIRST BEFORE TRYING MULTI-THREAD CODE
  * test with code in folder "Display_Code_Combined_5_newlib_with_atmega32u4_pins_2"
  * attempt to decrease run time
* combining code-- etc checks & multi-thread
  * etc checks (notes to self)
    * check DDRB |= 0b........, and PORTB |= 0b........
      * check 0b, check |= or &=
    * check "Serial." is not running (make sure all commented out or deleted)
  * multi-threading
    * referencing website https://create.arduino.cc/projecthub/reanimationxp/how-to-multithread-an-arduino-protothreading-tutorial-dd2c37 
    * priority function: inverter
    * secondary function: display

Combined code (sensing, display, & inverter) using multi-thread: 
```
// Team 3 (TREEE) 
// Combined sensing & display & inverter code

/*****************************************************************************************************/
/********************************** SOURCE/REFERENCE CODE COMMENTS ***********************************/
/*****************************************************************************************************/

/*
 * sPWMv2.c
 *
 * Created: 31/12/2014 3:44:43 PM
 *  Author: Kurt Hutten
 sPWM on the atMega328 for the arduino Uno, might be compatable with other atmel chips / arduino boards.
 Compare outputs A and B output to PORTB pins 1 and 2 which are pins 9 and 10 respectively on the Uno.
 Also useful to know the led on the Uno is pin 5 on PORTB.
 */ 

 /*
 * Initial Edits: 17/10/2021
 * Author: Elisa Krause
 * Adjusted code for ATmega32u4 with a q1 all positive sinusoidal signal and a q3 square wave.
 * This version is for an internal clock configuration with fclk = 8MHz
 */

/* SevSeg Library-- REVISED by TREEE to use with Atmega32U4
 *
 * Copyright 2020 Dean Reading
 *
 * This library allows an Arduino to easily display numbers and letters on a
 * 7-segment display without a separate 7-segment display controller.
 *
 * See the included readme for instructions.
 * https://github.com/DeanIsMe/SevSeg
 */

/*
  Smoothing-- referenced by TREEE to use with project

  Reads repeatedly from an analog input, calculating a running average and
  printing it to the computer. Keeps ten readings in an array and continually
  averages them.

  The circuit:
  - analog sensor (potentiometer will do) attached to analog input 0

  created 22 Apr 2007
  by David A. Mellis  <dam@mellis.org>
  modified 9 Apr 2012
  by Tom Igoe

  This example code is in the public domain.

  https://www.arduino.cc/en/Tutorial/BuiltInExamples/Smoothing
*/

/*
 * How to Set up Seven Segment Displays on the Arduino-- referenced by TREEE to use with project
 * by Krishna Pattabiraman
 */

/*
 * Long Press / Short Press code-- referenced by TREEE to use with project
 * Created by ArduinoGetStarted.com
 *
 * This example code is in the public domain
 *
 * Tutorial page: https://arduinogetstarted.com/tutorials/arduino-button-long-press-short-press
 */

 
#include <TimedAction.h>

#include <avr/io.h>
#include <avr/interrupt.h>

#include "SevSeg.h"
SevSeg sevseg; 

/***********************************************************************/
/******************* SENSE AND CALIBRATION VARIABLES *******************/
/***********************************************************************/

// defining ADC pins being used
#define Vout_sens A5
#define Vdc_sens A4
#define Vgrid_sens A3
#define Iout_sens A2

// store raw ADC in these variables
float Vout_adc;
float Vdc_adc;
float Vgrid_adc;
float Iout_adc;

// non-ideal mapping variables for AC, DC sensing circuit
float inv_0; //output of inverter sensing circuit when 0V potential is applied
float inv_25; //output of inverter sensing circuit when +25V potential is applied
float dc_0; //output of DC sensing circuit when 0V is applied
float dc_25; //output of DC sensing circuit when 25V is applied
float grid_0;
float grid_25;
float i_0; //output of current sensor when 0A is applied to input
float i_30; //output of current sensor when 30mA is applied to input

// non-ideal mapping variables for Arduino
//float ard_0; //ADC value of microcontroller when 0V is applied to microcon
//float ard_5; //ADC value of microcontroller when 5V is applied to microcon

//mapping ADC value to microcontroller input value
float Vard_map_inv;
float Vard_map_dc;
float Vard_map_grid; 
float Vard_map_i;

//slopes based on calibration values
float Vard_slope;
float inv_slope;
float dc_slope;
float grid_slope;
float i_slope;

// original values
float Vout_map;   //inverter output
float Vdc_map;    //DC-DC output
float Vgrid_map;  //grid output
float Iout_map;   //current output

/*************************************************************************/
/******************* CALCULATION AND DISPLAY VARIABLES *******************/
/*************************************************************************/

//intiliazing values
#define BUTTON_PIN A1
const int RESET_PRESS_TIME = 2000;

int buttonMode = 1;
int lastButtonState;  //previous state of button
int currentButtonState; //current state of button
unsigned long pressedTime = 0;
unsigned long releasedTime = 0;
unsigned long final_time = 0; //for energy calculation
unsigned long init_time = 0;  //for energy calculation
unsigned long total_time_elapsed = 0;

int iteration;                  //loop for printing delay
float totPower;                 //total power used to calculate average power
float totalEnergy;              //total energy
float currPower;                //current power
float avgPower_display;         //average power being displayed since it is not constantly changing

const int powerReadings = 100;   // size of array
float readings[powerReadings];  //values from current sensor
int readIndex = 0;              //index of current value
float avgPower = 0;             //average power

/***********************************************************************/
/********************(***** INVERTER VARIABLES *************************/
/***********************************************************************/

long unsigned count;

// Look up tables with 256 entries each, normalised to have max value of 800 which is the period of the PWM loaded into register ICR1.
int lookUp1[] = {410, 420, 429, 439, 449, 459, 468, 478, 488, 497, 507, 516, 525, 535, 544, 553, 562, 571, 580, 589, 597, 606, 614, 622, 630, 638, 646, 654, 661, 669, 676, 683, 690, 696, 703, 709, 715, 721, 727, 733, 738, 743, 748, 753, 757, 762, 766, 770, 773, 777, 780, 783, 786, 788, 790, 792, 794, 796, 797, 798, 799, 800, 800, 800, 800, 800, 799, 798, 797, 796, 794, 792, 790, 788, 786, 783, 780, 777, 773, 770, 766, 762, 757, 753, 748, 743, 738, 733, 727, 721, 715, 709, 703, 696, 690, 683, 676, 669, 661, 654, 646, 638, 630, 622, 614, 606, 597, 589, 580, 571, 562, 553, 544, 535, 525, 516, 507, 497, 488, 478, 468, 459, 449, 439, 429, 420, 410, 400, 390, 380, 371, 361, 351, 341, 332, 322, 312, 303, 293, 284, 275, 265, 256, 247, 238, 229, 220, 211, 203, 194, 186, 178, 170, 162, 154, 146, 139, 131, 124, 117, 110, 104, 97,  91,  85,  79,  73,  67,  62,  57,  52,  47,  43,  38,  34,  30,  27,  23,  20,  17,  14,  12,  10,  8, 6, 4, 3, 2, 1, 0, 0, 0, 0, 0, 1, 2, 3, 4, 6, 8, 10,  12,  14,  17,  20,  23,  27,  30,  34,  38,  43,  47,  52,  57,  62,  67,  73,  79,  85,  91,  97,  104, 110, 117, 124, 131, 139, 146, 154, 162, 170, 178, 186, 194, 203, 211, 220, 229, 238, 247, 256, 265, 275, 284, 293, 303, 312, 322, 332, 341, 351, 361, 371, 380, 390, 400};

/*************************************************************************/
/*************************************************************************/
/*************************************************************************/


/*****************************************************************/
/********************** FUNCTION 1: DISPLAY **********************/
/*****************************************************************/

// sense & display code that does not have to happen as frequently as the inverter code

void display_code(){

  /*************************************************************************/
  /*************************************************************************/
  
  // read raw ADC values (0 to 1023)
  Vout_adc = analogRead(Vout_sens);
  Vdc_adc = analogRead(Vdc_sens);
  Vgrid_adc = analogRead(Vgrid_sens);
  Iout_adc = analogRead(Iout_sens);

  //ADC -> voltage input into microcontroller
  Vard_map_inv = Vard_slope*Vout_adc;
  Vard_map_dc = Vard_slope*Vdc_adc;
  Vard_map_grid = Vard_slope*Vgrid_adc;
  Vard_map_i = Vard_slope*Iout_adc;

  Vout_map = (Vard_map_inv-inv_0)*inv_slope;  //sense_out = (inv_25-inv_0)/25 x input + inv_0
  Vdc_map = Vard_map_dc*dc_slope;             //sense_out = (dc_25 - dc_0)/25 x input
  Vgrid_map = (Vard_map_grid-grid_0)*grid_slope;
  Iout_map = (Vard_map_i-i_0)*i_slope;        //sense_out = (i_30-i_0)/.03 x input + i_0

  /*************************************************************************/
  /*************************************************************************/

   currentButtonState = digitalRead(BUTTON_PIN);
    if(lastButtonState == HIGH && currentButtonState == LOW){
      pressedTime = millis();
    }
    else if (lastButtonState == LOW && currentButtonState == HIGH){      
      releasedTime = millis();

      long pressDuration = releasedTime - pressedTime;

      if(pressDuration > RESET_PRESS_TIME) {
        totalEnergy = 0.0;
      }

      else if(pressDuration < RESET_PRESS_TIME){
        if(buttonMode == 1){
          buttonMode = 2;
        }
        else if (buttonMode == 2){
          buttonMode = 1;
        }
        else
          buttonMode = 1;
      }
    }
     switch(buttonMode){
      case 1:
        sevseg.setNumberF(avgPower_display, 1);
        PORTB &= 0b11101111; //turn off energy LED
        PORTB |= 0b10000000; //turn on power LED
        break;
      case 2:
        sevseg.setNumberF(totalEnergy, 1);
        PORTB |= 0b00010000; //turn on energy LED
        PORTB &= 0b01111111; //turn off energy LED
        break;
      default:
        sevseg.setNumberF(127.4, 1);
        break;  
     }
    currPower = Vout_map*Iout_map;  //p=IV 
    
    totPower = totPower - readings[readIndex];
    readings[readIndex] = currPower;
    totPower = totPower + readings[readIndex];
    readIndex = readIndex + 1;
    if (readIndex >= powerReadings ){
      readIndex = 0;
    }
    avgPower = totPower / powerReadings;
    
    lastButtonState = currentButtonState;
    sevseg.refreshDisplay(); 
    iteration++;
    iteration = iteration % 1000;
    if(iteration==0){
      avgPower_display = avgPower;
      final_time = millis();
      total_time_elapsed = final_time - init_time;
      totalEnergy = totalEnergy + (total_time_elapsed)*(avgPower)/(3600000); //division by 3.6e6 converts total time elapsed from ms to hours
      init_time = millis();
    }
}

// timer (function) that fires every 10 ms
TimedAction displayThread = TimedAction(10,display_code);


/*****************************************************************/
/***************************** SETUP *****************************/
/*****************************************************************/
void setup() {

  /***********************************************************************/
  /********************* SENSE AND CALIBRATION SETUP *********************/
  /***********************************************************************/

  // configure inputs/outputs
  pinMode(Vout_sens, INPUT);
  pinMode(Vdc_sens, INPUT);
  pinMode(Vgrid_sens, INPUT);
  pinMode(Iout_sens, INPUT);

  /* CALIBRATION VALUES */
  inv_0 = 1.588; 
  inv_25 = 1.885;
  dc_0 = 0.029;
  dc_25 = 0.6;
  grid_0 = 1.91;
  grid_25 = 2.3; 
  i_0 = 2.488;
  i_30 = 2.4955;

  /* calibrated slopes */
  Vard_slope = 5.0/1023;
  inv_slope = 25.0/(inv_25-inv_0);
  dc_slope = 25.0/(dc_25-dc_0);
  grid_slope = 25.0/(grid_25-grid_0);
  i_slope = 0.03/(i_30-i_0); //30mA


  /***********************************************************************/
  /******************** CALIBRATION AND DISPLAY SETUP ********************/
  /***********************************************************************/
  byte numDigits = 4;
  
  byte digitPins[] = {6, 7, 2, 6};
  byte digit_port[] = {3, 3, 4, 4};
  byte segmentPins[] = {6, 7, 0, 1, 2, 3, 4, 5};
  byte segment_port[] = {2, 2, 3, 3, 3, 3, 3, 3};

  pinMode(BUTTON_PIN, INPUT); //configure as input
  digitalWrite(BUTTON_PIN, HIGH); //internal pull-up
  bool resistorsOnSegments = true; 
  bool updateWithDelaysIn = false;
  byte hardwareConfig = N_TRANSISTORS; 
  sevseg.begin(hardwareConfig, numDigits, digitPins, segmentPins, digit_port, segment_port, resistorsOnSegments);
  
  sevseg.setBrightness(0);
  bool updateWithDelays = false; // Default 'false' is Recommended
  bool leadingZeros = false; // Use 'true' if you'd like to keep the leading zeros
  bool disableDecPoint = false;

  buttonMode = 1;
  lastButtonState = HIGH;
  currentButtonState = HIGH;
  totalEnergy = 0;
  currPower = 0;

  for (int i = 0; i < powerReadings; i++){
    readings[i] = 0;
  }

  init_time = millis();
  final_time = 0;

  /***********************************************************************/
  /*************************** INVERTER SETUP ****************************/
  /***********************************************************************/

  // Register initilisation, see datasheet for more detail.
    TCCR1A = 0b10100010;
       /*10 clear on match, set at BOTTOM for compA.
         10 clear on match, set at BOTTOM for compB.
         00
         10 WGM1 1:0 for waveform 15.
       */
    TCCR1B = 0b00011001;
       /*000
         11 WGM1 3:2 for waveform 15.
         001 no prescale on the counter.
       */
    TIMSK1 = 0b00000001;
       /*0000000
         1 TOV1 Flag interrupt enable. 
       */
    ICR1   = 800;     // Period for 16MHz crystal, for a switching frequency of 100KHz for 200 subdevisions per 50Hz sin wave cycle.
    sei();             // Enable global interrupts.

    DDRB |= 0b01100000; // Set PB1 and PB2 as outputs.

  /***********************************************************************/
  /****************************** ETC SETUP ******************************/
  /***********************************************************************/

  DDRB |= 0b10010001; //PB7 and PB4 as outputs (LEDs). configure PB0 relay pin as output
  PORTB |= 0b00000001; //always keep relay on

  //check if the functions need to be fired, and firing them if yes
  //threads should/must also be checked within loops, too (for, while, etc)
  displayThread.check();
  
}

/***********************************************************************************/
/********************** FUNCTION 2 INCLUDED IN LOOP: INVERTER **********************/
/***********************************************************************************/
//loop that repeats the most often included in the loop

void loop() {

  //check if the functions need to be fired, and firing them if yes
  displayThread.check();

    //inverter code below
    static float angle;
    static char trig;
    int index;
    int index2;
    // change duty-cycle every period.
    index = 0.7111*(angle + 1);
 
    if(index + 128 < 256){
       index2 = index + 128;
    }else{
       index2 = index - 128;
    }
 
    OCR1A = lookUp1[index];
    OCR1B = lookUp1[index2];

    angle = count*1.08;
    
    if(angle >= 360){ // Pre-increment num then check it's below 200.
       angle = 0;       // Reset num.
       count = 0;
     }
  
  /***********************************************************************/
  /***********************************************************************/
  /***********************************************************************/
  
  PORTB |= 0b00000001; //always keep relay on
}

/***********************************************************************/
/**************************** INTERRUPT ********************************/
ISR(TIMER1_OVF_vect){
count = count + 1;
}
```

Code to test tomorrow:
* test new sevseg library revision with code in folder "Display_Code_Combined_5_newlib_with_atmega32u4_pins_2"
  * if it works, then update files in revision files in ECE 445 desktop folder
* test multi-thread TimedAction code
  * adjust ms wait time below if necessary
```
// timer (function) that fires every 10 ms
TimedAction displayThread = TimedAction(10,display_code);
```
<a name="2021/11/17"></a>
# 2021/11/17
* test new sevseg library revision-- it works
* multi-thread revision
  * decrease function firing delay to at least less than 1ms-- hex display is flickering too much when larger than 1ms
    * 1 period of power sinusoid = 1/120 seconds
    * have the ADC sensing information be sampled at least 12 times
    * so ADC should be sampled every 1/120/12 seconds = 6.94*10^-4 seconds = 0.694 ms
    * so make 0.5ms to be the new firing delay instead
  * temporarily use 3 decimal places to see power output (because power output is incredibly small)
  * since there is now a built-in delay to the display function b/c of multi-threading, let's try just using the avgPower variable instead of avgPower_display
    * nah, that flickers too fast

<a name="2021/11/18"></a>
# 2021/11/18
new calibration values (using display)
* Grid calibrate
  * 25V -> 2.31V mapped 
  * 0V -> 1.98V
    * slope = 0.0132 V/V
    * previous calibration slope = 0.0156 V/V
    * sanity check slope: ideal = (4/340) = 0.01176 V/A, (4.5/340) = 0.013235 V/V
* DC calibrate
  * 25V -> 0.694V
  * 0V -> 0.000V
    * slope = 0.0274 V/V
    * previous calibration slope = 0.02776 V/V
    * sanity check slope: ideal = (4.737/170) = 0.02786 V/V
* Current calibrate:
  * normal resistor circuit-- 6.6V supply, 220-Ohm resistance, resulting in 30mA
  * 0mA -> 2.488V
    * previously calibrated slope based on circuit: 
      * 30mA -> 2.517V
      * 0mA -> 2.511V
      * slope = 0.2 V/A 
      * sanity check slope: ideal = (5/20) = 0.25 V/A
    * make 30mA -> 2.494V to make slope = 0.2 V/A 
* Inverter calibrate
  * issue: 0.06V-reading at 0V input 
  * uh... ok. one or more of the LMV358 chips are broken
  * ok, both inverter-related sides of the chips are broken. just replace them both
  * supply shop: LM358N only, not LMV358, and with a chip
    * https://www.ti.com/product/LM358-N 
  * uhhh it's working again?? ok keep going wtf wtf wtf 
  * calibrate 
    * 0V -> 1.52V
    * assume it follows grid's slope = 0.0132 V/V 

new calibration code
```
  /* CALIBRATION VALUES */
  inv_0 = 1.71; 
  inv_25 = 2.04;
  dc_0 = 0.0;
  dc_25 = 0.694;
  grid_0 = 1.98;
  grid_25 = 2.31; 
  i_0 = 2.5;
  i_30 = 2.5075;
```
<a name="2021/11/19"></a>
# 2021/11/19

* inverter 2nd calibration value needs to be estimated based on wattage instead of solidly determined
* can still find out 0V mapping, tho
* based on wattage-- the slope should be 1.5x as steep (our wattage display was 1/1.5th the actual wattage)
* inverter calibration-- 0V and 10V instead of 0V and 25V
```
inv_slope = 10.0/(inv_25-inv_0); //calibration values changed from 0V,25V to 0V,10V
```

* new voltage dividers b/c resolution is sht
  * we are doing 50VAC instead of 170VAC now
  * values from from 2021/10/07:
* DC: 50V to 5V
  * CONCLUSIONs: R_1 = 10 kOhms, R_2 = 1 kOhms
  * using resistors found in lab:
    * R1 = 13 kOhms, R2 = 1.3 kOhms, mapping range: 0V to 4.545V

* AC: 50V to 2V
  * CONCLUSIONs: R_1 = 10 kOhms, R_2 = 400 Ohm
  * using resistors found in lab:
    * R1 = 30 kOhms, R2 = 1.3 kOhms, mapping range: 0V to 2.08V

new calibration values:
```
  /* CALIBRATION VALUES */
  inv_0 = 1.9;
  inv_25 = 2.15; //actually corresponds to 10V input
  dc_0 = 0.0;
  dc_25 = 0.9; //actually corresponds to 10V input
  grid_0 = 1.89;
  grid_25 = 2.33; //actually corresponds to 10V input
  i_0 = 2.5;
  i_30 = 2.5075;
  ```

code changes to calibrate the inverter:
```
// Register initilisation, see datasheet for more detail.
    //TCCR1A = 0b10100010;
       /*10 clear on match, set at BOTTOM for compA.
         10 clear on match, set at BOTTOM for compB.
         00
         10 WGM1 1:0 for waveform 15.
       */
    //TCCR1B = 0b00011001;
       /*000
         11 WGM1 3:2 for waveform 15.
         001 no prescale on the counter.
       */
    //TIMSK1 = 0b00000001;
       /*0000000
         1 TOV1 Flag interrupt enable. 
       */
    //ICR1   = 400;     // Period for 16MHz crystal, for a switching frequency of 100KHz for 200 subdevisions per 50Hz sin wave cycle.
    //sei();             // Enable global interrupts.
```

<a name="2021/11/28"></a>
# 2021/11/28
* inverter output sensing-- rly bad...
* 0V -> 1.75V, 30V -> 2.0V...
* op-amps working properly
* oh wait... DC input voltage doesn't properly translate over to inverter output?! or is it just something with the code...
  * inverter output differential not lining up with DC input voltage. perhaps something i don't know about with elisa's switching code that does something when making the inverter output as 0VDC 
* find calibration points based on inverter output, not DC input 
  * 0V -> 1.75V 
  * 8.3V -> 1.9V
  * therefore, 10V would map with 1.93V
* power a lot more accurate (...?)

<a name="2021/11/29"></a>
# 2021/11/29
* attempt to calibrate inverter more accurately 
  * measure differential voltage from inverter directly 
  * take instant initial voltage and instant initial Vard_map_inv value (use display)
* data points (Vin -> Vout)
  * -9.15mV -> 1.93V
  * 8.55014V -> 2.05V
  * 0.014019854V output / V input
  * Vout - 1.93 = (slope) (Vin + 9.15m)
  * Vout = (slope)Vin + 1.930128282
